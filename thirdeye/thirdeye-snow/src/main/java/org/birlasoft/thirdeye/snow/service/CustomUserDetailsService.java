/**
 * 
 */
package org.birlasoft.thirdeye.snow.service;

import org.birlasoft.thirdeye.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author shaishav.dixit
 *
 */
public interface CustomUserDetailsService extends UserDetailsService {
	
	/**
	 * Get current user.
	 * @return {@code User}
	 */
	public User getCurrentUser();
	
}
