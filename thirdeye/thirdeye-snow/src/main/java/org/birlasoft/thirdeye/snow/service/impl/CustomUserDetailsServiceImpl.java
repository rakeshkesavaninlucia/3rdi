package org.birlasoft.thirdeye.snow.service.impl;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.snow.beans.SecurityUser;
import org.birlasoft.thirdeye.snow.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.snow.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class for load user by user name and get current user. 
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class CustomUserDetailsServiceImpl implements CustomUserDetailsService {	

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		
		User user = userService.findUserByEmail(userName);
		
		// Check if you have the user and the user is not in the deleted status.
		if(user == null || user.isDeleteStatus()){
			throw new UsernameNotFoundException("UserName "+userName+" not found");
		}
		return new SecurityUser(user);
	}
	
	/**
	 * Get current user.
	 * @return {@code User}
	 */
	@Override
	public User getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null && auth instanceof UsernamePasswordAuthenticationToken) {
			UsernamePasswordAuthenticationToken userNameToken = (UsernamePasswordAuthenticationToken) auth;
			Object principal = userNameToken.getPrincipal();
			
			if (principal instanceof UserDetails){
				String email = ((UserDetails) principal).getUsername();
				
				User loginUser = userService.findUserByEmail(email);
				loginUser.setEmailAddress(email); 
				
				return new SecurityUser(loginUser);
			}
		}
		
		return null;
	}
	
}
