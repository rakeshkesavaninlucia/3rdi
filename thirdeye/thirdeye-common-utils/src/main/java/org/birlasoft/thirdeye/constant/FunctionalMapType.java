package org.birlasoft.thirdeye.constant;

/**
 * 
 * Enum Types for Functional Map
 *
 */
public enum FunctionalMapType {
	
	L1("L1",1), L2("L2",2), L3("L3",3), L4("L4",4);
	
	private final String value;
	private final int key;   

    private FunctionalMapType(String value,int key) {
        this.value = value;
        this.key = key;
    }

	public String getValue() {
		return value;
	}
	
	public int getKey() {
		return key;
	}

}
