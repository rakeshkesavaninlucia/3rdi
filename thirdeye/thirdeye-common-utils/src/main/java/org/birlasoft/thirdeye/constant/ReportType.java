package org.birlasoft.thirdeye.constant;

/**
 * 
 * Enum Types for ReportType
 *
 */
public enum ReportType {

	DEPENDENCY_DIAGRAM ("Dependency Diagram"),
	GO_NO_GO_STRATEGY ("Go/No-Go Strategy"),
	HEALTH_ANALYSIS ("Health Analysis"), 
	ASSET_INTERFACE ("Asset Interface"),
	PORTFOLIO_HEALTH ("Portfolio Health"), 
	ASSET_MAPPING ("Asset Mapping"),
	FUNCTIONAL_REDUNDNACY ("Functional Redundancy"),
	CHART_OF_ACCOUNT ("Chart of Account"),
	WAVE_ANALYSIS ("Wave Analysis"),
	TCO_ANALYSIS ("TCO Analysis"),
	TREND_ANALYSIS ("Trend Analysis"),
	DENSITY_HEAT_MAP("Density Heat Map");
	
	String description;
	
	ReportType(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
	
	
}


