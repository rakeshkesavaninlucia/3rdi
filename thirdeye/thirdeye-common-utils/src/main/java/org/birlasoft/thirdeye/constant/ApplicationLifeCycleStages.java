package org.birlasoft.thirdeye.constant;

public enum ApplicationLifeCycleStages {
	NEW("NEW"),
	EMERGING("EMERGING"),
	MAINSTREAM("MAINSTREAM"),
	CONTAINMENT("CONTAINMENT"),
	SUNSET("SUNSET"),
	PROHIBITED("PROHIBITED");
	
	private String stages;
	
	 ApplicationLifeCycleStages(String stages) {
		this.stages = stages;
	}

	public String getStages() {
		return stages;
	}

}
