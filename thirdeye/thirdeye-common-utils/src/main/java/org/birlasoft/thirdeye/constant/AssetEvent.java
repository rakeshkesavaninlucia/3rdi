package org.birlasoft.thirdeye.constant;

public enum AssetEvent {

	CREATE("Create"), 
	EDIT("Edit"), 
	DELETE("Delete");
	
	private final String event;       

    private AssetEvent(String event) {
        this.event = event;
    }

	public String getEvent() {
		return event;
	}
}
