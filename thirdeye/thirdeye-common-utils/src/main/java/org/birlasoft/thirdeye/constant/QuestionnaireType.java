package org.birlasoft.thirdeye.constant;

/**
 * Enum to describe Questionnaire Type
 */
public enum QuestionnaireType {

	DEF ("questionnaire"),
	TCO ("tco");

	private String action;
	
	QuestionnaireType(String action){
		this.action = action;
	}

	public String getAction() {
		return action;
	}
}
