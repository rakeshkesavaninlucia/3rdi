/**
 * @author shagun.sharma
 *
 */

package org.birlasoft.thirdeye.constant;

public enum HintQuestionType {

	QUESTION_ONE("Which is your favorite web browser?"),
	QUESTION_TWO("What is the name of your first school?"),
	QUESTION_THREE("In what city were you born?"),
	QUESTION_FOUR("What is your mother's maiden name?"),
	QUESTION_FIVE("What is the name of your first grade teacher?"),
	QUESTION_SIX("In what city or town does your nearest sibling live?"),
	QUESTION_SEVEN("Who is your favorite actor, musician, or artist?");

	private String value;

	private HintQuestionType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
