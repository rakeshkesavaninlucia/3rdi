///**
// * 
// */
//package org.birlasoft.thirdeye.service.impl;
//
//import static org.junit.Assert.assertNotNull;
//
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
//import org.birlasoft.thirdeye.beans.AssetBean;
//import org.birlasoft.thirdeye.beans.ParameterBean;
//import org.birlasoft.thirdeye.beans.ParameterEvaluationResponse;
//import org.birlasoft.thirdeye.beans.QuestionBean;
//import org.birlasoft.thirdeye.beans.TemplateColumnBean;
//import org.birlasoft.thirdeye.beans.parameter.ParameterValueBoostBean;
//import org.birlasoft.thirdeye.entity.Asset;
//import org.birlasoft.thirdeye.entity.AssetData;
//import org.birlasoft.thirdeye.entity.AssetTemplate;
//import org.birlasoft.thirdeye.entity.AssetTemplateColumn;
//import org.birlasoft.thirdeye.entity.AssetType;
//import org.birlasoft.thirdeye.entity.Parameter;
//import org.birlasoft.thirdeye.entity.Question;
//import org.birlasoft.thirdeye.entity.Questionnaire;
//import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
//import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
//import org.birlasoft.thirdeye.entity.QuestionnaireParameterAsset;
//import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
//import org.birlasoft.thirdeye.entity.ResponseData;
//import org.birlasoft.thirdeye.entity.User;
//import org.birlasoft.thirdeye.service.AssetService;
//import org.birlasoft.thirdeye.service.CustomUserDetailsService;
//import org.birlasoft.thirdeye.service.ParameterBoostService;
//import org.birlasoft.thirdeye.service.ParameterService;
//import org.birlasoft.thirdeye.service.QuestionnaireParameterAssetService;
//import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
//import org.birlasoft.thirdeye.service.QuestionnaireService;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.runners.MockitoJUnitRunner;
//
///**
// * @author dhruv.sood
// *
// */
//@RunWith(MockitoJUnitRunner.class)
//public class ParameterBoostServiceImplTest {
//	
//	private static final int ID = 1;
//	
//	//User
//	private static final String FIRST_NAME = "Demo First";
//	private static final String LAST_NAME = "Demo Last";
//	private static final String EMAIL = "demo@birlasoft.com";
//	private static final String PASSWORD = "welcome12#";
//
//	@Mock
//	private QuestionnaireParameterService questionnaireParameterService;
//	@Mock
//	private ParameterService parameterService;
//	@Mock
//	private AssetService assetService;
//	@Mock
//	private QuestionnaireService questionnaireService;
//	@Mock
//	private CustomUserDetailsService customUserDetailsService;
//	@Mock
//	private QuestionnaireParameterAssetService questionnaireParameterAssetService;
//	@InjectMocks
//	private ParameterBoostService parameterBoostService = new ParameterBoostServiceImpl();
//	
//	private User dummyCurrentUser;
//	private List<QuestionnaireParameter> listOfQp = new ArrayList<>();
//	Set<Integer> setOfParameterId = new HashSet<>();	
//	Set<AssetBean> setOfAssetBean = new HashSet<>();
//	private Questionnaire questionnaire;
//	private AssetBean dummyAssetBean;
//	private Asset dummyAsset;
//	private AssetData dummyAssetData;
//	private AssetTemplate assetTemplate;
//	private AssetType assetType;
//	private AssetTemplateColumn assetTemplateColumn;
//	private QuestionnaireAsset questionnaireAsset;
//	private Parameter dummyParameter;
//	private ParameterBean dummyParameterBean;
//	private Set<QuestionnaireAsset> setOfQa = new HashSet<>();
//	private QuestionBean questionBean1;
//	private QuestionBean questionBean2;
//	private QuestionnaireQuestion qq1;
//	private QuestionnaireQuestion qq2;
//	private ParameterEvaluationResponse parameterEvaluationResponse;
//	private List<ParameterEvaluationResponse> listOfParameterEvaluationResponse = new ArrayList<>();
//	private QuestionnaireParameterAsset questionnaireParameterAsset;
//	private Date dummyDate = new Date();
//	private BigDecimal bigDecimal = new BigDecimal(1);
//	private ParameterValueBoostBean parameterValueBoostBean = new ParameterValueBoostBean();
//	private List<ParameterValueBoostBean> listOfParameterValueBoostBean = new ArrayList<>();
//	
//	@Before
//	public void setup(){
//		questionnaire = new Questionnaire();
//		questionnaire.setId(ID);
//		questionnaire.setName("Dummy Questionnaire");
//		
//		setOfParameterId.add(2);		
//		setupAsset();			
//		dummyCurrentUser = new User();
//		dummyCurrentUser.setId(ID);
//		dummyCurrentUser.setFirstName(FIRST_NAME);
//		dummyCurrentUser.setLastName(LAST_NAME);
//		dummyCurrentUser.setEmailAddress(EMAIL);
//		dummyCurrentUser.setPassword(PASSWORD);
//		
//		questionnaireAsset = new QuestionnaireAsset();
//		questionnaireAsset.setId(21);
//		questionnaireAsset.setQuestionnaire(questionnaire);
//		questionnaireAsset.setAsset(dummyAsset);
//		setOfQa.add(questionnaireAsset);
//		
//		QuestionnaireParameter questionnaireParameter = new QuestionnaireParameter();
//		questionnaireParameter.setId(ID);
//		questionnaireParameter.setQuestionnaire(questionnaire);
//		
//		dummyParameter = new Parameter();
//		dummyParameter.setId(1);
//		questionnaireParameter.setParameterByParameterId(dummyParameter);
//		
//		listOfQp.add(questionnaireParameter);
//		
//		Question question1 = new Question();
//		question1.setId(22);
//		question1.setQuestionMode("FIXED");
//		
//		questionBean1 = new QuestionBean(question1);
//		questionBean1.setType("NUMBER");
//		
//		
//		Question question2 = new Question();
//		question2.setId(21);
//		question2.setQuestionMode("FIXED");
//		
//		questionBean2 = new QuestionBean(question2);
//		questionBean2.setType("NUMBER");
//		
//		ParameterBean childParamBean = new ParameterBean();
//		childParamBean.setDisplayName("Dummy ParameterBean LP");
//		childParamBean.setName("Dummy ParameterBean LP");
//		childParamBean.setParamType("LP");
//		childParamBean.addChildQuestion(questionBean1);
//		childParamBean.addChildQuestion(questionBean2);
//		
//		dummyParameterBean = new ParameterBean();
//		dummyParameterBean.setDisplayName("Dummy ParameterBean AP");
//		dummyParameterBean.setName("Dummy ParameterBean AP");
//		dummyParameterBean.setParamType("AP");
//		dummyParameterBean.addChildParameter(childParamBean);
//		
//		questionBean1.setParentParameter(childParamBean);
//		questionBean2.setParentParameter(childParamBean);
//		
//		qq1 = new QuestionnaireQuestion();
//		qq1.setId(21);
//		qq1.setQuestionnaire(questionnaire);
//		qq1.setQuestion(question1);
//		qq1.setQuestionnaireParameter(questionnaireParameter);
//		qq1.setQuestionnaireAsset(questionnaireAsset);
//		qq1.setSequenceNumber(1);
//		qq1.setResponseDatas(new HashSet<ResponseData>());
//		
//		qq2 = new QuestionnaireQuestion();
//		qq2.setId(22);
//		qq2.setQuestionnaire(questionnaire);
//		qq2.setQuestion(question2);
//		qq2.setQuestionnaireParameter(questionnaireParameter);
//		qq2.setQuestionnaireAsset(questionnaireAsset);
//		qq2.setSequenceNumber(2);
//		qq2.setResponseDatas(new HashSet<ResponseData>());
//		
//		setupQuestionnaireParameterAsset();
//		setupPVBBean();
//		setupParameterEvauationResponse();
//		listOfParameterEvaluationResponse.add(parameterEvaluationResponse);
//	}
//
//	
//	private void mockServiceClassMethods() {
//		Mockito.when(assetService.findOne(ID)).thenReturn(dummyAsset);		
//		Mockito.when(parameterService.findOne(ID)).thenReturn(dummyParameter);
//		Mockito.when(questionnaireParameterService.findByQuestionnaire(questionnaire)).thenReturn(listOfQp);		
//		Mockito.when(parameterService.evaluateParameters(setOfParameterId,ID,setOfAssetBean)).thenReturn(listOfParameterEvaluationResponse);
//		Mockito.when(questionnaireParameterAssetService.findByQuestionnaireAndParameterAndAsset(questionnaire,dummyParameter,dummyAsset)).thenReturn(questionnaireParameterAsset);
//	}
//
//	
//	private void setupAsset() {
//		assetType = new AssetType();
//		assetType.setId(ID);
//		assetType.setAssetTypeName("Application");
//			
//		assetTemplate = new AssetTemplate();
//		assetTemplate.setId(ID);
//		assetTemplate.setAssetTemplateName("Dummy Asset Template");
//		assetTemplate.setAssetType(assetType);
//		
//		assetTemplateColumn = new AssetTemplateColumn();
//		assetTemplateColumn.setAssetTemplateColName("Dummy Asset Template");
//		assetTemplateColumn.setSequenceNumber(ID);
//		
//		dummyAssetData = new AssetData();
//		dummyAssetData.setId(ID);
//		dummyAssetData.setAsset(dummyAsset);
//		dummyAssetData.setAssetTemplateColumn(assetTemplateColumn);
//		
//		assetTemplate.setAssetType(assetType);
//		
//		dummyAsset = new Asset();
//		dummyAsset.setId(ID);
//		dummyAsset.setAssetTemplate(assetTemplate);
//			
//		List<TemplateColumnBean> assetDatas = new ArrayList<>();
//		TemplateColumnBean tcb = new TemplateColumnBean();
//		tcb.setData("Dummy Data");
//		tcb.setName("Test Name");
//		tcb.setSequenceNumber(1);
//		assetDatas.add(tcb);
//		
//		
//		Set<AssetData> setOfAssetData = new HashSet<>();
//		setOfAssetData.add(dummyAssetData);
//		
//		dummyAsset.setAssetDatas(setOfAssetData);
//		dummyAssetBean = new AssetBean(dummyAsset);	
//		dummyAssetBean.setShortName("Test");
//		dummyAssetBean.setAssetDatas(assetDatas);	
//		dummyAssetBean.setHexColor("#FFF");
//		dummyAssetBean.setRelationshipAssetId(ID);
//		dummyAssetBean.setAssetStyle("Asset Style");
//		setOfAssetBean.add(dummyAssetBean);
//	}
//	
//	private void setupParameterEvauationResponse() {
//		parameterEvaluationResponse = new ParameterEvaluationResponse();
//		parameterEvaluationResponse.setParameterBean(dummyParameterBean);
//		parameterEvaluationResponse.addAssetEvaluation(dummyAssetBean, bigDecimal);
//	}
//	
//	
//	private void setupQuestionnaireParameterAsset() {
//		questionnaireParameterAsset = new QuestionnaireParameterAsset();
//		questionnaireParameterAsset.setId(ID);		
//		questionnaireParameterAsset.setQuestionnaire(questionnaire);
//		questionnaireParameterAsset.setParameter(dummyParameter);
//		questionnaireParameterAsset.setAsset(dummyAsset);
//		questionnaireParameterAsset.setBoostPercentage(bigDecimal);
//		questionnaireParameterAsset.setCreatedDate(dummyDate);
//		questionnaireParameterAsset.setUpdatedDate(dummyDate);
//		questionnaireParameterAsset.setUserByCreatedBy(dummyCurrentUser);
//		questionnaireParameterAsset.setUserByUpdatedBy(dummyCurrentUser);
//	}
//	
//	private void setupPVBBean() {
//		parameterValueBoostBean = new ParameterValueBoostBean();
//		parameterValueBoostBean.setAssetId(ID);
//		parameterValueBoostBean.setParameterId(ID);
//		parameterValueBoostBean.setBoostPercentage("1.00");
//		parameterValueBoostBean.setEvaluatedValue(bigDecimal);
//		parameterValueBoostBean.setParameterName("Dummy Parameter");
//		parameterValueBoostBean.setQeId(ID);
//	}
//	
//	@Test
//	public void testGeneratetListOfPVBbean(){
//		mockServiceClassMethods();		
//		List<ParameterValueBoostBean> listOfParameterValueBoostBean = parameterBoostService.generatetListOfPVBbean(questionnaire, ID);
//		//Assert.assertEquals(1, listOfParameterValueBoostBean.size());//Still to work on
//	}
//		
//	@Test
//	public void testvalidateNewBoostValue(){
//		mockServiceClassMethods();		
//		QuestionnaireParameterAsset questionnaireParameterAsset = parameterBoostService.validateNewBoostValue(ID, ID, ID);
//		assertNotNull(questionnaireParameterAsset);		
//	}
//			
//	@Test
//	public void testboostEvaluatedResponse(){
//		mockServiceClassMethods();		
//		List<ParameterEvaluationResponse> questionnaireParameterAsset = parameterBoostService.boostEvaluatedResponse(questionnaire, listOfParameterEvaluationResponse );
//		assertNotNull(questionnaireParameterAsset);		
//	}	
//}
