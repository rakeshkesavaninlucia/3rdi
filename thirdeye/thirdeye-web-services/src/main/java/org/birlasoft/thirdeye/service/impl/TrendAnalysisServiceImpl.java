package org.birlasoft.thirdeye.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.birlasoft.thirdeye.beans.TrendData;
import org.birlasoft.thirdeye.beans.TrendWrapper;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.InputTrend;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Response;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.ParameterRepository;
import org.birlasoft.thirdeye.repositories.TrendRepository;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.ResponseService;
import org.birlasoft.thirdeye.service.TrendAnalysisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation service class for plotting scatter graph.
 * @author samar.gupta
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class TrendAnalysisServiceImpl implements TrendAnalysisService {

	private static Logger logger = LoggerFactory.getLogger(TrendAnalysisServiceImpl.class);

	@Autowired
	private QuestionnaireService questionnaireService;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private ResponseService responseService;
	@Autowired
	private TrendRepository trendRepository;
	@Autowired
	private ParameterRepository parameterRepository;
	@Override
	public TrendWrapper plotTrendGraph(String[] questionnaireIds, String[] parameterIds,Date startDate1,Date endDate1,Date startDate2,Date endDate2) {
           
		  SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		  Parameter xAxisParameter =parameterRepository.findOne(Integer.parseInt(parameterIds[0]));
		  Parameter yAxisParameter =parameterRepository.findOne(Integer.parseInt(parameterIds[1]));
         
		  List<InputTrend> xAxisTrends = trendRepository.findByParameterAndWorkspaceAndStartDateBetween(xAxisParameter,customUserDetailsService.getActiveWorkSpaceForUser(),startDate1, endDate1);
          Map<Date,InputTrend> yAxisTrends = trendRepository.findByParameterAndWorkspaceAndStartDateBetween(yAxisParameter,customUserDetailsService.getActiveWorkSpaceForUser(),startDate2, endDate2)
        		                         .stream().collect(Collectors.toMap(InputTrend::getStartDate, inputTrend->inputTrend,(e1, e2) ->e1)); 		 
        
        
          List<TrendData>trendDataList = xAxisTrends.stream().filter(x -> yAxisTrends.containsKey(x.getStartDate()))
                              .map(x -> new TrendData(formatter.format(x.getStartDate()),x.getValue(),yAxisTrends.get(x.getStartDate()).getValue(),x.getParameter().getDisplayName(),yAxisTrends.get(x.getStartDate()).getParameter().getDisplayName())).collect(Collectors.toList());
  	   
  	     TrendWrapper trendMapper = new TrendWrapper();
  	     trendMapper.setxAxisLabel(xAxisParameter.getDisplayName());
  	     trendMapper.setyAxisLabel(yAxisParameter.getDisplayName());
  	     trendMapper.setTrendData(trendDataList);
  	     return trendMapper;
	}
	@Override
	public List<Questionnaire> getQuestionnaireforTrend() {
		final Workspace activeWS = customUserDetailsService.getActiveWorkSpaceForUser();
		Set<Workspace> activeWorkspace = new HashSet<>(Arrays.asList( activeWS ));
		List<Questionnaire> questionnairesInWorkspaces = questionnaireService.findByWorkspaceInAndQuestionnaireType(activeWorkspace,QuestionnaireType.DEF.toString());
		List<Questionnaire> questionnairesToDisplay = new ArrayList<>();
		if(!questionnairesInWorkspaces.isEmpty()){
			List<Response> responses = responseService.findByQuestionnaireIn(questionnairesInWorkspaces);
			for (Response response : responses) {
				questionnairesToDisplay.add(response.getQuestionnaire());
			}
		}
		return questionnairesToDisplay;
	}

	

}
