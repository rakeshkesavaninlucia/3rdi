package org.birlasoft.thirdeye.service;

import java.util.List;

import org.birlasoft.thirdeye.beans.ParameterSelectorBean;
import org.birlasoft.thirdeye.entity.Parameter;

public interface ParameterSelectorService {

	public List<Parameter> getModalParameterForQuestionnaire(
			ParameterSelectorBean parameterSelectorBean);

}
