package org.birlasoft.thirdeye.service;

import java.util.List;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.asset.AssetChartRequestParamBean;
import org.birlasoft.thirdeye.beans.asset.AssetChartResponseParamBean;
import org.birlasoft.thirdeye.beans.asset.AssetParameterHealthBean;
import org.birlasoft.thirdeye.beans.parameter.ParamDrillDownBean;
import org.birlasoft.thirdeye.entity.Asset;

/**
 * Interface for asset parameter health drill down
 *
 */
public interface AssetParameterHealthService {

	/**
	 * method  get Asset View Health Parameter
	 * @author sunil1.gupta
	 * @param asset
	 * @return list of AssetParameterHealthBean
	 */
	public List<AssetParameterHealthBean> getAssetViewHealthParameter(Asset asset);

	/**
	 * method to get health parameter for asset view spider chart
	 * @author sunil1.gupta
	 * @param chartRequestParamBean
	 *  @return list of List AseetChartResponseParamBean
	 */
	public List<List<AssetChartResponseParamBean>> getAssetParamHealthData(AssetChartRequestParamBean chartRequestParamBean);

	/**
	 * Get data for parameter drill down
	 * @param chartRequestParamBean
	 * @return
	 */
	public List<ParamDrillDownBean> getAssetParamDrillDownData(AssetChartRequestParamBean chartRequestParamBean);

	/**
	 * Get parameter bean data for asset spider charts
	 * @param chartRequestParamBean
	 * @return
	 */
	public ParameterBean getParameterBean(AssetChartRequestParamBean chartRequestParamBean);

}
