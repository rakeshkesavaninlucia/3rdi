package org.birlasoft.thirdeye.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.ParameterEvaluationResponse;
import org.birlasoft.thirdeye.beans.QuestionBean;
import org.birlasoft.thirdeye.beans.QuestionnaireParameterEvaluator;
import org.birlasoft.thirdeye.beans.TcoParameterEvaluator;
import org.birlasoft.thirdeye.beans.parameter.FunctionalCoverageParameterConfigBean;
import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.constant.QuestionnaireStatusType;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.FunctionalMap;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterConfig;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.ParameterQualityGate;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.ParameterConfigRepository;
import org.birlasoft.thirdeye.repositories.ParameterFunctionRepository;
import org.birlasoft.thirdeye.repositories.ParameterQualityGateRepository;
import org.birlasoft.thirdeye.repositories.ParameterRepository;
import org.birlasoft.thirdeye.repositories.QuestionnaireParameterRepository;
import org.birlasoft.thirdeye.service.BCMLevelService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.FunctionalMapService;
import org.birlasoft.thirdeye.service.ParameterBoostService;
import org.birlasoft.thirdeye.service.ParameterFunctionService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QualityGateService;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.ResponseDataService;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service {@code class} of {@link Parameter} to {@code save, update, delete} , and 
 * find existing.
 * @author shaishav.dixit
 *
 */
/**
 * @author samar.gupta
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class ParameterServiceImpl implements ParameterService {
	
	@Autowired
	private ParameterRepository parameterRepository;
	@Autowired
	private ParameterFunctionRepository parameterFunctionRepository;
	@Autowired
	private QuestionnaireParameterRepository questionnaireParameterRepository;
	@Autowired
	private QuestionnaireQuestionService questionnaireQuestionService;
	@Autowired
	private QuestionnaireService questionnaireService;
	@Autowired
	private ParameterFunctionService parameterFunctionService;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private ParameterConfigRepository parameterConfigRepository;
	@Autowired
	private FunctionalMapService functionalMapService;
	@Autowired
	private BCMLevelService bcmLevelService;
	@Autowired
	private ParameterQualityGateRepository parameterQualityGateRepository;
	@Autowired
	private QualityGateService qualityGateService;
	@Autowired
	private QuestionnaireParameterService questionnaireParameterService;
	@Autowired
	private QuestionnaireAssetService questionnaireAssetService;
	@Autowired
	private ResponseDataService responseDataService ;
	@Autowired
	private ParameterBoostService parameterBoostService;
	
	
	@Override
	public Parameter save(Parameter parameter) {
		return parameterRepository.save(parameter);
	}

	@Override
	public List<Parameter> findAll() {
		return parameterRepository.findAll();
	}

	@Override
	public Parameter findOne(Integer id) {
		return parameterRepository.findOne(id);
	}

	@Override
	public Parameter findFullyLoaded(Integer id) {
		Parameter parameter = parameterRepository.findOne(id);
		loadParameterDetails(parameter);
		
		return parameter;
	}
	
	@Override
	public List<Parameter> findByQuestionnaire(Questionnaire qe, boolean loaded) {
		List<QuestionnaireParameter> listOfQP = questionnaireParameterRepository.findByQuestionnaire(qe);
		List<Parameter> parameterToBeReturned = new ArrayList<>();
		
		for (QuestionnaireParameter p : listOfQP){
			parameterToBeReturned.add(p.getParameterByParameterId());
		}
		
		if (loaded) {
			for (Parameter p : parameterToBeReturned){
				loadParameterDetails(p);
			}
		}
		
		return parameterToBeReturned;
	}
	
	/**
	 * Get the set of assets for a parameter
	 * @param p
	 * @return
	 */
	private Set<Asset> fetchAssetsForParameter(Parameter p) {
		Set<Asset> setToReturn = new HashSet<>();
		// handle for AP:
		// It will be AP if the Parameter Function Table has some entries where it 
		// is the parent and there is no entry in the QQ table.
		p.getQuestionnaireParametersForParameterId().size();
		Set<QuestionnaireQuestion> qqs = new HashSet<>();
		for (QuestionnaireParameter qp : p.getQuestionnaireParametersForParameterId()) {
			qp.getQuestionnaireQuestions().size();
			qqs.addAll(qp.getQuestionnaireQuestions());
		}
		if (!p.getParameterFunctionsForParentParameterId().isEmpty() && qqs.isEmpty()){
			return handleAggregateParameter(p);
		}
		
		// handle for LP
		if (!qqs.isEmpty()){
			return handleLeafParameter(p);
		}
		
		return setToReturn;
	}

	/**
	 * Returns the assets associated to the leaf parameter based on the 
	 * entries in the Questionnaire Question table.
	 *  
	 * @param p
	 * @return
	 */
	private Set<Asset> handleLeafParameter(Parameter p) {
		Set<Asset> assetSetToReturn = new HashSet<>();
		p.getQuestionnaireParametersForParameterId().size();
		Set<QuestionnaireQuestion> qqs = new HashSet<>();
		for (QuestionnaireParameter qp : p.getQuestionnaireParametersForParameterId()) {
			qp.getQuestionnaireQuestions().size();
			qqs.addAll(qp.getQuestionnaireQuestions());
		}
		if (qqs.isEmpty()){
			return assetSetToReturn;
		}
		
		for (QuestionnaireQuestion qq : qqs){
			assetSetToReturn.add(qq.getQuestionnaireAsset().getAsset());
		}
		
		return assetSetToReturn;
	}

	/**
	 * Returns the list of assets for an aggregate parameter.
	 * 
	 * @param p
	 * @return
	 */
	private Set<Asset> handleAggregateParameter(Parameter p) {
		// Find out which is common across all children
		Set<Asset> assetSetToReturn = new HashSet<>();
		
		// Safeguard the method from any garbage. We only need aggregate params here.
		if ( p.getParameterFunctionsForParentParameterId().isEmpty()){
			return assetSetToReturn;
		}
		
		Map<Integer, Set<Asset>> assetListForChildren = createChildParameterAssetMap(p);
		
		assetSetToReturn = Utility.findUniqueValues(assetListForChildren);
		
		return assetSetToReturn;
	}


	/**
	 * This method returns a map which has key as child parameter Id and
	 * the value is a set of assets which are on the child parameter.
	 * 
	 * @param p
	 * @return
	 */
	private Map<Integer, Set<Asset>> createChildParameterAssetMap(Parameter p) {
		// This map will hold a list of assets with the associated child parametr id as Key
		Map<Integer, Set<Asset>> assetListForChildren = new HashMap<>(); 
		
		// Go through all the parameter functions
		for (ParameterFunction oneChildParameterFunction : p.getParameterFunctionsForParentParameterId()){
			// You will only consider the child parameter Not the child questions
			Set<Asset> assetForOneChildParam = fetchAssetsForParameter(oneChildParameterFunction.getParameterByChildparameterId());
			assetListForChildren.put(oneChildParameterFunction.getParameterByChildparameterId().getId(), assetForOneChildParam);
		}
		
		return assetListForChildren;
	}
	
	private void loadParameterDetails(Parameter parameter) {
		// For cases where it a parent
		for (ParameterFunction oneFunction : parameter.getParameterFunctionsForParentParameterId()){
			// in which case the child is either a question or 
			if (oneFunction.getQuestion() != null){
				oneFunction.getQuestion().getTitle();
			}
			if (oneFunction.getParameterByChildparameterId() != null){
				oneFunction.getParameterByChildparameterId().getUniqueName();
			}
		}
		
		// Load the cases where it is the child parameter
		for (ParameterFunction oneFunction : parameter.getParameterFunctionsForChildparameterId()){
			if (oneFunction.getQuestion() != null){
				oneFunction.getQuestion().getTitle();
			}
			if (oneFunction.getParameterByChildparameterId() != null){
				oneFunction.getParameterByChildparameterId().getUniqueName();
			}
		}
	}

	@Override
	public Parameter createParameterObject(Parameter parameter, Integer questionnaireId, User currentUser) {
		parameter.setUserByCreatedBy(currentUser);
		parameter.setCreatedDate(new Date());
		parameter.setUserByUpdatedBy(currentUser);
		parameter.setUpdatedDate(new Date());
		// By Default parameter is created as a LP
		parameter.setType(ParameterType.LP.toString());
		
		Questionnaire qe = questionnaireService.findOne(questionnaireId);
		parameter.setWorkspace(qe.getWorkspace());
		// Temporary to run the existing flow of QE.
		parameter.setDisplayName(parameter.getUniqueName());
		return parameter;
	}

	@Override
	public List<ParameterBean> getParameterBeanList(List<Parameter> paramList) {
		// Create a bean from the Parameter
		List<ParameterBean> listOfParameterBeans = new ArrayList<>();
		if(paramList !=null){
		 for(Parameter oneParameter : paramList){	
		   ParameterBean pb = new ParameterBean(oneParameter.getId(), oneParameter.getUniqueName(), oneParameter.getDescription());
		   listOfParameterBeans.add(pb);
		 }
		}
		return listOfParameterBeans;
	}
	
	@Override
	public List<ParameterBean> getParametersOfQuestionnaire(List<Parameter> paramList) {
		List<ParameterBean> listOfRootParameters = new ArrayList<>(); 	
		if(paramList != null && !paramList.isEmpty()){
			for(Parameter parameter : paramList){
			   ParameterBean parameterBean = generateParameterTree(parameter);
			   listOfRootParameters.add(parameterBean);
			}
		}
		// based on the parameters get the root parameters list only		
		return listOfRootParameters;
	}
	
	@Override
	public List<ParameterBean> getRootParametersOfQuestionnaire(Questionnaire questionnaire) {
		// Find all the parameters that are in the qe
		List<Parameter> listOfLoadedParameters = findByQuestionnaireAndParameterByParentParameterIdIsNull(questionnaire, true);
		
		// based on the parameters get the root parameters only
		List<ParameterBean> listOfRootParameters = new ArrayList<>();
		for (Parameter parameterBean : listOfLoadedParameters) {
			listOfRootParameters.add(generateParameterTree(parameterBean));
		}
		return listOfRootParameters;
	}

	@Override
	public List<Parameter> findByQuestionnaireAndParameterByParentParameterIdIsNull(
			Questionnaire questionnaire, boolean loaded) {
		List<QuestionnaireParameter> listOfQP = questionnaireParameterRepository.findByQuestionnaireAndParameterByParentParameterIdIsNull(questionnaire);
		List<Parameter> parameterToBeReturned = new ArrayList<>();
		
		for (QuestionnaireParameter p : listOfQP){
			parameterToBeReturned.add(p.getParameterByParameterId());
		}
		
		if (loaded) {
			for (Parameter p : parameterToBeReturned){
				loadParameterDetails(p);
			}
		}
		
		return parameterToBeReturned;
	}

	@Override
	public Set<Parameter> findByWorkspaceIsNullOrWorkspaceIn(Set<Workspace> workspaces) {
		return parameterRepository.findByWorkspaceIsNullOrWorkspaceIn(workspaces);
	}

	@Override
	public List<Parameter> findByWorkspaceIsNullOrWorkspace(Workspace workspace) {
		return parameterRepository.findByWorkspaceIsNullOrWorkspaceLoaded(workspace);
	}

	@Override
	public List<Parameter> findByWorkspaceIsNull() {
		return parameterRepository.findByWorkspaceIsNull();
	}

	@Override
	public List<Parameter> findByWorkspace(Workspace activeWorkSpaceForUser) {
		return parameterRepository.findByWorkspace(activeWorkSpaceForUser);
	}
	
	@Override
	public Map<ParameterType, List<ParameterBean>> getAllParametersInTree(Parameter parameter) {
		Map<ParameterType, List<ParameterBean>> mapToReturn = new EnumMap<>(ParameterType.class);
		
		List<ParameterBean> listOfLp = new ArrayList<>();
		List<ParameterBean> listOfAp = new ArrayList<>();
		List<ParameterBean> listOfFc = new ArrayList<>();
		List<ParameterBean> listOfTcoA = new ArrayList<>();
		List<ParameterBean> listOfTcoL = new ArrayList<>();
		
		List<Parameter> listOfChildParameters = extractChildParameters(parameter);
		
		// Code does not include the parameter that is being searched for
		// we need to add it so that it shows up on the tree
		listOfChildParameters.add(parameter);
		
		Map<Integer, ParameterBean> parameterMap = new HashMap<>();
		for (Parameter oneParameter : listOfChildParameters) {
			parameterMap.put(oneParameter.getId(), new ParameterBean(oneParameter));
		}
		
		// Sort out the LP and AP
		sortChildParameters(listOfLp, listOfAp, listOfFc, listOfTcoA, listOfTcoL, listOfChildParameters, parameterMap);
		
		// Revalidate tree due to potential overwrite on the parent parameter.		
		List<ParameterBean> listOfParameterBean = new CopyOnWriteArrayList<>(parameterMap.values());
		Iterator<ParameterBean> paramBeanIterator = listOfParameterBean.iterator();
		while (paramBeanIterator.hasNext()){
			ParameterBean oneBean = paramBeanIterator.next();
			if (oneBean.getChildParameters() != null){	
				handleCloningOfChildBean(listOfLp, listOfAp, listOfFc, listOfTcoA, listOfTcoL, oneBean);
			}
		}
		
		mapToReturn.put(ParameterType.AP, listOfAp);
		mapToReturn.put(ParameterType.LP, listOfLp);
		mapToReturn.put(ParameterType.FC, listOfFc);
		mapToReturn.put(ParameterType.TCOA, listOfTcoA);
		mapToReturn.put(ParameterType.TCOL, listOfTcoL);
		
		return mapToReturn;
	}

	/**
	 * Clone duplicate child parameters in the tree
	 * @param listOfLp
	 * @param listOfAp
	 * @param listOfFc
	 * @param oneBean
	 */
	private void handleCloningOfChildBean(List<ParameterBean> listOfLp, List<ParameterBean> listOfAp, 
			List<ParameterBean> listOfFc, List<ParameterBean> listOfTcoA, List<ParameterBean> listOfTcoL, ParameterBean oneBean) {
		List<ParameterBean> listOfChildParameterBean = new CopyOnWriteArrayList<>(oneBean.getChildParameters() );
		Iterator<ParameterBean> childIterator = listOfChildParameterBean.iterator();
		while (childIterator.hasNext()){
			ParameterBean childBean  = childIterator.next();
			if (! childBean.getParent().equals(oneBean)){
				ParameterBean clonedBean = childBean.getClone();
				// Remove has to be done before the add.						
				oneBean.getChildParameters().remove(childBean);							
				oneBean.addChildParameter(clonedBean);						
				if (clonedBean.getParameterType().equals(ParameterType.LP)){
					listOfLp.add(clonedBean);
				} else if (clonedBean.getParameterType().equals(ParameterType.AP)){
					listOfAp.add(clonedBean);
				}else if (clonedBean.getParameterType().equals(ParameterType.FC)){
					listOfFc.add(clonedBean);
				} else if (clonedBean.getParameterType().equals(ParameterType.TCOL)){
					listOfTcoL.add(clonedBean);
				} else if (clonedBean.getParameterType().equals(ParameterType.TCOA)){
					listOfTcoA.add(clonedBean);
				}
			}
		}
	}
	
	/**
	 * Sort out child parameters into its different types.
	 * @param listOfLp
	 * @param listOfAp
	 * @param listOfFc
	 * @param listOfTcoA
	 * @param listOfTcoL
	 * @param listOfChildParameters
	 * @param parameterMap
	 */
	private void sortChildParameters(List<ParameterBean> listOfLp, List<ParameterBean> listOfAp, List<ParameterBean> listOfFc, List<ParameterBean> listOfTcoA, List<ParameterBean> listOfTcoL,
			List<Parameter> listOfChildParameters, Map<Integer, ParameterBean> parameterMap) {
		
		for (Parameter oneParameter : listOfChildParameters) {
			
			ParameterBean parent = parameterMap.get(oneParameter.getId());
			if (oneParameter.getType().equals(ParameterType.LP.toString())){
				addChildToLeafParameter(listOfLp, oneParameter, parent);
			} else if (oneParameter.getType().equals(ParameterType.AP.toString())){
				addChildToAggregateParameter(listOfAp, parameterMap, oneParameter, parent);
			}else if (oneParameter.getType().equals(ParameterType.FC.toString())){
				listOfFc.add(new ParameterBean(oneParameter));
			} else if (oneParameter.getType().equals(ParameterType.TCOL.toString())){
				addChildToLeafParameter(listOfTcoL, oneParameter, parent);
			} else if (oneParameter.getType().equals(ParameterType.TCOA.toString())){
				addChildToAggregateParameter(listOfTcoA, parameterMap, oneParameter, parent);
			}
		}
	}

	/**
	 * Add aggregate parameter to the parent parameter
	 * @param listOfAp
	 * @param parameterMap
	 * @param oneParameter
	 * @param parent
	 */
	private void addChildToAggregateParameter(List<ParameterBean> listOfAp, Map<Integer, ParameterBean> parameterMap, 
			Parameter oneParameter, ParameterBean parent) {
		for (ParameterFunction pf : oneParameter.getParameterFunctionsForParentParameterId()){
			if(pf.getParameterByChildparameterId() != null){
				ParameterBean child = parameterMap.get(pf.getParameterByChildparameterId().getId());
				if(pf.getSequenceNumber() != null){
					child.setSequenceNumber(pf.getSequenceNumber());
				}
				parent.addParameterFunction(child.getId(), pf);
				parent.addChildParameter(child);
			}
		}
		if(oneParameter.getType().equals(ParameterType.TCOA.name()))
			Collections.sort(parent.getChildParameters(), new SequenceNumberComparator());		
		listOfAp.add(parent);
	}

	/**
	 * Add Questions to the parameter
	 * @param listOfLp
	 * @param oneParameter
	 * @param parent
	 */
	private void addChildToLeafParameter(List<ParameterBean> listOfLp, Parameter oneParameter, ParameterBean parent) {
		for (ParameterFunction pf : oneParameter.getParameterFunctionsForParentParameterId()){
			if(pf.getQuestion() != null){
				QuestionBean questionBean = new QuestionBean(pf.getQuestion(), parent);
				if(pf.getSequenceNumber() != null){
					questionBean.setSequenceNumber(pf.getSequenceNumber());
				}
				parent.addChildQuestion(questionBean);
				parent.addParameterFunction(pf.getQuestion().getId(), pf);
			}
		}
		if(oneParameter.getType().equals(ParameterType.TCOL.name()))
			Collections.sort(parent.getChildQuestions(), new SequenceNumberComparator());
		listOfLp.add(parent);
	}
	
	@Override
	public ParameterBean generateParameterTree (Parameter rootLoadedParam){
		
		Map<ParameterType, List<ParameterBean>> paramsOnTheTree = getAllParametersInTree(rootLoadedParam);
		
		// Find out what we're dealing with
		ParameterType rootParamCandidate = null;
		if (paramsOnTheTree.containsKey(ParameterType.AP) && !paramsOnTheTree.get(ParameterType.AP).isEmpty()){
			rootParamCandidate = ParameterType.AP;
		} else if (paramsOnTheTree.containsKey(ParameterType.LP)  && !paramsOnTheTree.get(ParameterType.LP).isEmpty()){
			rootParamCandidate = ParameterType.LP;
		} else if (paramsOnTheTree.containsKey(ParameterType.TCOA) && !paramsOnTheTree.get(ParameterType.TCOA).isEmpty()){
			rootParamCandidate = ParameterType.TCOA;
		} else if (paramsOnTheTree.containsKey(ParameterType.TCOL) && !paramsOnTheTree.get(ParameterType.TCOL).isEmpty()){
			rootParamCandidate = ParameterType.TCOL;
		}
		
		
		if (rootParamCandidate != null){
			for (ParameterBean pb : paramsOnTheTree.get(rootParamCandidate)){
				if (pb.getParent() == null){
					return pb;
				}
			}
		}
		
		return null;
	}

	private List<Parameter> extractChildParameters(Parameter parameter) {
		//get list of all child AP and LP
		String childParameters = parameterFunctionRepository.getParameterTree(parameter.getId());
		if (childParameters == null || childParameters.isEmpty()){
			return new ArrayList<>();
		}
		String[] parameters = childParameters.split(",");
		
		//convert array of string to array of int
		int[] array = Arrays.stream(parameters).mapToInt(Integer::parseInt).toArray();
		
		//convert array of int to array of Integer
		Integer[] integerParameters = Arrays.stream( array ).boxed().toArray( Integer[]::new );
		
		return findByIdIn(new HashSet<Integer>(Arrays.asList(integerParameters)));
	}

	@Override
	public List<ParameterEvaluationResponse> evaluateParameters(Set<Integer> idsOfParameters, Integer idOfQuestionnaire, Set<AssetBean> assets) {
		List<Parameter> listOfParameters = findByIdIn(idsOfParameters);
		QuestionnaireParameterEvaluator lpapParameterEvaluator = new QuestionnaireParameterEvaluator(this, questionnaireQuestionService, functionalMapService, bcmLevelService, listOfParameters, questionnaireService.findOne(idOfQuestionnaire),parameterBoostService);
		return lpapParameterEvaluator.evaluateParameter(new ArrayList<AssetBean>(assets));
	}
	
	@Override
	public List<ParameterEvaluationResponse> evaluateTcoParameters(Integer idOfQuestionnaire, Set<AssetBean> assets) {		
		TcoParameterEvaluator tcoParameterEvaluator = new TcoParameterEvaluator(questionnaireQuestionService,questionnaireService.findOne(idOfQuestionnaire),questionnaireParameterService,questionnaireAssetService,responseDataService,this);
		return tcoParameterEvaluator.evaluateParameter(new ArrayList<AssetBean>(assets));
	}


	@Override
	public List<Parameter> findByIdIn(Set<Integer> idsOfParameter) {
		return parameterRepository.findByIdIn(idsOfParameter);
	}
	
	@Override
	public List<Parameter> findFullyLoadedParametersByIds(Set<Integer> idsOfParameter) {
		 List<Parameter> parameterListToBeReturned = parameterRepository.findByIdIn(idsOfParameter);
		for (Parameter p : parameterListToBeReturned){
			loadParameterDetails(p);
		}
		return parameterListToBeReturned;
	}

	@Override
	public Set<Parameter> findByParameter(Parameter parentParameter) {
		return parameterRepository.findByParameterLoadedFunctions(parentParameter);
	}

	@Override
	public void deleteParameter(Integer parameterId) {
		parameterRepository.delete(parameterId);
	}

	@Override
	public Parameter createVariantParameter(Parameter parameter) {
		Parameter newParameter = new Parameter();
		newParameter.setWorkspace(parameter.getWorkspace());
		newParameter.setType(parameter.getType());
		newParameter.setUniqueName(parameter.getUniqueName() + " - " + new SimpleDateFormat("MM-dd HH:mm:ss").format(new Date()));
		newParameter.setDisplayName(parameter.getDisplayName());
		newParameter.setDescription(parameter.getDescription());
		newParameter.setParameter(parameter);

		return newParameter;
	}

	@Override
	public Map<Integer, Map<Integer, BigDecimal>> createVariantAndChildEntitiesMapping(Set<Parameter> listOfVariants) {
		Map<Integer, Map<Integer, BigDecimal>> mapToShow = new HashMap<>();
		for (Parameter p : listOfVariants) {
			Set<ParameterFunction> functions = p.getParameterFunctionsForParentParameterId();
			Map<Integer, BigDecimal> mapOfEntityAndWeight = getMapOfChildEntityAndWeight(functions);
			mapToShow.put(p.getId(), mapOfEntityAndWeight);
		}
		return mapToShow;
	}

	/**
	 * Get a map of child entity and weight
	 * @param functions
	 * @return
	 */
	private Map<Integer, BigDecimal> getMapOfChildEntityAndWeight(Set<ParameterFunction> functions) {
		Map<Integer, BigDecimal> map2 = new HashMap<>();
		if(!functions.isEmpty()){
			for (ParameterFunction pf : functions) {
				if(pf.getQuestion() != null)
					map2.put(pf.getQuestion().getId(), pf.getWeight());
				if(pf.getParameterByChildparameterId() != null)
					map2.put(pf.getParameterByChildparameterId().getId(), pf.getWeight());
			}
		}
		return map2;
	}

	@Override
	public Map<Integer, Integer> getParamsOnQuestionnaire() {
		Set<QuestionnaireParameter> qps = questionnaireParameterRepository.getQuestionnaireParameterGroupByParameterId();
		Map<Integer, Integer> mapOfParams = new HashMap<>();
		for (QuestionnaireParameter qp : qps) {
			//TODO : Samar - why map? when key and value are same
			mapOfParams.put(qp.getParameterByParameterId().getId(), qp.getParameterByParameterId().getId());
		}
		return mapOfParams;
	}

	@Override
	public Set<Integer> createExtendedParameters(Set<Parameter> listOfParameter) {
		Set<Integer> setOfExtendedBaseParams = new HashSet<>();
		for (Parameter p : listOfParameter) {
			if(p.getParameter() != null)
				setOfExtendedBaseParams.add(p.getParameter().getId());
			if(!p.getParameterFunctionsForParentParameterId().isEmpty()){
				addChildParametersToSet(setOfExtendedBaseParams, p);
			}
		}
		return setOfExtendedBaseParams;
	}

	/**
	 * @param setOfExtendedBaseParams
	 * @param p
	 */
	private void addChildParametersToSet(Set<Integer> setOfExtendedBaseParams, Parameter p) {
		for (ParameterFunction pf : p.getParameterFunctionsForParentParameterId()) {
			if(pf.getParameterByChildparameterId() != null){
				setOfExtendedBaseParams.add(pf.getParameterByChildparameterId().getId());
			}
		}
	}

	@Override
	public Parameter updateParameterObject(ParameterBean parameterBean) {
		Parameter paramFromDB = findOne(parameterBean.getId());
		paramFromDB.setType(parameterBean.getParamType());
		paramFromDB.setUniqueName(parameterBean.getName());
		paramFromDB.setDisplayName(parameterBean.getDisplayName());
		paramFromDB.setDescription(parameterBean.getDescription());
		paramFromDB.setUpdatedDate(new Date());
		paramFromDB.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		return paramFromDB;
	}

	@Override
	public Parameter createNewParameter(ParameterBean parameterBean) {
		Parameter parameter = new Parameter();
		if(parameterBean.getWorkspaceId() != null && parameterBean.getWorkspaceId() > 0)
			parameter.setWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
		parameter.setType(parameterBean.getParamType());
		parameter.setUniqueName(parameterBean.getName());
		parameter.setDisplayName(parameterBean.getDisplayName());
		parameter.setDescription(parameterBean.getDescription());
		parameter.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
		parameter.setCreatedDate(new Date());
		parameter.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		parameter.setUpdatedDate(new Date());
		if(parameterBean.getBaseParameterId() != null)
			parameter.setParameter(findOne(parameterBean.getBaseParameterId()));
		return parameter;
	}

	@Override
	public ParameterConfig saveParameterConfig(ParameterConfig parameterConfig) {
		return parameterConfigRepository.save(parameterConfig);
	}

	@Override
	public List<ParameterConfig> findParameterConfigListByParameter(Parameter parameter) {
		return parameterConfigRepository.findByParameter(parameter);
	}
	
	@Override
	public ParameterConfig findParameterConfigByParameter(Parameter parameter) {
		return parameterConfigRepository.findByParameter(parameter).get(0);
	}

	@Override
	public void deleteParameterConfig(ParameterConfig parameterConfig) {
		parameterConfigRepository.delete(parameterConfig);
	}

	@Override
	public ParameterConfig findOneParameterConfig(Integer idOfParameterConfig) {
		return parameterConfigRepository.findOne(idOfParameterConfig);
	}

	@Override
	public void deleteParameterConfigs(List<ParameterConfig> parameterConfigs) {
		parameterConfigRepository.deleteInBatch(parameterConfigs);
	}

	@Override
	public ParameterConfig createNewParameterConfigObject(String functionalMapLevelType, Parameter savedParameter, FunctionalCoverageParameterConfigBean fcBean) {
		ParameterConfig pc = new ParameterConfig();
		pc.setParameter(savedParameter);
		if(!functionalMapLevelType.isEmpty())
			fcBean.setFunctionalMapLevelType(functionalMapLevelType);
		pc.setParameterConfig(Utility.convertObjectToJSONString(fcBean));
		return pc;
	}

	@Override
	public QualityGate getQualityGate(Integer idOfParam) {
		ParameterQualityGate parameterQualityGate = parameterQualityGateRepository.findByParameter(this.findOne(idOfParam));
		if(parameterQualityGate != null){		
			parameterQualityGate.getQualityGate().getColorDescription();
			return parameterQualityGate.getQualityGate();
		}
		return null;
	}

    @Override
	public Set<Parameter> findByWorkspaceInAndType(Set<Workspace> listOfWorkspaces,String type) {		
		return parameterRepository.findByWorkspaceInAndType(listOfWorkspaces,type);
	}
   
	@Override
	public Set<Parameter> getCostStructureByWorkspaceInAndType(Set<Workspace> listOfWorkspaces,String type) {
		
		Set<Parameter> allTcoByType = parameterRepository.findByWorkspaceInAndType(listOfWorkspaces,type);
		return getRootCostStructure(allTcoByType);	
	 }

	@Override
	public List<Parameter> findByWorkspaceIsNullOrWorkspaceAndType(Workspace activeWorkSpaceForUser, List<String> types) {
		List<Parameter> listOfParam = parameterRepository.findByWorkspaceIsNullOrWorkspaceLoadedAndType(activeWorkSpaceForUser,types);
		if(types.contains(ParameterType.TCOA.toString())){
			List<Parameter> rootCostStructure = new ArrayList<>(getRootCostStructure( new HashSet<Parameter>(listOfParam)));
			return rootCostStructure;	
		}
		return listOfParam;
	}

	@Override
	public List<Parameter> findByWorkspaceIsNullAndType(List<String> types) {		
		List<Parameter> listOfParam = parameterRepository.findByWorkspaceIsNullAndType(types);
		if(types.contains(ParameterType.TCOA.toString())){	
			List<Parameter> rootCostStructure = new ArrayList<>(getRootCostStructure( new HashSet<Parameter>(listOfParam)));
			return rootCostStructure;	
		}
		return listOfParam;
	}
	
	private Set<Parameter> getRootCostStructure(Set<Parameter> allTcoByType) {
		Set<Parameter> setOfRootCS = new HashSet<>();
		Set<Parameter> setOfChilParameter = new HashSet<>();
		if(!allTcoByType.isEmpty()){
			setOfChilParameter = parameterFunctionService.findByParameterByParentParameterIdIn(allTcoByType);
		}
		for(Parameter param : allTcoByType){
			if(!setOfChilParameter.contains(param)){
				setOfRootCS.add(param);
			}
		}
	    return setOfRootCS;
	}

	@Override
	public void deleteParameterQualityGate(Parameter parameter) {
		ParameterQualityGate parameterQualityGate = parameterQualityGateRepository.findByParameter(parameter);
		if(parameterQualityGate != null){
			Integer idOfQualityGate = parameterQualityGate.getQualityGate().getId();
			//delete patameter quality gate mapping
			parameterQualityGateRepository.delete(parameterQualityGate);
			//delete quality gate
			if (idOfQualityGate != null){				
				qualityGateService.delete(idOfQualityGate);
			}
		}
	}

	@Override
	public FunctionalCoverageParameterConfigBean getFcConfigBeanFromParameteConfig(List<ParameterConfig> parameterConfigs) {
		FunctionalCoverageParameterConfigBean fcParamConfigBean = null;
		for (ParameterConfig pc : parameterConfigs) {
			String paramConfig = pc.getParameterConfig();
			if(paramConfig != null){
				fcParamConfigBean = Utility.convertJSONStringToObject(paramConfig, FunctionalCoverageParameterConfigBean.class);
				FunctionalMap fmFromDB = functionalMapService.findOne(fcParamConfigBean.getFunctionalMapId(), false);
				fcParamConfigBean.setName(fmFromDB.getName());
			}
		}
		return fcParamConfigBean;
	}

	@Override
	public void extractSetOfQuestionsAndMandatoryQuestions(List<ParameterFunction> parameterFunctions,
			Map<Integer, Double> map, Set<Question> questions, Set<Integer> mandatoryQuestions) {
		for (ParameterFunction pf : parameterFunctions) {
			map.put(pf.getQuestion().getId(), pf.getWeight().doubleValue());
			questions.add(pf.getQuestion());
			if(pf.isMandatoryQuestion())
				mandatoryQuestions.add(pf.getQuestion().getId());
		}
	}
	
	@Override
	public Set<Parameter> findParameterIdByClosedStatus(String Status){
		List<Questionnaire>	questionnaireIdList = questionnaireService.findByClosedStatus(Status);
		Set<Parameter> parameterIdSet = new HashSet<>();

		if(!questionnaireIdList.isEmpty())
		{
			parameterIdSet =  questionnaireParameterService.findByQuestionnaireIn(questionnaireIdList);
		}
		return parameterIdSet;
	}
	
	@Override
	public List<QuestionnaireParameter> getQuestionnaireByParam(Parameter parameter) {
		List <QuestionnaireParameter> listOfQuestParam = questionnaireParameterService.findByParameterByParameterId(parameter);
		return listOfQuestParam;
	}
	
	@Override
	public boolean getStatusOfParam(Parameter parameter){
		boolean closedStatus = false;
		List <QuestionnaireParameter> listOfQuest = questionnaireParameterService.findByParameterByParameterId(parameter);
		if(!listOfQuest.isEmpty()){
			for (QuestionnaireParameter questionnaireParameter : listOfQuest) {
				if(questionnaireParameter.getQuestionnaire().getStatus().equals(QuestionnaireStatusType.CLOSED.toString())){
					closedStatus = true;
				}
			}
		}
		return closedStatus;
	}

	@Override
	public Set<Parameter> findByWorkspaceIsNullOrWorkspaceInAndTypeIn(Set<Workspace> workspaces, List<String> types) {
		return parameterRepository.findByWorkspaceIsNullOrWorkspaceInAndTypeIn(workspaces, types);
	}

	@Override
	public List<QualityGate> findByQualityGateIn(List<QualityGate> qg) {
		return parameterQualityGateRepository.findByQualityGateIn(qg);
	}

}
