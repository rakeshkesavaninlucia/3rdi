package org.birlasoft.thirdeye.beans.fr;

import java.util.ArrayList;
import java.util.List;

/**
 * @author samar.gupta
 *
 */
public class BcmParentLevelWrapper {
	
	private int parentLevelId;
	private String parentLevel;
	private List<BcmChildLevelWrapper> children = new ArrayList<>();
	
	public String getParentLevel() {
		return parentLevel;
	}
	public void setParentLevel(String parentLevel) {
		this.parentLevel = parentLevel;
	}
	public List<BcmChildLevelWrapper> getChildren() {
		return children;
	}
	public void setChildren(List<BcmChildLevelWrapper> children) {
		this.children = children;
	}
	public int getParentLevelId() {
		return parentLevelId;
	}
	public void setParentLevelId(int parentLevelId) {
		this.parentLevelId = parentLevelId;
	}
}
