package org.birlasoft.thirdeye.service;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.beans.report.ReportConfigBean;
import org.birlasoft.thirdeye.entity.Report;
import org.birlasoft.thirdeye.entity.SharedReport;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;

/**
 * @author manoj.shrivas
 *
 */
/**
 * @author manoj.shrivas
 *
 */
public interface SharedReportService {
		
		/**
		 * @param idOfSharedReport
		 * @return
		 */
		public List<SharedReport> findById(int idOfSharedReport);

		/**
		 * @param reportId
		 */
		public void save(List<SharedReport> reportId);

		
		/**
		 * @param userId
		 * @return
		 */
		public List<ReportConfigBean> getSharedReport(Integer userId);
       
		/**
		 * @param reportId
		 * @param currentuser
		 */
		public void deleteSavedReport(Integer reportId, User currentuser,Integer workspaceId);

		/**
		 * @param reportId
		 */
		public void deletesharedReport(List<SharedReport> report);

		/**
		 * @param workspaceId
		 * @return
		 */
		public SharedReport findByWorkspaceId(Integer workspaceId);
		
		/**
		 * @param reportId
		 * @param workspaceId
		 * @return
		 */
		public List<SharedReport> findByReportIdAndWorkspaceId(Integer reportId,Integer workspaceId);
      
		

	}
