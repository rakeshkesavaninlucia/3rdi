package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.search.api.beans.AssetQuestionWrapper;

public interface QuestionSearchService {
	
	public AssetQuestionWrapper getQuestionResponseFromElasticsearch(Integer questionId, Integer questionnaireId,
			Set<AssetBean> assetList, Map<String, List<String>> filterMap);

}
