package org.birlasoft.thirdeye.service.impl;


import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.Dashboard;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserWorkspace;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.UserWorkspaceRepository;
import org.birlasoft.thirdeye.service.BCMService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.DashboardService;
import org.birlasoft.thirdeye.service.UserService;
import org.birlasoft.thirdeye.service.UserWorkspaceService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 * 
 * UserWorkspaceService.java - Service implementation class for UserWorkspaceService
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class UserWorkspaceServiceImpl implements UserWorkspaceService {
	
	@Autowired
	private UserWorkspaceRepository userWorkspaceRepository;
	
	@Autowired
	private WorkspaceService workspaceService;
	
	@Autowired
	private DashboardService dashboardService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	BCMService bcmService;
	
	@Override
	public UserWorkspace findUserWorkspaceById(Integer userWorkspaceId){
		return userWorkspaceRepository.findOne(userWorkspaceId);
	}
	
	@Override
	public void delete(UserWorkspace userWorkspace) {
		userWorkspaceRepository.delete(userWorkspace);
	}
	
	@Override
	public UserWorkspace findByUserAndWorkspace(User user,Workspace workspace) {
	return userWorkspaceRepository.findByUserAndWorkspace(user, workspace);
		
	}
	
	@Override
	public UserWorkspace save(UserWorkspace userworkspace) {
		return userWorkspaceRepository.save(userworkspace);
	}
	
	@Override
	public void deleteUserFromWorkspace(Workspace workspace, Integer idOfUser) {
		Set<UserWorkspace> userWorkspaces =  workspace.getUserWorkspaces();
		     for (UserWorkspace userWorkspace : userWorkspaces) {
				if(userWorkspace.getUser().getId() == idOfUser){
					delete(userWorkspace);
					break;
				}
			}
	}

	@Override
	public UserWorkspace createNewUserWorkspaceObject(User user, Workspace workspace) {
		UserWorkspace userworkspceToSave = new UserWorkspace();
		userworkspceToSave.setUser(user);
		userworkspceToSave.setWorkspace(workspace);
		return userworkspceToSave;
	}

	@Override
	public List<UserWorkspace> findByUser(User user) {
		return userWorkspaceRepository.findByUser(user);
	}
		
	@Override
	public UserWorkspace updateUserWorkspaceObject(List<User> user, Workspace workspace) {
		UserWorkspace userworkspceToSave = new UserWorkspace();
		for(User userToSave : user){
		userworkspceToSave.setUser(userToSave);
		userworkspceToSave.setWorkspace(workspace);
		}
		return userworkspceToSave;
	}

	@Override
	public boolean getStatusForDashboardHomeLandingButton(String landingPageName ,Integer currentId) {
		boolean buttonStatus = false;
		User currentUserId = userService.findUserById(customUserDetailsService.getCurrentUser().getId());
		Workspace currentWorkspaceId = workspaceService
				.findOne(customUserDetailsService.getActiveWorkSpaceForUser().getId());
		UserWorkspace userWorkspace = findByUserAndWorkspace(currentUserId, currentWorkspaceId);

		 if (landingPageName.equals(userWorkspace.getSelectedHomePage())) {
					buttonStatus = true;
				}
		return buttonStatus;
	}
	
	@Override
	public boolean getStatusForAssetMappingReportHomeLandingButton(String landingPageName) {
		boolean buttonStatus = false;
		User currentUserId = userService.findUserById(customUserDetailsService.getCurrentUser().getId());
		Workspace currentWorkspaceId = workspaceService
				.findOne(customUserDetailsService.getActiveWorkSpaceForUser().getId());
		UserWorkspace userWorkspace = findByUserAndWorkspace(currentUserId, currentWorkspaceId);

	    if (landingPageName.equals(userWorkspace.getSelectedHomePage())) {
					buttonStatus = true;
				}
		return buttonStatus;
	}


	@Override
	public List<UserWorkspace> findByWorkspace(Workspace workspaceId) {
		return userWorkspaceRepository.findByWorkspace(workspaceId);
	}

}
