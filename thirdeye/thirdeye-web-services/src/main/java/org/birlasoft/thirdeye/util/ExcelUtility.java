package org.birlasoft.thirdeye.util;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Utility class for Excel 
 * @author dhruv.sood
 *
 */
public final class ExcelUtility {
	
	private static Logger logger = LoggerFactory.getLogger(ExcelUtility.class);
	private static final String SHEET_CONFIG = "config";
	public static final String EXCEL_FONT = "Calibri";
	private static final String DD_MMM_YYYY = "dd-MMM-yyyy";
	
	private ExcelUtility() {
	    throw new IllegalAccessError("Excel Utility class");
	  }

	
	/**@author dhruv.sood
	 ** @param workbook
	 * @param cellColorIndex
	 * @return
	 */
	public static final CellStyle getCellWithColor(XSSFWorkbook workbook, Short cellColorIndex) {
		CellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setFillForegroundColor(cellColorIndex);
		cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyle.setBorderTop((short) 1);
		cellStyle.setBorderBottom((short) 1);
		cellStyle.setBorderLeft((short) 1);
		cellStyle.setBorderRight((short) 1);
		cellStyle.setWrapText(true);
		return cellStyle;
	}
	
	/**@author dhruv.sood
	 * @param hash
	 * @param workbook
	 */
	public static final void setupConfigSheet(String hash, XSSFWorkbook workbook) {
		XSSFSheet sheet2 = workbook.createSheet(SHEET_CONFIG);
		
		logger.info("Setting up the config sheet");
		
		// make sure that the ordering is what you expect it to be
		// before you hide the sheet
		workbook.setSheetOrder(SHEET_CONFIG, 1);
		workbook.setSheetHidden(1, true);// hides the sheet at index 1
		
		CellStyle hiddenCell = getCustomCellStyle(workbook,";;;");
		
		// Creating sheet2 as secure and hidden
		Row row0Sheet2 = sheet2.createRow(0);
		Random random = new Random();
		String passwordToHash = new BigInteger(130, random).toString(32);
	
		Cell row0col0Sheet2 = row0Sheet2.createCell(0);
		row0col0Sheet2.setCellValue(hash);
		row0col0Sheet2.setCellStyle(hiddenCell);
		
		// Securing sheet2 with password
		// protect the sheet with a password
		sheet2.protectSheet(passwordToHash);
		// enable the locking features
		sheet2.enableLocking();
	
		// fine tune the locking options (in this example we are blocking all
		// operations on the cells: select, edit, etc.)
		sheet2.lockSelectLockedCells(true);
		sheet2.lockSelectUnlockedCells(true);
	}
	
	/**@author dhruv.sood
	 * @param workbook
	 * @param type
	 * @return
	 */
	public static final CellStyle getCustomCellStyle(XSSFWorkbook workbook, String type){
		DataFormat format = workbook.createDataFormat();
		CellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setDataFormat(format.getFormat(type));
		return cellStyle;
	}
	
	/**@author dhruv.sood
	 * @param sheet
	 * @param startRow
	 * @param lastRow
	 * @param startCell
	 * @param lastCell
	 * @param optionsArray
	 */
	public static final void generateDropDown(XSSFSheet sheet, int startRow, int lastRow, int startCell, int lastCell, String[] optionsArray)
	{
		DataValidationHelper validationHelper = new XSSFDataValidationHelper(sheet);
		CellRangeAddressList addressList = new CellRangeAddressList(startRow, lastRow, startCell, lastCell);
		DataValidationConstraint constraint = validationHelper.createExplicitListConstraint(optionsArray);
		DataValidation dataValidation = validationHelper.createValidation(constraint,addressList);
		dataValidation.setSuppressDropDownArrow(true);
		sheet.addValidationData(dataValidation);
	}
		
	/**@author dhruv.sood
	 * @param workbook
	 * @return
	 */
	public static final CellStyle getDateFormatCellStyle(XSSFWorkbook workbook){
		CellStyle dateCellStyle = workbook.createCellStyle();
		CreationHelper createHelper = workbook.getCreationHelper();
		dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat(DD_MMM_YYYY));
		return dateCellStyle;
	}
	
	/**@author dhruv.sood
	 * @param dateString
	 * @return
	 */
	public static final String convertDateFormat(String dateString){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateInString = dateString;
		try{
			Date date = formatter.parse(dateInString);
			dateInString=new SimpleDateFormat(DD_MMM_YYYY).format(date);
		}catch(ParseException e){
			logger.error("Error thrown in class ExcelUtility :: convertDateFormat(): "+e);
		}
		return dateInString;
	}
		
	/**@author dhruv.sood
	 * @param dateString
	 * @return
	 */
	public static final Date stringToDate(String dateString){
		SimpleDateFormat formatter = new SimpleDateFormat(DD_MMM_YYYY);
		Date date=new Date();
		try {
			date=formatter.parse(dateString);
		} catch (ParseException e) {
			logger.error("Error thrown in class ExcelUtility :: stringToDate(): "+e);
		}
		return date;
	}
	
	/**@author dhruv.sood
	 * @param date
	 * @return
	 */
	public static final String convertDateToString(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateString="";
		try{
			dateString=formatter.format(date);			
		}catch(Exception e){
			logger.error("Error thrown in class ExcelUtility :: convertDateToString(): "+e);
		}
		return dateString;
	}
	
	
}
