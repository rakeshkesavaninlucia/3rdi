package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.MultiMap;
import org.birlasoft.thirdeye.beans.DensityHeatMapWrapper;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.Country;
import org.birlasoft.thirdeye.repositories.CountryRepository;
import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.api.beans.TemplateColumnBean;
import org.birlasoft.thirdeye.service.BCMReportService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.DensityHeatMapService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class DensityHeatMapServiceImpl implements DensityHeatMapService  {
	private static Logger logger = LoggerFactory.getLogger(DensityHeatMapServiceImpl.class);

	@Value("#{systemProperties['" + Constants.ELASTICSEARCH_HOST_NAME + "']}")
	private String elasticSearchHost;
	@Autowired
	private CountryRepository countryRepository;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private BCMReportService bcmReportService;

	
	@SuppressWarnings({ "rawtypes", "unchecked" })	
	@Override
	public  List<DensityHeatMapWrapper> getAllAssetsFromAssetTemplatesForActiveWorkSpace(Integer[] idsOfLevel1,Integer[] idsOfLevel2,Integer[] idsOfLevel3,Integer[]idsOfLevel4) {
		MultiMap mapOfnoOfColumns = new org.apache.commons.collections4.map.MultiValueMap<>();
		List<Integer> assetIdlist = new ArrayList<>();
		List<DensityHeatMapWrapper> listWorldMapWrapper = new ArrayList<>();
		List<IndexAssetBean>  templateColumnBeans = getAssetTemplateFromElasticSearch(assetIdlist);
		if(idsOfLevel1!=null || idsOfLevel2!=null || idsOfLevel3!=null ||idsOfLevel4 !=null) {
			templateColumnBeans = getAssetIdsBasedOnLevelIdsFromElasticSearc(idsOfLevel1,idsOfLevel2,idsOfLevel3,idsOfLevel4);
		}
		for(IndexAssetBean oneBeans : templateColumnBeans) {
			List<TemplateColumnBean> templateColumnBean = new ArrayList<>();
			for (Map.Entry<String, TemplateColumnBean> entry : oneBeans.getTemplateCols().entrySet()) {
    			templateColumnBean.add(entry.getValue());
		}
			Map<String,Integer> mpColId = new HashMap<>();
			for(TemplateColumnBean oneBean :templateColumnBean) {
				if(oneBean.getName().equalsIgnoreCase("Country") || oneBean.getName().equalsIgnoreCase("No Of Users")){
					if(oneBean.getColumnDataType().getValue().equalsIgnoreCase("TEXT")) {
						mpColId.put(oneBean.getName(), oneBean.getId());
					}
					else {
						mpColId.put(oneBean.getName(), oneBean.getId());
					}
				}
			}
			Map<Object,Object> mapOfAssetTempCol= new HashMap<Object, Object>();
			for(TemplateColumnBean assetdata :  templateColumnBean)    	  
			{
				if( mpColId.containsValue(assetdata.getId()))
				{
					if(assetdata.getColumnDataType().getValue().equalsIgnoreCase("TEXT"))
					{
						mapOfAssetTempCol.put("country", assetdata.getValue());
					}
					else
						mapOfAssetTempCol.put("userscount",  assetdata.getValue());

					if(mapOfAssetTempCol.size()==2)
					{
						mapOfnoOfColumns.put(mapOfAssetTempCol.get("country"), mapOfAssetTempCol.get("userscount"));
						mapOfAssetTempCol= null ;
					}

				}	  
			}

		}
		
	
	Set keySet = mapOfnoOfColumns.keySet();
	Iterator keyIterator = keySet.iterator();
	while (keyIterator.hasNext()) {
		Integer userCount = 0;
		String countryNamekey = (String) keyIterator.next();
		List listuserCounts = (List) mapOfnoOfColumns.get(countryNamekey);
		for(Object oneCount : listuserCounts) {
			userCount = userCount +  Integer.parseInt(oneCount.toString());
		}
		Country oneCountry = findByCountryName(countryNamekey);
		listWorldMapWrapper.add(new DensityHeatMapWrapper(countryNamekey,userCount,listuserCounts.size(),oneCountry.getCountryCode()));
	}
		
	return listWorldMapWrapper;
}

	@Override
	public Country findByCountryName(String CountryName) {
		return countryRepository.findBycountryName(CountryName);
	}

	@Override
	public List<Country> findListOfCountry() {
		return countryRepository.findAll();
	}
	
	public List<IndexAssetBean> getAssetTemplateFromElasticSearch(List<Integer> assetIdsList) {
		final String uri = elasticSearchHost + Constants.BASESEARCHURL + "/assetColumns";

		List<IndexAssetBean> indexAssetBeanList = new ArrayList<>();
		List<IndexAssetBean> indexJsonAssetBeanList = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		RestTemplate restTemplate = new RestTemplate();
		SearchConfig searchConfig = setSearchConfig(assetIdsList);
		//Firing a rest request to elastic search server
		try {
			indexAssetBeanList = (List<IndexAssetBean>)restTemplate.postForObject(uri, searchConfig, List.class);
			indexJsonAssetBeanList = mapper.convertValue(indexAssetBeanList, new TypeReference<List<IndexAssetBean>>() { });
	
		} catch (RestClientException e) {				
			logger.error("Exception occured in BCMReportServiceImpl :: getBcmFromElasticsearch() :", e);
		}
		return indexJsonAssetBeanList;
	}
	
	/**
	 * Set tenant and workspace in search config. Also set search config values for widgets.
	 * @param tenantId
	 * @param activeWorkspaceId
	 * @return {@code SearchConfig}
	 */
	private SearchConfig setSearchConfig(List<Integer> assetIdsList) {
		String tenantId = customUserDetailsService.getCurrentAuthenticationDetails().getTenantURL();
		Integer activeWorkspaceId = customUserDetailsService.getActiveWorkSpaceForUser().getId();
		SearchConfig searchConfig=new SearchConfig();
		List<Integer> listOfWorkspace = new ArrayList<>();
		listOfWorkspace.add(activeWorkspaceId);
		searchConfig.setListOfWorkspace(listOfWorkspace);
		if(assetIdsList!=null && (!assetIdsList.isEmpty())){
		searchConfig.setListOfIds(assetIdsList);
		}
		List<String>list = new ArrayList<>();
		list.add(tenantId);
		searchConfig.setTenantURLs(list);
		return searchConfig;
	}
	
	private List<IndexAssetBean> getAssetIdsBasedOnLevelIdsFromElasticSearc(Integer[] idsOfLevel1, Integer[] idsOfLevel2,
			Integer[] idsOfLevel3,Integer[] idsOfLevel4) {
		List<IndexAssetBean> assetBeans = new ArrayList<>();
		if((idsOfLevel1!=null && idsOfLevel1.length>0) && (idsOfLevel2==null || idsOfLevel2.length == 0) && (idsOfLevel3 == null || idsOfLevel3.length == 0)){
			assetBeans = getAssetsByLevels(idsOfLevel1);
			
		}else if((idsOfLevel2!=null && idsOfLevel2.length>0) && (idsOfLevel3 == null || idsOfLevel3.length == 0)){
			assetBeans = getAssetsByLevels(idsOfLevel2);
			
		}else if((idsOfLevel3!=null && idsOfLevel3.length >0) && (idsOfLevel4==null || idsOfLevel4.length == 0)){
			assetBeans = getAssetsByLevels(idsOfLevel3);
		}
		else if(idsOfLevel4!=null && idsOfLevel4.length>0){
			assetBeans = getAssetsByLevels(idsOfLevel4);	
		}
		return assetBeans;
	}

	private List<IndexAssetBean> getAssetsByLevels(Integer[] idsOfLevel) {
		List<Integer> listOfAssestIds = bcmReportService.getListOfAssetBasedOnLevelIdsFromElasticSearch(idsOfLevel);
		List<IndexAssetBean>  listIndexAssetBean = getAssetTemplateFromElasticSearch(listOfAssestIds);
		return listIndexAssetBean;
	}
	
}
