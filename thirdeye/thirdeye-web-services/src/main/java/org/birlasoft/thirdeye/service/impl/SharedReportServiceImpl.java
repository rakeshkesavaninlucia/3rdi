package org.birlasoft.thirdeye.service.impl;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.birlasoft.thirdeye.beans.report.ReportConfigBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Report;
import org.birlasoft.thirdeye.entity.SharedReport;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.SharedReportRepository;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ReportService;
import org.birlasoft.thirdeye.service.SharedReportService;
import org.birlasoft.thirdeye.service.UserService;
import org.birlasoft.thirdeye.service.UserWorkspaceService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class SharedReportServiceImpl implements SharedReportService{

	private CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	ReportService reportService;
	
	
	@Autowired
	SharedReportRepository sharedReportRepo;
	
	@Autowired
	WorkspaceService workspaceService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserWorkspaceService userworkspaceService;
	
	/* (non-Javadoc)
	 * @see org.birlasoft.thirdeye.service.SharedReportService#getSharedReport(java.lang.Integer)
	 */
	@Override
    public List<ReportConfigBean> getSharedReport(Integer userId) {
           List<Report> listOfSharedReport = new ArrayList<>();
           List<ReportConfigBean> sharerdlist = new ArrayList<>();
           List<SharedReport> listSharedReportForCurrentUser = sharedReportRepo.findByUserId(userId);
           for(SharedReport oneReport : listSharedReportForCurrentUser){
                 Report oneSharedReport =  reportService.findReportById(oneReport.getReportId());
                 listOfSharedReport.add(oneSharedReport);
           } 
           
           for(Report one: listOfSharedReport){
                  ReportConfigBean rc = new ReportConfigBean();
                  rc.setId(one.getId());
                  rc.setReportName(one.getReportName());
                  rc.setDescription(one.getDescription());
                  rc.setReportType(one.getReportType());
                  rc.setStatus(one.isStatus());
                  rc.setSharedStatus(true);
                  sharerdlist.add(rc);
           }
    return sharerdlist;
    
    }

	@Override
	public List<SharedReport> findById(int idOfSharedReport) {
		return sharedReportRepo.findById(idOfSharedReport);
	}


	@Override
	public void save(List<SharedReport> reportId) {
		sharedReportRepo.save(reportId);
	}
	
	/* (non-Javadoc)
	 * @see org.birlasoft.thirdeye.service.SharedReportService#deletesharedReport(java.lang.Integer)
	 */
	@Override
	public void deletesharedReport(List<SharedReport> report){
		sharedReportRepo.deleteInBatch(report);
	}
	
	@Override
	public void deleteSavedReport(Integer reportId,User currentuser,Integer workspaceId) {
		List<SharedReport> sharedReportList = this.findByReportIdAndWorkspaceId(reportId,workspaceId);
		Report oneReport = reportService.findReportById(reportId);
		if(oneReport.getUserByCreatedBy().equals(currentuser)){
			if(sharedReportList !=null){
			this.deletesharedReport(sharedReportList);
			}
			reportService.deleteReport(oneReport);
		}
		else{
			this.deletesharedReport(sharedReportList);
		}
			
	}

	@Override
	public SharedReport findByWorkspaceId(Integer  workspaceId) {
		return sharedReportRepo.findByWorkspaceId(workspaceId);
	}

	@Override
	public List<SharedReport> findByReportIdAndWorkspaceId(Integer reportId, Integer workspaceId) {
		return sharedReportRepo.findByReportIdAndWorkspaceId(reportId, workspaceId);
	}
	
}


