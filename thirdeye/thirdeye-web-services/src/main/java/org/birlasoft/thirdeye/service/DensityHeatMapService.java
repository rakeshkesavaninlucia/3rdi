package org.birlasoft.thirdeye.service;


import java.util.List;
import org.birlasoft.thirdeye.beans.DensityHeatMapWrapper;
import org.birlasoft.thirdeye.entity.Country;
import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;

/**
 * @author shagun.sharma
 *
 */
public interface DensityHeatMapService {

	/**
	 * Method to get world map wrapper across active workspace
	 * @return{@code List Of World Map Wrapper}
	 */
	List<DensityHeatMapWrapper> getAllAssetsFromAssetTemplatesForActiveWorkSpace(Integer[] idsOfLevel1,Integer[] idsOfLevel2,Integer[] idsOfLevel3,Integer[]idsOfLevel4);
	
	/** Method to find the Country Data 
	 * @param CountryName
	 * @return{@code countrydata}
	 */
	Country findByCountryName(String CountryName); 
	
	/** Method to find the list of country data
	 * @return{@code list Of Country }
	 */
	List<Country> findListOfCountry();
	
	/**
	 * @return{@code list of index Asset Bean From Elastic Search}
	 */
	public List<IndexAssetBean> getAssetTemplateFromElasticSearch(List<Integer> assetIdsList);


}
