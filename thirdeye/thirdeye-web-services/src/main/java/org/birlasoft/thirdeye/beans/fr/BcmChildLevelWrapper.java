package org.birlasoft.thirdeye.beans.fr;

import java.util.ArrayList;
import java.util.List;

/**
 * @author samar.gupta
 *
 */
public class BcmChildLevelWrapper {
	
	private int id;
	private String name;
	private boolean dataExist; 
	private List<Integer> assetValues = new ArrayList<>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Integer> getAssetValues() {
		return assetValues;
	}
	public void setAssetValues(List<Integer> assetValues) {
		this.assetValues = assetValues;
	}
	public boolean isDataExist() {
		return dataExist;
	}
	public void setDataExist(boolean dataExist) {
		this.dataExist = dataExist;
	}
}
