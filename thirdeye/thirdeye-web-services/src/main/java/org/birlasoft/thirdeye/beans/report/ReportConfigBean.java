package org.birlasoft.thirdeye.beans.report;

import org.birlasoft.thirdeye.entity.Report;

/**
 * @author sunil1.gupta
 *
 */
public class ReportConfigBean {
	
	
	private Integer id;
	private String reportName;
	private String reportType;
	private String description;
	private String reportConfig;
	private boolean deletedStatus;
	private boolean status;
	private boolean sharedStatus;
		
	
	public ReportConfigBean() {
	
	}

	/**
	 * 
	 */
	public ReportConfigBean(Report report) {
		this.id = report.getId();
		this.reportName = report.getReportName();
		this.reportType = report.getReportType();
		this.description = report.getDescription();
		this.reportConfig = report.getReportConfig();
		this.deletedStatus = report.isDeletedStatus();
		this.status = report.isStatus();
	}

	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReportConfig() {
		return reportConfig;
	}

	public void setReportConfig(String reportConfig) {
		this.reportConfig = reportConfig;
	}

	public boolean isDeletedStatus() {
		return deletedStatus;
	}

	public void setDeletedStatus(boolean deletedStatus) {
		this.deletedStatus = deletedStatus;
	}
	
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public boolean isSharedStatus() {
		return sharedStatus;
	}

	public void setSharedStatus(boolean sharedStatus) {
		this.sharedStatus = sharedStatus;
	}
}
