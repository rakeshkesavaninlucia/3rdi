package org.birlasoft.thirdeye.beans.aid;

import org.birlasoft.thirdeye.constant.aid.AIDBlockType;
import org.birlasoft.thirdeye.entity.AidBlock;
import org.birlasoft.thirdeye.interfaces.SubBlockUser;
import org.birlasoft.thirdeye.search.api.beans.IndexAidBlockBean;

public class AIDBlockHelper {


	public static <T> T getAIDBlockBeanFromEntity(AidBlock oneEntity){

		AIDBlockType blockType = AIDBlockType.valueOf(oneEntity.getAidBlockType());
		switch (blockType) {
		case BLOCK_1:
			JSONAIDBlock1Bean oneBlock = new JSONAIDBlock1Bean(oneEntity);
			return (T) oneBlock;
		case BLOCK_2:
			JSONAIDBlock2Bean oneBlock2 = new JSONAIDBlock2Bean(oneEntity);
			return (T) oneBlock2;
		case BLOCK_3:
			JSONAIDBlock3Bean oneBlock3 = new JSONAIDBlock3Bean(oneEntity);
			return (T) oneBlock3;
		case BLOCK_4:
			JSONAIDBlock4Bean oneBlock4 = new JSONAIDBlock4Bean(oneEntity);
			return (T) oneBlock4;
		default:
			break;
		}

		return null;
	}

	public static JSONAIDSubBlockConfig getSubBlockByIdentifier(AidBlock oneBlock, String subBlockType) {
		SubBlockUser subBlockUser = AIDBlockHelper.getAIDBlockBeanFromEntity(oneBlock);
		
		return subBlockUser.getSubBlockFromIdentifier(subBlockType);
	}
	
	/**
	 * Get AID block Bean from IndexAidBlockBean.
	 * @param indexAidBlockBean
	 * @return <T>
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getAIDBlockBeanFromIndexAidBlockBean(IndexAidBlockBean indexAidBlockBean){

		AIDBlockType blockType = AIDBlockType.valueOf(indexAidBlockBean.getAidBlockType());
		switch (blockType) {
		case BLOCK_1:
			JSONAIDBlock1Bean oneBlock = new JSONAIDBlock1Bean(indexAidBlockBean);
			return (T) oneBlock;
		case BLOCK_2:
			JSONAIDBlock2Bean oneBlock2 = new JSONAIDBlock2Bean(indexAidBlockBean);
			return (T) oneBlock2;
		case BLOCK_3:
			JSONAIDBlock3Bean oneBlock3 = new JSONAIDBlock3Bean(indexAidBlockBean);
			return (T) oneBlock3;
		case BLOCK_4:
			JSONAIDBlock4Bean oneBlock4 = new JSONAIDBlock4Bean(indexAidBlockBean);
			return (T) oneBlock4;
		default:
			break;
		}

		return null;
	}
}
