package org.birlasoft.thirdeye.beans.widgets;

import org.birlasoft.thirdeye.entity.Widget;
import org.birlasoft.thirdeye.util.Utility;


public class GroupedWidgetJSONConfig extends BaseWidgetJSONConfig {

	private int questionnaireId;
	private String fieldOne;
	private String fieldTwo;
	private int fieldOneId;
	private int fieldTwoId;
	
	public GroupedWidgetJSONConfig() {
		// TODO Auto-generated constructor stub
	}
	
	public GroupedWidgetJSONConfig(Widget w) {
		super(w);
		
		GroupedWidgetJSONConfig deserialized = Utility.convertJSONStringToObject(w.getWidgetConfig(), GroupedWidgetJSONConfig.class);
		if (deserialized != null){
			this.questionnaireId = deserialized.getQuestionnaireId();
			this.fieldOne = deserialized.fieldOne;
			this.fieldTwo = deserialized.fieldTwo;
			this.fieldOneId = deserialized.fieldOneId;
			this.fieldTwoId = deserialized.fieldTwoId;
		}
				
	}
	public int getQuestionnaireId() {
		return questionnaireId;
	}
	public void setQuestionnaireId(int questionnaireId) {
		this.questionnaireId = questionnaireId;
	}
	
	public String getFieldOne() {
		return fieldOne;
	}

	public void setFieldOne(String fieldOne) {
		this.fieldOne = fieldOne;
	}

	public String getFieldTwo() {
		return fieldTwo;
	}

	public void setFieldTwo(String fieldTwo) {
		this.fieldTwo = fieldTwo;
	}

	public int getFieldOneId() {
		return fieldOneId;
	}

	public void setFieldOneId(int fieldOneId) {
		this.fieldOneId = fieldOneId;
	}

	public int getFieldTwoId() {
		return fieldTwoId;
	}

	public void setFieldTwoId(int fieldTwoId) {
		this.fieldTwoId = fieldTwoId;
	}
	
}
