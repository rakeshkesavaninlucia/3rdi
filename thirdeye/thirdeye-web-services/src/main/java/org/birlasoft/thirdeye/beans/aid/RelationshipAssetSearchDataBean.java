package org.birlasoft.thirdeye.beans.aid;


/**
 * This bean class {@link RelationshipAssetSearchDataBean} is used for 
 * display relationship data.
 * @author samar.gupta
 *
 */
public class RelationshipAssetSearchDataBean {

	private String displayName;
	private String dataType;
	private String frequency;
	private String relationshipTypeDisplayName;
	private String direction;
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getRelationshipTypeDisplayName() {
		return relationshipTypeDisplayName;
	}
	public void setRelationshipTypeDisplayName(String relationshipTypeDisplayName) {
		this.relationshipTypeDisplayName = relationshipTypeDisplayName;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	
}
