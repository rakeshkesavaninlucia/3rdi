package org.birlasoft.thirdeye.service;

import java.util.List;

import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserWorkspace;
import org.birlasoft.thirdeye.entity.Workspace;

/**
 *	Service interface for user workspace.
 */
/**
 * @author manoj.shrivas
 *
 */
public interface UserWorkspaceService {
	
	/**
	 * @param userWorkspaceId
	 * @return
	 */
	public UserWorkspace findUserWorkspaceById(Integer userWorkspaceId);
	/** 
	 * Delete {@code User Workspace}
	 * @param userWorkspace
	 */
	public void delete(UserWorkspace userWorkspace);
	/**
	 * Save user workspace object.
	 * @param userworkspace
	 * @return {@code UserWorkspace}
	 */
	public UserWorkspace save(UserWorkspace userworkspace);
	/**
	 * delete user from workspace.
	 * @param workspace
	 * @param idOfUser
	 */
	public void deleteUserFromWorkspace(Workspace workspace, Integer idOfUser);
	/**
	 * Create user workspace object.
	 * @param user
	 * @param workspace
	 * @return {@code UserWorkspace}
	 */
	public UserWorkspace createNewUserWorkspaceObject(User user, Workspace workspace);
	/**
	 * Find by user.
	 * @param user
	 * @return {@code List<UserWorkspace>}
	 */
	public List<UserWorkspace> findByUser(User user);
	/**
	 * Update User Workspace Object By List Of User and Workspace Object	
	 * @param user
	 * @param workspace
	 * @return {@code UserWorkspace}
	 */
	public UserWorkspace updateUserWorkspaceObject(List<User> user, Workspace workspace);
	/**
	 * @param user
	 * @param workspace
	 * @return
	 */
	public UserWorkspace findByUserAndWorkspace(User user, Workspace workspace);
	/**
	 * @param landingPageName
	 * @return
	 */
	public boolean getStatusForAssetMappingReportHomeLandingButton(String landingPageName);
	/**
	 * @param landingPageName
	 * @param currentId
	 * @return
	 */
	public boolean getStatusForDashboardHomeLandingButton(String landingPageName, Integer currentId);
	
	/**
	 * @param workspaceId
	 * @return
	 */
	public List<UserWorkspace> findByWorkspace(Workspace workspaceId); 
	
	
}
