package org.birlasoft.thirdeye.service;

import java.util.List;

import org.birlasoft.thirdeye.entity.AssetTemplate;

public interface AdminReportsService  {

	public List<AssetTemplate> findByAssetTypeid(Integer assetTypeId);

}
