package org.birlasoft.thirdeye.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * This object will be used for JSON serialization
 * 
 * @author samar.gupta
 *
 */
public class JSONMultiChoiceQuestionMapper {

	private List<JSONQuestionOptionMapper> options = new ArrayList<JSONQuestionOptionMapper>();
	private boolean otherOption;

	public JSONMultiChoiceQuestionMapper() {
		super();
	}

	public List<JSONQuestionOptionMapper> getOptions() {
		return options;
	}

	public void setOptions(List<JSONQuestionOptionMapper> options) {
		this.options = options;
	}
	
	public void addOption (JSONQuestionOptionMapper oneOption){
		if (oneOption != null){
			options.add(oneOption);
		}
	}

	public boolean isOtherOption() {
		return otherOption;
	}

	public void setOtherOption(boolean otherOption) {
		this.otherOption = otherOption;
	}

}
