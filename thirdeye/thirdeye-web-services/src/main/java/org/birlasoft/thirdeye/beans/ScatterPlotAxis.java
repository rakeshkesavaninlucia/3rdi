package org.birlasoft.thirdeye.beans;

import org.birlasoft.thirdeye.beans.DataCoordinate.GraphCoordinate;
import org.birlasoft.thirdeye.entity.Questionnaire;

public class ScatterPlotAxis {
	
	
	private ParameterBean parameterBean;
	private Questionnaire questionnaire;
	
	private GraphCoordinate coorindate;
	
	
	public ParameterBean getParameterBean() {
		return parameterBean;
	}
	public void setParameterBean(ParameterBean pb) {
		this.parameterBean = pb;
	}
	public boolean isValid() {
		return parameterBean != null;
	}
	public void setQuestionnaire(Questionnaire questionnaire) {
		this.questionnaire = questionnaire;
		
	}
	public Questionnaire getQuestionnaire() {
		return questionnaire;
	}

	public GraphCoordinate getCoorindate() {
		return coorindate;
	}
	public void setCoorindate(GraphCoordinate coorindate) {
		this.coorindate = coorindate;
	}
	
	
}
