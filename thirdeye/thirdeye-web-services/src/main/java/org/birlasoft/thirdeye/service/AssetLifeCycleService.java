package org.birlasoft.thirdeye.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.beans.AssetLifeCycleStagesBean;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetTemplate;

public interface AssetLifeCycleService {
	
	public List<AssetTemplate> getListOfAssetTemplate();
	/**
	 * @param assetTemplateId
	 * @return
	 */
	public Map<String,Map<String,List<String>>> getAssetlifeCycle(Integer assetTemplateId);
	
	/**
	 * @param assetTemplateId
	 * @return
	 */
	public List<String> getYearRange (Integer assetTemplateId);
		
	/**
	 * @param asset
	 * @return
	 */
	public HashMap<Integer,AssetLifeCycleStagesBean> getJSONMapper(Asset asset);

}
