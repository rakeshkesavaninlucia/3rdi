package org.birlasoft.thirdeye.beans.report;

import java.util.Set;

import org.birlasoft.thirdeye.entity.Report;
import org.birlasoft.thirdeye.util.Utility;

/**
 * @author sunil1.gupta
 *
 */
public class PortfolioHealthJSONConfig {
	
	
	private Integer questionnaireId;
	private Set<Integer> assetTemplateIds;
	private Set<Integer> parameterIds;
	
	public PortfolioHealthJSONConfig() {
		
	}
	
	public PortfolioHealthJSONConfig(Report r) {
		PortfolioHealthJSONConfig phpJSONConfig = Utility.convertJSONStringToObject(r.getReportConfig(), PortfolioHealthJSONConfig.class);
		if (phpJSONConfig != null){
			this.questionnaireId = phpJSONConfig.getQuestionnaireId();
			this.assetTemplateIds = phpJSONConfig.getAssetTemplateIds();
			this.parameterIds = phpJSONConfig.getParameterIds();
		}	
	}
	public Integer getQuestionnaireId() {
		return questionnaireId;
	}
	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}
	public Set<Integer> getAssetTemplateIds() {
		return assetTemplateIds;
	}
	public void setAssetTemplateIds(Set<Integer> assetTemplateIds) {
		this.assetTemplateIds = assetTemplateIds;
	}
	public Set<Integer> getParameterIds() {
		return parameterIds;
	}
	public void setParameterIds(Set<Integer> parameterIds) {
		this.parameterIds = parameterIds;
	}
	
}
