/**
 * 
 */
package org.birlasoft.thirdeye.service;

import org.birlasoft.thirdeye.beans.tco.TcoAnalysisGraphBean;

/**
 * @author shaishav.dixit
 *
 */
public interface TcoAnalysisService {

	public TcoAnalysisGraphBean getTotalCostOfAssets(Integer idOfChartOfAccount);

	public TcoAnalysisGraphBean getDataForCombiGraph(Integer idOfChartOfAccount, Integer idOfQuestionnaire,
			Integer idOfParameter);

}
