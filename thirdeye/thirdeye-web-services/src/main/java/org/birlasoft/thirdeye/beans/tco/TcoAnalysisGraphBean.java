package org.birlasoft.thirdeye.beans.tco;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.search.api.beans.TcoAssetBean;

public class TcoAnalysisGraphBean {
	
	private String graphType;
	private String graphName;
	private List<TcoCombiGraphBean> tcoCombiGraphBeans = new ArrayList<>();
	private List<TcoAssetBean> tcoAssetBeans;

	public List<TcoAssetBean> getTcoAssetBeans() {
		return tcoAssetBeans;
	}

	public void setTcoAssetBeans(List<TcoAssetBean> tcoAssetBeans) {
		this.tcoAssetBeans = tcoAssetBeans;
	}

	public List<TcoCombiGraphBean> getTcoCombiGraphBeans() {
		return tcoCombiGraphBeans;
	}
	
	public void addCombiBean(TcoCombiGraphBean combiGraphBean) {
		tcoCombiGraphBeans.add(combiGraphBean);
	}

	public String getGraphType() {
		return graphType;
	}

	public void setGraphType(String graphType) {
		this.graphType = graphType;
	}
	
	public String getGraphName() {
		return graphName;
	}

	public void setGraphName(String graphName) {
		this.graphName = graphName;
	}
	
}
