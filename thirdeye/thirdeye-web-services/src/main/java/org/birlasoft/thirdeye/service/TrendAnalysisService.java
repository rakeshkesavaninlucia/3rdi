package org.birlasoft.thirdeye.service;

import java.util.Date;
import java.util.List;

import org.birlasoft.thirdeye.beans.TrendWrapper;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.repositories.TrendRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Service Interface for plot scatter graph.
 * 
 * @author samar.gupta
 *
 */

public interface TrendAnalysisService {
	
	/**
	 * Plot scatter graph.
	 * @param questionnaireIds
	 * @param parameterIds
	 * @param idOfQualityGate
	 * @param filterMap 
	 * @return {@code ScatterWrapper}
	 */
	public TrendWrapper plotTrendGraph(String[] questionnaireIds,
			String[] parameterIds,Date startDate1,Date endDate1,Date startDate2,Date endDate2);

	public List<Questionnaire> getQuestionnaireforTrend();
	
    
}
