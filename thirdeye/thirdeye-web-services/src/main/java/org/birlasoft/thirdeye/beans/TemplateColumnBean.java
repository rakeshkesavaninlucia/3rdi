package org.birlasoft.thirdeye.beans;

import org.birlasoft.thirdeye.comparator.Sequenced;

/**
 * Bean for asset template column.
 * @author samar.gupta
 *
 */
public class TemplateColumnBean implements Sequenced {
	
	private String name;
	private String data;
	private int sequenceNumber;
	private String dataType;
	private int assetTemplateColId;
	private boolean mandatory;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public int getAssetTemplateColId() {
		return assetTemplateColId;
	}
	public void setAssetTemplateColId(int assetTemplateColId) {
		this.assetTemplateColId = assetTemplateColId;
	}
	public boolean isMandatory() {
		return mandatory;
	}
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	@Override
	public String toString() {
		return name + " = " + data ;
	}
	
	
}
