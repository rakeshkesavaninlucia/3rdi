package org.birlasoft.thirdeye.service.impl;

import org.birlasoft.thirdeye.beans.SecurityUser;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.security.UserAuthenticationDetailsBean;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Service class for load user by user name and get current user. 
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class CustomUserDetailsServiceImpl implements CustomUserDetailsService {	

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		
		User user = userService.findUserByEmail(userName);
		
		// Check if you have the user and the user is not in the deleted status.
		if(user == null || user.isDeleteStatus()){
			throw new UsernameNotFoundException("UserName "+userName+" not found");
		}
		return new SecurityUser(user);
	}
	
	/**
	 * Get current user.
	 * @return {@code User}
	 */
	@Override
	public User getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null && auth instanceof UsernamePasswordAuthenticationToken) {
			UsernamePasswordAuthenticationToken userNameToken = (UsernamePasswordAuthenticationToken) auth;
			Object principal = userNameToken.getPrincipal();
			
			if (principal instanceof UserDetails){
				String email = ((UserDetails) principal).getUsername();
				
				User loginUser = userService.findUserByEmail(email);
				loginUser.setEmailAddress(email);
				
				return new SecurityUser(loginUser);
			}
		}
		
		return null;
	}
	
	/**
	 * Returns the {@link UserAuthenticationDetailsBean} for the current authenticated user session.
	 * @return
	 */
	@Override
	public UserAuthenticationDetailsBean getCurrentAuthenticationDetails() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null && auth instanceof UsernamePasswordAuthenticationToken) {
			return (UserAuthenticationDetailsBean)(auth.getDetails());
		}

		return null;
	}
	
	/**
	 * Returns the active workspace for the user.
	 * 
	 * @return
	 */
	@Override
	public Workspace getActiveWorkSpaceForUser(){
		 ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		 Workspace workspace = null;
		 if (attr.getRequest().getSession().getAttribute("USER_ACTIVE_WORKSPACE") != null){
			 workspace = (Workspace) attr.getRequest().getSession().getAttribute("USER_ACTIVE_WORKSPACE");
		 }
		 return workspace;
	}
	
}
