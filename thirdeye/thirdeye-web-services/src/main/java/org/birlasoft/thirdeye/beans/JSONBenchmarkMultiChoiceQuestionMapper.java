package org.birlasoft.thirdeye.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * This object will be used for JSON serialization
 * @author samar.gupta
 */
public class JSONBenchmarkMultiChoiceQuestionMapper {

	private List<JSONBenchmarkQuestionOptionMapper> options = new ArrayList<>();

	/**
	 * default constructor 
	 */
	public JSONBenchmarkMultiChoiceQuestionMapper() {
		super();
	}

	public List<JSONBenchmarkQuestionOptionMapper> getOptions() {
		return options;
	}

	public void setOptions(List<JSONBenchmarkQuestionOptionMapper> options) {
		this.options = options;
	}
	
	/**
	 * Add option in list.
	 * @param oneOption
	 */
	public void addOption (JSONBenchmarkQuestionOptionMapper oneOption){
		if (oneOption != null){
			options.add(oneOption);
		}
	}
}
