package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Modal_popup_createcostelement extends Util{

	
	public  Modal_popup_createcostelement(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	//objects of Question Page
		
		
	     
	@FindBy(xpath = "//h4[contains(text(),'Create Cost Elements')][@class='modal-title']")
    public WebElement verify_modal_title;       
	
	@FindBy(xpath = "//*[@id='title']")
    public WebElement Cost_element_name;
	
	@FindBy(xpath = "//*[@id='title']")
    public WebElement Cost_element_name1;
	
	@FindBy(xpath = "//*[@id='helpText']")
    public WebElement Cost_element_desc;
	
	@FindBy(xpath = "//*[@id='createNewCostElementForm']/div[3]/input")
    public WebElement Create_button;
	              
	       
	       

	 
	 public String get_pagetitle()
		{
			String pageTitle = verify_modal_title.getText();
			
			return pageTitle ;
			
			
		}
	       
	       
	    //To verify whether the user is landing on the proper page or not
	 public void pageetitle_verify()
		{
				try{
					String expectedTitle = "Create Cost Elements";
					if(expectedTitle.equals(get_pagetitle()))
					{
						System.out.println("Page Title is"+expectedTitle);
					}
					
				}catch(Exception e){

			      throw new AssertionError("A clear description of the failure", e);
				}
		}
	   		
	   		
	   		
	   		
	   	
	   	
	   	   	
	   	public void Cost_elementname() throws InterruptedException {
	   		Popup_table_close(Cost_element_name);
	   		
	   		}
	   	
	   	public void Cost_elementname1(String cost_desc) {
	   		Cost_element_name1.sendKeys(cost_desc);
	   	}
	
	   	public void Cost_elementdesc(String cost_desc) {
	   		Cost_element_desc.sendKeys(cost_desc);
	   	}
	   		
	   	
	   	
	    public void Click_create_element() {
	   		click_Method(Create_button);
	   		
	   		       }
	
	    
	
	
}
