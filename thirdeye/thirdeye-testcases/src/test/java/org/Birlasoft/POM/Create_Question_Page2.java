package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Create_Question_Page2 extends Util {

	public  Create_Question_Page2(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		//objects of Question Page
		
		
	     
	       
	       @FindBy(xpath = "//*[@id='questionList_filter']/label/input")
		     public WebElement ViewQuestion_Search;
	       
	       @FindBy(xpath = "//*[@id='questionList_filter']/label/input")
		     public WebElement ViewQuestion_Search_input;
	       
	       @FindBy(id = "questionList")
			 WebElement ViewQuestion_click;
	       
	       
	       
	        // @FindBy(id = "questionList")
	        // public WebElement ViewQuestion_table;
	       
	      public void search_Question()
	       {
	    	   click_Method(ViewQuestion_Search);
           }
	              
	       public void Click_input(String Search) throws InterruptedException
	       {
	    	   Webtable_Search(ViewQuestion_Search_input,Search);
	    	   Webtable_element_click(ViewQuestion_click,Search);
           }
	       
	
	
}
