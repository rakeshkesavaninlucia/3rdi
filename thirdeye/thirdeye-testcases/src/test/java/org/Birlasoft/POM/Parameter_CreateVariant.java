package org.Birlasoft.POM;



import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Parameter_CreateVariant extends Util
{
	public Parameter_CreateVariant(WebDriver driver)
    {
		
		PageFactory.initElements(driver,this);
		
	}
	
	/* Locators  */
	
	//page title
	@FindBy(xpath = "//span[contains(text(),'Create a  Variant Parameter')]")
	public WebElement Parameter_CreateVariant_pagetitle;
	
	//uniquename(mandatory)
	@FindBy(id = "name")
	public WebElement Parameter_CreateVariant_Uniquename;
	
	//display name (mandatory)
	@FindBy(id = "displayName")
	public WebElement Parameter_CreateVariant_Displayname;
	
	//Description
	@FindBy(id = "description")
	public WebElement Parameter_CreateVariant_Description;
	
	
	@FindBy(xpath = "//input[@type='submit']")
	public WebElement Parameter_CreateVariant_submitbtn;

	
	public String Varaintget_pagetitle()
	{
		String pageTitle = Parameter_CreateVariant_pagetitle.getText();
		
		return pageTitle ;
		
		
	}
	
	//To verify whether the user is landing on the proper page or not
	public void CreateVariant_pagetitle_verify()
	{
			try{
				String expectedTitle = "Create a  Variant Parameter";
				if(expectedTitle.equals(Varaintget_pagetitle()))
				{
					System.out.println("Page Title is"+expectedTitle);
				}
				
			}catch(Exception e){

		      throw new AssertionError("A clear description of the failure", e);
			}
			
	}
	
	
	public void Parameter_CreateVariant_Uniquename(String textbox) 
	{
		
		if(Parameter_CreateVariant_Uniquename.isDisplayed())
			
			//calling the method settext	
		Set_Text(Parameter_CreateVariant_Uniquename, textbox);
		
		System.out.println("Entered Template name is" +textbox);
		
	 }
	
	
	
	public void Parameter_CreateVariant_Displayname(String textbox) 
	{
			
		if(Parameter_CreateVariant_Displayname.isDisplayed())
				
		//calling the method settext	
		Set_Text(Parameter_CreateVariant_Displayname,textbox);
			
		System.out.println("Entered Template name is" +textbox);
			
	}
	
	//enter the template name

	public void Parameter_CreateVariant_Description(String textbox) 
	{
				
	     if(Parameter_CreateVariant_Description.isDisplayed())
					
	     //calling the method settext	
		 Set_Text(Parameter_CreateVariant_Description, textbox);
				
	     System.out.println("Entered Template name is" +textbox);
				
	}
	
	public void Parameter_CreateVariant_submitbtn()

	{
		if(Parameter_CreateVariant_submitbtn.isDisplayed())
			//calling click method
		click_Method(Parameter_CreateVariant_submitbtn);
		System.out.println("User click on Submit button");
	}



	
}

