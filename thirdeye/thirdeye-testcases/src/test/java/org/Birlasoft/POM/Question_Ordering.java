package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Question_Ordering extends Util{
	
	
	public  Question_Ordering (WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	
	
	        @FindBy(xpath = "//span[contains(text(),'Question Ordering')]")
             public WebElement Question_Ordering;
	
             @FindBy(xpath = "//a[contains(text(),'Next')]")
		     public WebElement Click_next_ordering;
	       
	       

	       
             public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = Question_Ordering.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle = "Question Ordering";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is"+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		
	   	}
	       
	   
	       public void get_clickNext() {
	    	   
		    	  //Webtable_Element_check(Click_Tree,Tree);
		        click_Method(Click_next_ordering);
		        }

}
