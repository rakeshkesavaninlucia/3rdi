package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BenchMark_Item_Score_Create {

	
	
	

	public BenchMark_Item_Score_Create(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
	
	//objects of BenchMark
	
	
	@FindBy(xpath = "//*[@id='addRowButton']")
	 public WebElement BenchMark_Button;
	
	
	
//    @FindBy(xpath = "//*[@id='startYear']")
//		 public WebElement BenchMark_Start_Year;
//    
    @FindBy(xpath="//*[@id='endYear']")
    public WebElement BenchMark_End_Year;	
	
    @FindBy(xpath="//*[@id='startYear']")
	public WebElement BenchMark_Start_Year;
	
  
    @FindBy(xpath="  //*[@id='score']")
    public WebElement BenchMark_Score;
    
    
    @FindBy(xpath="//tbody/tr/td/a[1]")
 	 public WebElement Actions_Benchmark_Save_Button;
    
//    @FindBy(xpath="//tbody/tr/td/a[1]")
  //a[@class='editRow fa fa-pencil-square-o']
    @FindBy(xpath="a[@class='editRow fa fa-pencil-square-o']")
	 public WebElement Actions_Benchmark_Edit_Button;
   
    
    @FindBy(xpath="//tbody/tr/td/a[2]")
 	 public WebElement Item_Score_Delete_Button;
    
    
    @FindBy(xpath=" //*[@id='addRowButton']")
 	 public WebElement Add_Benchmark_Score_Button;
    
//calling all objects


    
    public void BenchMark_Button ()
    {
    	Util.fn_CheckVisibility_Button(BenchMark_Button);
    	
    }
	
    public void get_Text(String BenchMark_Year_Start) {
 	   BenchMark_Start_Year.sendKeys(BenchMark_Year_Start);
    }
    
    public void get_Text1(String BenchMark_Year_End) {
  	   BenchMark_End_Year.sendKeys(BenchMark_Year_End);
     }
	
    public void get_Score(String BenchMark_Scoring) {
   	   BenchMark_Score.sendKeys(BenchMark_Scoring);
      }
	

    
    
    public void Actions_Benchmark_Save_Button()
    {
    	Util.fn_CheckVisibility_Button(Actions_Benchmark_Save_Button);
    }
    
    public void Actions_Benchmark_Edit_Button()
    {
    	Util.fn_CheckVisibility_Button(Actions_Benchmark_Edit_Button);
    }
	
    public void Add_Benchmark_Score_Button()
    {
    	Util.fn_CheckVisibility_Button(Add_Benchmark_Score_Button);
    }
    
    
    public void Actions_Delete_Button() throws Exception
    {
    	Util.Popup_Delete_Alert(Item_Score_Delete_Button);
    }
	
}
