package org.Birlasoft.POM;





import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class Template_CreateTemplate_PageObject extends Util {
	
	public Template_CreateTemplate_PageObject(WebDriver driver)
    {
		
		PageFactory.initElements(driver,this);
		
	}

     //objects of Create template

	
@FindBy(xpath = "//span[contains(text(),'Create a Asset Template')]")	
public WebElement CreateTemplate_title_verify;
	
@FindBy(id = "select2-assetTypeListId-container")	
public WebElement CreateTemplate_Assettype_click;

@FindBy(id = "select2-assetTypeListId-results")	
public WebElement CreateTemplate_Assettype_ajax;

@FindBy(id = "assetTemplateName")	
public WebElement CreateTemplate_template_name;

@FindBy(id = "description")	
public WebElement CreateTemplate_template_description;


@FindBy(xpath = "//input[@type='submit']")	
public WebElement CreateTemplate_template_save;



public String get_createtemplate_Title()
{
	String pageTitle = CreateTemplate_title_verify.getText();
	
	return pageTitle ;
	
}

public void CreateTemplate_title_verify()
{
	try{
		String expectedTitle = "Create a Asset Template";
		if(expectedTitle.equals(get_createtemplate_Title()))
		{
			System.out.println("Page Title is"+expectedTitle);
		}
		
	}catch(Exception e){

      throw new AssertionError("A clear description of the failure", e);
	}
}

public void CreateTemplate_Assettype_click(String Person) throws InterruptedException
{
	if(CreateTemplate_Assettype_click.isDisplayed())
		Thread.sleep(1000);
	CreateTemplate_Assettype_click.click();
	//calling the method ajax auto select
	AjaxAuto_select(CreateTemplate_Assettype_ajax,Person);//method(WebElement name , value)
	

}

public void CreateTemplate_template_name(String textbox) {
	
	if(CreateTemplate_template_name.isDisplayed())
		
		//calling the method settext	
	Set_Text(CreateTemplate_template_name, textbox);
 }

public void CreateTemplate_template_description(String desc)
{
	if(CreateTemplate_template_description.isDisplayed())
		//	Login_Accountid.sendKeys(String.valueOf(i));
		CreateTemplate_template_description.sendKeys(desc);
	
}

public void CreateTemplate_template_save()

{
	if(CreateTemplate_template_save.isDisplayed())
		//calling click method
	click_Method(CreateTemplate_template_save);
}


}