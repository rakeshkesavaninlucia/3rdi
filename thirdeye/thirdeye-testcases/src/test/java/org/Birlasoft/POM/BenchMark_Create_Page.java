package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BenchMark_Create_Page extends Util {
	
	public BenchMark_Create_Page (WebDriver driver) 
	{	
			PageFactory.initElements(driver,this);
			
	}
	

	//objects of BenchMark
	
	
    @FindBy(xpath="//*[@id='userWorkSpaceId']")
	public WebElement BenchMark_Workspace_Dropdown;
    
//  @FindBy(xpath="//*[@id='userWorkSpaceId']")
// 	public WebElement BenchMark_Workspace_Dropdown1;
    
    @FindBy(xpath="//*[@id='name']")
    public WebElement BenchMark_Name;
	
    @FindBy(xpath="//*[@id='addRowButton']")
	 public WebElement BenchMark_Button;
   
    @FindBy(xpath="//*[@id='baseName']")
	public WebElement BenchMark_Base_Name;
	
    @FindBy(xpath="//*[@id='version']")
	 public WebElement BenchMark_Version;
    
    @FindBy(xpath="//*[@id='displayName']")
	 public WebElement BenchMark_Display;
   
    @FindBy(xpath="//tbody/tr/td/a[1]")
  	 public WebElement Actions_Save_Button;
     
    @FindBy(xpath="//a[@class='deleteRow  fa fa-trash-o']")
  	 public WebElement Actions_Delete_Button;
     
    @FindBy(xpath="//a[@class='fa fa-plus-square-o']")
 	 public WebElement Actions_Row_Button;
    
    

     
      //calling all objects

//       public void BenchMark_Workspace ()
//       {
//       	Util.fn_CheckVisibility_Button(BenchMark_Workspace);
//       	
//       }
    
  
    
    
    
       public void BenchMark_Workspace_Dropdown() {
			click_Method(BenchMark_Workspace_Dropdown);
  		}
		
		public void BenchMark_Workspace_Dropdown(String value) {
			Dropdownselect(BenchMark_Workspace_Dropdown,value);
  		}
		

       public void get_Desc(String Workspace_Desc) {
    	   BenchMark_Name.sendKeys(Workspace_Desc);
       }
	
       
       public void BenchMark_Button ()
       {
       	Util.fn_CheckVisibility_Button(BenchMark_Button);
       	System.out.println("Click on the button");
       	
       }
   	  
       
       public void get_Text(String Workspace_Base_Text) {
    	   BenchMark_Base_Name.sendKeys(Workspace_Base_Text);
       }
	
       public void get_Version(String Workspace_Base_Version) {
    	   BenchMark_Version.sendKeys(Workspace_Base_Version);
       }
     
       public void get_Display(String Workspace_Base_Display) {
    	   BenchMark_Display.sendKeys(Workspace_Base_Display);
       }
  
       public void Actions_Save_Button()
       {
       	Util.fn_CheckVisibility_Button(Actions_Save_Button);
       }
       
       public void Actions_Delete_Button() throws Exception
       {
       	Util.Popup_Delete_Alert(Actions_Delete_Button);
       }
       
       public void Actions_Row_Button()
       {
       	Util.fn_CheckVisibility_Button(Actions_Row_Button);
       }
       
       
       }
       
       

