package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Publish_Questionnaire extends Util

{

	
	public  Publish_Questionnaire (WebDriver driver) 
		{
				
				PageFactory.initElements(driver,this);
				
			}
			
		
		
		        @FindBy(xpath = "//span[contains(text(),'Publishing your questionnaire')]")
	             public WebElement Publishing_questionnaire;
		
	             @FindBy(xpath = "//*[@id='publishForm']/div/button")
			     public WebElement Click_Publish;
		       
		       

		       
	             public String get_PageTitle() {
	  	    	   
	  	    	   String pageTitle = Publishing_questionnaire.getText();
	  	   		
	  	   		return pageTitle ;
	  	   		
	  	       }
		       
		       
		    //To verify whether the user is landing on the proper page or not
		   	public void pageetitle_verify()
		   	{
		   		try{
		   			String expectedTitle = "Publishing your questionnaire";
		   			if(expectedTitle.equals(get_PageTitle()))
		   			{
		   				System.out.println("Page Title is"+expectedTitle);
		   			}
		   			
		   		}catch(Exception e){

		   	      throw new AssertionError("A clear description of the failure", e);
		   		}
		   		
		   	}
		       
		   
		       public void get_clickPubQue() {
		    	   
			    	  //Webtable_Element_check(Click_Tree,Tree);
			        click_Method(Click_Publish);
			        }
}
