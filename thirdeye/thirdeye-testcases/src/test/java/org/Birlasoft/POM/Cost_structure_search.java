package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Cost_structure_search extends Util{

	public  Cost_structure_search(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	//objects of Question Page
		
		
	     
	@FindBy(xpath = "//span[contains(text(),'Available Cost Structure')]")
    public WebElement verify_page_title;       
	
	@FindBy(xpath = "//*[@id='DataTables_Table_0_filter']/label/input")
    public WebElement SearchCost_Structure;
	
	@FindBy(xpath = "//tr[1]/td[5]/a")
    public WebElement Edit_table_Cost_Structure;
	              
	       
	       
public String get_PageTitle() 
         {
  	    	   
  	    	   String pageTitle = verify_page_title.getText();
  	   		   return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle ="Available Cost Structure";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is "+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		}
	       
	   	
	   	public void Click_Cost_Struct(String search) {
	   		SearchCost_Structure.sendKeys(search);
	   	 }
	
	   	public void Click_Edit_Cost_Struct() {
	   		click_Method(Edit_table_Cost_Structure);
	   	 }
	   	
}
