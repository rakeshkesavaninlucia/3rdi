package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateCStruct_Modal extends Util{

	public  CreateCStruct_Modal(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	//Handling Cost structure modal popup
		
		     
	
	@FindBy(xpath = "//*[@id='createCostStructureForm']/div[1]/h4")
    public WebElement modal_Structure;
	
	@FindBy(xpath = "//*[@id='uniqueName']")
    public WebElement Unique_name_text;
	              
	@FindBy(xpath = "//*[@id='displayName']")
    public WebElement Display_name_text;   
	   
	@FindBy(xpath = "//*[@id='description']")
    public WebElement Desc_text;
	
	@FindBy(xpath = "//input[@value='Create Cost Structure']")
    public WebElement Create_button;
	
	 	
	public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = modal_Structure.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle ="Create Cost Structure";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is "+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		}
	       
	   	
	   	
	   	   	
	   	public void Cost_StructureModal() throws InterruptedException {
	   		Popup_table_close(modal_Structure);
	   		
	   		}
	  	
	   	
	   	public void Uname_Cost_Struct(String cost_str) {
	   			Unique_name_text.sendKeys(cost_str);
		   	}
	   		
	   			      
	   	public void Dname_Cost_Struct(String cost_str) {
	   		Display_name_text.sendKeys(cost_str);
	   	}

	   	public void Desc_Cost_Struct(String cost_str) {
	   		Desc_text.sendKeys(cost_str);
	   	}
	   	
	   	public void Create_Cost_Struct_button_click() {
	   		click_Method(Create_button);
	   	}

}
	  	
	  	
	

