package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.util.Map;

import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.Template_CreateTemplate_PageObject;
import org.Birlasoft.POM.Template_Inventory;
import org.Birlasoft.POM.Template_Inventory_AddAsset;
import org.Birlasoft.POM.Template_ViewTemplate_Pageobject;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Us25Tc001CreateTemplateAddAsset extends Util
{
	

	@BeforeMethod
	public void launchApp()
	{
		
		browser();
		
	}
	
	@Test(dataProvider="dp_us2120Tc1",dataProviderClass=org.Birlasoft.DataProvider.DataProviderLoadData.class)
	public void createTemplateandAsset(Map<String,String> addAsset) throws InterruptedException
	{
		String Username=addAsset.get("Username");
		String Password=addAsset.get("Password");
		String AccountId=addAsset.get("Tenant_ID");
		String Order_ID=addAsset.get("Order_Set");
		String TC_ID=addAsset.get("TC_ID");
		String Asset_Type=addAsset.get("Asset_Type");
		String Template_Name=addAsset.get("Template_Name");
		String Description=addAsset.get("Description");
		String Asset_Column1type=addAsset.get("Asset_Column1type");
		String Asset_Column1=addAsset.get("Asset_Column1");
		String Asset_column1Response1=addAsset.get("Asset_column1Response1");
		String Asset_column1Response2=addAsset.get("Asset_column1Response2");
		String Workspace=addAsset.get("Workspace");
		
		// create a object reference for the page objects involved in this scenario
		Login_page_object loginpage =new Login_page_object(driver);
		Home_PageObjecet homepage=new Home_PageObjecet(driver);
		Template_CreateTemplate_PageObject createtemplatepg = new Template_CreateTemplate_PageObject(driver);
		Template_ViewTemplate_Pageobject viewpage = new Template_ViewTemplate_Pageobject(driver);
		Template_Inventory viewAssetData =new  Template_Inventory(driver);
		Template_Inventory_AddAsset addAssetpage = new Template_Inventory_AddAsset(driver);
		
		//sequence of flow
		//1 .login into the application
		Thread.sleep(2000);
		loginpage.Login_username(Username);
		loginpage.Login_Password(Password);
		loginpage.Login_Accountid1(AccountId);
		loginpage.Login_Submit();
		Thread.sleep(2000);
		
		//2.navigate to create template page
		homepage.Homepage_Workspace_change(Workspace);
		Thread.sleep(2000);
		homepage.Homepage_Configuration();
		homepage.Homepage_TemplateManagement();
		homepage.Homepage_TemplateManagement_CreateTemplate();
		Thread.sleep(2000);
		//3.creating a template
		createtemplatepg.CreateTemplate_template_name(Template_Name);
		createtemplatepg.CreateTemplate_template_description(Description);
		Thread.sleep(2000);
		createtemplatepg.CreateTemplate_Assettype_click(Asset_Type);
		Thread.sleep(2000);
		createtemplatepg.CreateTemplate_template_save();
		
		viewpage.createdTemplateVerifyintheTable(Template_Name,"Verify");
		
		homepage.Homepage_TemplateManagement_Inventory();
		Thread.sleep(2000);
		homepage.Homepage_TemplateManagement_Inventorylist(Template_Name);
		//
		viewAssetData.Inventory_AddAsset_Btn_clk();
		addAssetpage.Assetform(Asset_Column1type, Asset_Column1,Asset_column1Response1);
		addAssetpage.Inventory_addasset_submit_btn();
		addAssetpage.Assetform(Asset_Column1type, Asset_Column1,Asset_column1Response2);
		addAssetpage.Inventory_addasset_submit_btn();
		addAssetpage.Inventory_addasset_back_btn();
		
		viewAssetData.assetVerify(Asset_column1Response1,"Verify");
		
		viewAssetData.assetVerify(Asset_column1Response2,"Verify");
	}
	
}
