package org.Birlasoft.Testcase.org.Birlasoft.Testcase;



import java.io.IOException;

import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
//import org.Pom.Parameter_CreateParameter;
//import org.Birlasoft.POM.Home_PageObjecet;
//import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.Parameter_CreateParameter;
import org.Birlasoft.POM.Parameter_CreateParameterpage;
import org.Birlasoft.POM.Parameter_CreateVariant;
import org.Birlasoft.POM.Parameter_UpdateParameter;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC_Parameter extends Util {
	
	@BeforeClass
	public void browserinit() throws IOException
	{
		//intialization of the browser
		browser();
		
	}
	
    @Test(priority = 0)
    public void f() throws InterruptedException  {
    	
  	  
    	
		//login page
		Login_page_object obj1 = new Login_page_object(driver);
		obj1.verifyLogInPageTitle();
		
		Thread.sleep(1000);
		
		obj1.Login_username("rakesh1.k@birlasoft.com");
		
		Thread.sleep(2000);
		obj1.Login_Password("welcome12#");
		Thread.sleep(2000);
		obj1.Login_Accountid1("qadd");
		Thread.sleep(1000);
		obj1.Login_Submit();
		//driver.navigate().to("http://130.1.4.252:8080/home"); 
		obj1.Login_ErrorMsg();
		Thread.sleep(1000);
		
		
}	
	  
    @Test(priority = 1)
    public void Naviagate_To_Parameter()
	{
		 Home_PageObjecet obj2 = new Home_PageObjecet(driver);
		 obj2.Homepage_titleverify("Portfolio Home");
		 obj2.Homepage_ParameterManagement();
		 obj2.Homepage_parameterManagement_ViewParameter();
	}
    
     @Test(priority = 2)
	public void Create_Parameter() throws InterruptedException
	{
		Parameter_CreateParameter obj3 = new Parameter_CreateParameter(driver);
		Thread.sleep(1000);
		obj3.pageetitle_verify();
		obj3.Create_Parameter_CrtParamBtn();
			
	}
	
	
	
    @Test(priority = 3)
	public void Create_AP_Paramter() throws InterruptedException
	{
		Parameter_CreateParameterpage obj4 = new Parameter_CreateParameterpage(driver);
		obj4.CreateParameter_pagetitle_verify();
		obj4.CreateParameter_Parametertype_click("Aggregate Parameter - Parameter of Parameters");
		Thread.sleep(1000);
		obj4.CreateParameter_Uniquename("AP_Param_Auto_Dec_12"+java.time.LocalDateTime.now());
		Thread.sleep(1000);
		obj4.CreateParameter_Displayname("AP_Automation_Dec_12");
		Thread.sleep(1000);
		obj4.CreateParameter_Description("Create AP Parameter through Automation");
		Thread.sleep(1000);
		obj4.CreateParameter_AP_Btn();
		
		//obj4.CreateParameter_AP_Parameter_Popup_pagetitle_verify();
		obj4.CreateParameter_Allpopup_Radiobutton1("App _Governance");
		obj4.CreateParameter_AP_Submit();
		Thread.sleep(1000);
		obj4.CreateParameter_AP_Btn();
		obj4.CreateParameter_Allpopup_Radiobutton1("Tech_Stability");
		obj4.CreateParameter_AP_Submit();
		Thread.sleep(1000);
		obj4.CreateParamete_Weight1strow("0.5");
		obj4.CreateParamete_Weight2ndrow("0.5");
		obj4.CreateParameter_submitbtn();
	}
	
	
	
  @Test(priority = 4)
    public void AP_Parameter_Variant_Verify() throws InterruptedException
    {
    	Parameter_CreateParameter obj7 = new Parameter_CreateParameter(driver);
    	obj7.pageetitle_verify();
    	Thread.sleep(1000);
    	obj7.Create_Parameter_Table_parameter_verify("AP_Automation_Dec_12 - *");
    	Thread.sleep(1000);
    	obj7.Create_Parameter_Variant_icon("AP_Automation_Dec_12");
    	System.out.println("User click on varaint");
    }
    

@Test(priority = 5)
    public void AP_Parameter_CreateVariant() throws InterruptedException
    {
    	Parameter_CreateVariant obj6 = new Parameter_CreateVariant(driver);
    	Thread.sleep(1000);
    	obj6.CreateVariant_pagetitle_verify();
    	obj6.Parameter_CreateVariant_Uniquename("Variant_AP_Param_Auto_Dec_12");
    	obj6.Parameter_CreateVariant_Displayname("Variant_AP_Automation_Dec_12");
    	obj6.Parameter_CreateVariant_Description("variant AP Parameter theough Automation");
    	obj6.Parameter_CreateVariant_submitbtn();
    }
    
    
@Test(priority = 6)
    public void AP_Parameter_Verify() throws InterruptedException
    {
    	Parameter_CreateParameter obj5 = new Parameter_CreateParameter(driver);
    	obj5.pageetitle_verify();
    	Thread.sleep(1000);
    	obj5.Create_Parameter_Table_parameter_verify("Variant_AP_Param_Auto_Dec_12");
    	Thread.sleep(3000);
    	obj5.Create_Parameter_Update_icon("AP_Automation_Dec_12");
    }

@Test(priority = 7)
    public void AP_Parameter_Update() throws InterruptedException
    {
    	Parameter_UpdateParameter obj8 = new Parameter_UpdateParameter(driver);
    	Thread.sleep(1000);
    	obj8.pageetitle_verify();
    	obj8.UpdateParameter_Displayname("AP_Automation_Dec_12_update");
    	obj8.UpdateParameter_Description("update AP Parameter theough Automation");
    	obj8.UpdateParameter_submitbtn();
    	
    }
@Test(priority = 8)
    public void AP_Parameter_Verify_Treeview_Delete() throws InterruptedException
    {
    	Parameter_CreateParameter obj9 = new Parameter_CreateParameter(driver);
    	Thread.sleep(3000);
    	obj9.Create_Parameter_Table_parameter_verify("AP_Param_Auto_Dec_12");
    	Thread.sleep(1000);
    	obj9.Create_Parameter_Treeview_icon("AP_Param_Auto_Dec_12");
  
    	//obj9.Create_Parameter_Table_parameter_verify("Variant_AP_Param_Auto_Sep-14");
    	obj9.Create_Parameter_Delete_icon("Variant_AP_Param_Auto_Dec_12");
    	 
    	Thread.sleep(1000);
    	obj9.Create_Parameter_Delete_icon("AP_Automation_Dec_12_update");
    	
    }


@Test(priority = 9)
    public void LP_Parameter() throws InterruptedException
    {
		Parameter_CreateParameter obj3a = new Parameter_CreateParameter(driver);
		Thread.sleep(1000);
		obj3a.pageetitle_verify();
		obj3a.Create_Parameter_CrtParamBtn();
		
    }	


@Test(priority = 10)
    public void Create_LP_Parameter() throws InterruptedException
    {
		Parameter_CreateParameterpage obj4a = new Parameter_CreateParameterpage(driver);
		obj4a.CreateParameter_pagetitle_verify();
		obj4a.CreateParameter_Parametertype_click("Leaf Parameter - Parameter of Questions");
		Thread.sleep(1000);
		obj4a.CreateParameter_Uniquename("LP_Param_Auto_Dec_12");
		Thread.sleep(1000);
		obj4a.CreateParameter_Displayname("LP_Automation_Dec_12");
		Thread.sleep(1000);
		obj4a.CreateParameter_Description("Create LP Parameter through Automation");
		Thread.sleep(1000);
		obj4a.CreateParameter_LP_Btn();
		
		//obj4a.LPPopup_pagetitle_verify();
		obj4a.CreateParameter_LPpopup_Radiobutton2("Is the application a host for any master data?  If so, what?");
		obj4a.CreateParameter_LP_Submit();
		Thread.sleep(1000);
		obj4a.CreateParameter_LP_Btn();
		
		
		obj4a.CreateParameter_LPpopup_Radiobutton2("Number of  Data Archival processes defined for this application?");
		obj4a.CreateParameter_LP_Submit();
		Thread.sleep(1000);
		obj4a.CreateParamete_Weight1strow("0.5");
		obj4a.CreateParamete_Weight2ndrow("0.5");
		obj4a.CreateParameter_submitbtn();
		
		
    }
    
    @Test(priority = 11)
    public void LP_Parameter_Variant_Verify() throws InterruptedException
    {
    	Parameter_CreateParameter obj7a = new Parameter_CreateParameter(driver);
    	obj7a.pageetitle_verify();
    	Thread.sleep(1000);
    	obj7a.Create_Parameter_Table_parameter_verify("LP_Param_Auto_Dec_12");
    	Thread.sleep(1000);
    	obj7a.Create_Parameter_Variant_icon("LP_Param_Auto_Dec_12");
    	System.out.println("User click on varaint");
    }
    

@Test(priority = 12)
    public void LP_Parameter_CreateVariant() throws InterruptedException
    {
    	Parameter_CreateVariant obj6a = new Parameter_CreateVariant(driver);
    	Thread.sleep(1000);
    	obj6a.CreateVariant_pagetitle_verify();
    	obj6a.Parameter_CreateVariant_Uniquename("Variant_LP_Param_Auto_Dec_12");
    	obj6a.Parameter_CreateVariant_Displayname("Variant_LP_Automation_Dec_12");
    	obj6a.Parameter_CreateVariant_Description("variant LP Parameter theough Automation");
    	obj6a.Parameter_CreateVariant_submitbtn();
    }
    
    
@Test(priority = 13)
    public void LP_Parameter_Verify() throws InterruptedException
    {
    	Parameter_CreateParameter obj5a = new Parameter_CreateParameter(driver);
    	obj5a.pageetitle_verify();
    	Thread.sleep(1000);
    	obj5a.Create_Parameter_Table_parameter_verify("Variant_LP_Param_Auto_Dec_12");
    	Thread.sleep(3000);
    	obj5a.Create_Parameter_Update_icon("LP_Automation_Dec_12");
    }

@Test(priority = 14)
    public void LP_Parameter_Update() throws InterruptedException
    {
    	Parameter_UpdateParameter obj8a = new Parameter_UpdateParameter(driver);
    	Thread.sleep(1000);
    	obj8a.pageetitle_verify();
    	obj8a.UpdateParameter_Displayname("LP_Automation_Dec_12_update");
    	obj8a.UpdateParameter_Description("update LP Parameter theough Automation");
    	obj8a.UpdateParameter_submitbtn();
    	
    }
@Test(priority = 15)
    public void LP_Parameter_Verify_Treeview_Delete() throws InterruptedException
    {
    	Parameter_CreateParameter obj9a = new Parameter_CreateParameter(driver);
    	Thread.sleep(3000);
    	obj9a.Create_Parameter_Table_parameter_verify("LP_Param_Auto_Dec_12");
    	Thread.sleep(1000);
    	obj9a.Create_Parameter_Treeview_icon("LP_Param_Auto_Dec_12");
  
    	//obj9.Create_Parameter_Table_parameter_verify("Variant_AP_Param_Auto_Sep-14");
    	obj9a.Create_Parameter_Delete_icon("Variant_LP_Param_Auto_Dec_12");
    	 
    	Thread.sleep(1000);
    	obj9a.Create_Parameter_Delete_icon("LP_Automation_Dec_12_update");
    	
    }
     
@Test(priority = 16)
	public void FC_Parameter() throws InterruptedException
	{
		Parameter_CreateParameter obj3b = new Parameter_CreateParameter(driver);
		Thread.sleep(1000);
		obj3b.pageetitle_verify();
		obj3b.Create_Parameter_CrtParamBtn();
	
	}
	
@Test(priority = 17)	
    public void Create_FC_Parameter() throws InterruptedException
    {
		Parameter_CreateParameterpage obj4b = new Parameter_CreateParameterpage(driver);
		obj4b.CreateParameter_pagetitle_verify();
		obj4b.CreateParameter_Parametertype_click("Functional Coverage - Parameter of Functional Map");
		Thread.sleep(1000);
		obj4b.CreateParameter_Uniquename("FC_Param_Auto_Dec_12");
		Thread.sleep(1000);
		obj4b.CreateParameter_Displayname("FC_Automation_Dec_12");
		Thread.sleep(1000);
		obj4b.CreateParameter_Description("Create FC Parameter theough Automation");
		Thread.sleep(1000);
		obj4b.CreateParameter_FC_Btn();
		
		//obj4b.FmapPopup_pagetitle_verify();
		obj4b.CreateParameter_FCPopup_Radiobutton("Fmap test 1");
		//obj4b.CreateParameter_Allpopup_Radiobutton1("Fmap test 1");
		obj4b.CreateParameter_FC_Submit();
		Thread.sleep(1000);
		obj4b.CreateParameter_Level("L2");
		obj4b.CreateParameter_submitbtn();
		
		
    }
    
   
 
    
    
@Test(priority = 18)
    public void FC_Parameter_Verify() throws InterruptedException
    {
    	Parameter_CreateParameter obj5b = new Parameter_CreateParameter(driver);
    	obj5b.pageetitle_verify();
    	Thread.sleep(1000);
    	obj5b.Create_Parameter_Table_parameter_verify("FC_Param_Auto_Dec_12");
    	Thread.sleep(3000);
    	obj5b.Create_Parameter_Update_icon("FC_Automation_Dec_12");
    }

@Test(priority = 19)
    public void FC_Parameter_Update() throws InterruptedException
    {
    	Parameter_UpdateParameter obj8b = new Parameter_UpdateParameter(driver);
    	Thread.sleep(1000);
    	obj8b.pageetitle_verify();
    	obj8b.UpdateParameter_Displayname("FC_Automation_Dec_12_update");
    	obj8b.UpdateParameter_Description("update FC Parameter theough Automation");
    	obj8b.UpdateParameter_submitbtn();
    	
    }
@Test(priority = 20)
    public void FC_Parameter_Verify_Treeview_Delete() throws InterruptedException
    {
    	Parameter_CreateParameter obj9b = new Parameter_CreateParameter(driver);
    	Thread.sleep(3000);
    	obj9b.Create_Parameter_Table_parameter_verify("FC_Automation_Dec_12_update");
    	Thread.sleep(1000);
    	//obj9b.Create_Parameter_Treeview_icon("FC_Automation_01_update");
  
    	//obj9.Create_Parameter_Table_parameter_verify("Variant_AP_Param_Auto_Sep-14");
    	//obj9b.Create_Parameter_Delete_icon("Variant_FC_Param_Auto_Sep-19");
    	 
    	Thread.sleep(1000);
    	obj9b.Create_Parameter_Delete_icon("FC_Automation_Dec_12_update");
    	
    }
	
	@AfterClass(alwaysRun=true)
	public void Parameter_CloseBrowser()
	{
		
		closeBrowser();
		QuitBrowser();
	}

    
    
    
}

