package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.io.IOException;
import org.Birlasoft.POM.Filter_Check;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.Select_Filter_checkbox;
import org.Birlasoft.Utility.Util;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC_Filter extends Util{

	
	@BeforeClass
	public void browserinit() throws IOException{
		//intialization of the browser
		browser();
	}
	

	@Test
	  (priority = 0)
	  public void login_page() throws InterruptedException  {
		
		  
		
			//login page
			Login_page_object obj1 = new Login_page_object(driver);
			obj1.verifyLogInPageTitle();
			Thread.sleep(1000);
		    obj1.Login_username("shrivas.manoj@birlasoft.com");
			Thread.sleep(2000);
			obj1.Login_Password("welcome12#");
			Thread.sleep(2000);
			obj1.Login_Accountid1("qadd");
			Thread.sleep(1000);
			obj1.Login_Submit();
			//driver.navigate().to("http://130.1.4.252:8080/home"); 
			obj1.Login_ErrorMsg();
			Thread.sleep(1000);
}


	@Test
	  (priority = 1)
	public void checking_filter_image() throws InterruptedException
	{
		
		Filter_Check filImg = new Filter_Check(driver);
		filImg.pageetitle_verify();
		filImg.Click_on_filter_image();
	
			
	}

	@Test
	  (priority = 2)
	public void Click_Filter_Checkbox() throws InterruptedException
	{
		
		Select_Filter_checkbox Choosefill = new Select_Filter_checkbox(driver);
		Choosefill.pageetitle_verify();
		Choosefill.Click_On_Server();
		Choosefill.Check_facets("ServerType","ServerVersion");
		Choosefill.Click_on_facet();
		Choosefill.Click_on_facet_filter("213(1)","6(1)");
		
	}



}
