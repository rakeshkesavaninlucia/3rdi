package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.AssetAuditTrail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AssetAuditTrailRepository extends JpaRepository<AssetAuditTrail, Serializable> {

	public AssetAuditTrail findByassetId(Integer assetId);
	
	public List<AssetAuditTrail> findByWorkspaceId(Integer workspaceId);
	@Query("Select aat FROM AssetAuditTrail aat where aat.createdDate between ?1 and ?2 and workspaceId = ?3")
	public List<AssetAuditTrail> findByCreatedDateBetweenAndWorkspaceId(Timestamp startDate,Timestamp endDate,Integer workspaceId);	
	
	public List<AssetAuditTrail> findByCreatedByInAndWorkspaceId(Set<String> userIds,Integer workspaceId);	
	
	public List<AssetAuditTrail> findByEventNameInAndWorkspaceId(List<String> eventName,Integer workspaceId);
	
	public List<AssetAuditTrail> findByCreatedDateBetweenAndCreatedByInAndEventNameInAndWorkspaceId(Timestamp startDate,Timestamp endDate,Set<String> userIds,List<String> eventName,Integer workspaceId);
	
	public List<AssetAuditTrail> findByCreatedDateBetweenAndCreatedByInAndWorkspaceId(Timestamp startDate,Timestamp endDate,Set<String> userIds,Integer workspaceId);
	
	public List<AssetAuditTrail> findByCreatedDateBetweenAndEventNameInAndWorkspaceId(Timestamp startDate,Timestamp endDate,List<String> eventName,Integer workspaceId);
	
	public List<AssetAuditTrail> findByCreatedByInAndEventNameInAndWorkspaceId(Set<String> userIds,List<String> eventName,Integer workspaceId);
	
	public List<AssetAuditTrail> findByAssetIdInAndWorkspaceId(Set<Integer> assetIds,Integer workspaceId);
	
	public List<AssetAuditTrail> findByEventNameInAndAssetIdInAndWorkspaceId(List<String> eventName,Set<Integer> assetIds,Integer workspaceId);
	
	public List<AssetAuditTrail> findByCreatedByInAndAssetIdInAndWorkspaceId(Set<String> userIds,Set<Integer> assetIds,Integer workspaceId);
	
	public List<AssetAuditTrail> findByCreatedDateBetweenAndAssetIdInAndWorkspaceId(Timestamp startDate,Timestamp endDate,Set<Integer> assetIds,Integer workspaceId);
	
	public List<AssetAuditTrail> findByCreatedByAndAssetIdInAndEventNameInAndWorkspaceId(Set<String> userIds,Set<Integer> assetIds,List<String> eventName,Integer workspaceId);
	
	public List<AssetAuditTrail> findByCreatedDateBetweenAndAssetIdInAndEventNameInAndWorkspaceId(Timestamp startDate,Timestamp endDate,Set<Integer> assetIds,List<String> eventName,Integer workspaceId);
	
	public List<AssetAuditTrail> findByCreatedDateBetweenAndAssetIdInAndCreatedByInAndWorkspaceId(Timestamp startDate,Timestamp endDate,Set<Integer> assetIds,Set<String> userIds,Integer workspaceId);
	
	public List<AssetAuditTrail> findByCreatedDateBetweenAndAssetIdInAndEventNameInAndCreatedByInAndWorkspaceId(Timestamp startDate,Timestamp endDate,Set<Integer> assetIds,List<String> eventName,Set<String> userIds,Integer workspaceId);
	
	
}
