package org.birlasoft.thirdeye.repositories;

import java.util.List;

import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserWorkspace;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *	Repository interface for user workspace.
 */
public interface UserWorkspaceRepository extends JpaRepository<UserWorkspace, Integer> {
	/**
	 * Find by user.
	 * @param user
	 * @return {@code List<UserWorkspace>}
	 */
	public List<UserWorkspace> findByUser(User user);
	
	/**
	 * @param user
	 * @param workspace
	 * @return
	 */
	public UserWorkspace findByUserAndWorkspace(User user, Workspace workspace);
	
	/**
	 * @param workspace
	 * @return
	 */
	public List<UserWorkspace> findByWorkspace(Workspace workspaceId);
}
