package org.birlasoft.thirdeye.repositories;

import org.birlasoft.thirdeye.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface for category.
 * @author samar.gupta
 */
public interface CategoryRepository extends JpaRepository<Category, Integer> {
	
	/**
	 * @author dhruv.sood
	 * @param name
	 * @return
	 */
	public Category findByName(String name);
}
