package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;

import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.entity.FunctionalMap;
import org.birlasoft.thirdeye.entity.FunctionalMapData;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author sunil1.gupta
 *
 */
public interface FunctionalMapDataRepository extends JpaRepository<FunctionalMapData, Serializable> {

	/**
	 * @param fm
	 * @return
	 */
	public List<FunctionalMapData> findByFunctionalMap(FunctionalMap fm);

	/** 
	 * @param bcmLevel
	 * @return
	 */
	public List<FunctionalMapData> findByBcmLevel(BcmLevel bcmLevel);
	/**
	 * Fetch list of functional map data by list of qe.
	 * @param listOfQuestionnaires
	 * @return {@code List<FunctionalMapData>}
	 */
	public List<FunctionalMapData> findByQuestionnaireIn(List<Questionnaire> listOfQuestionnaires);
	/**
	 * find By FunctionalMap And Questionnaire.
	 * @param fm
	 * @param questionnaire
	 * @return {@code List<FunctionalMapData>}
	 */
	public List<FunctionalMapData> findByFunctionalMapAndQuestionnaire(FunctionalMap fm, Questionnaire questionnaire);
}
