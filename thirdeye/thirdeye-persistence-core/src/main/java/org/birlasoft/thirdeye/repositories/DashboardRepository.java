package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;

import org.birlasoft.thirdeye.entity.Dashboard;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 
 * Repository class for Dashboard Controller
 *
 */
public interface DashboardRepository extends JpaRepository<Dashboard, Serializable> {
}
