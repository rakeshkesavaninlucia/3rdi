package org.birlasoft.thirdeye.controller;

import java.util.List;

import org.birlasoft.thirdeye.beans.JSONMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONNumberQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONQuestionOptionMapper;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.service.QuestionService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for {@code question type} which will be {@code TEXT}, {@code PARATEXT}, {@code Multiple choice} and {@code Check boxes}.
 * @author samar.gupta
 */
@Controller
@RequestMapping(value="/questionType")
public class QuestionTypeController {
	
	private QuestionService questionService;
	private final WorkspaceSecurityService workspaceSecurityService;
	private static final String TEMPLATE_FRAGMENT_MULTIPLECHOICE = "question/questionTemplateFragement :: MULTIPLECHOICE";
	private static final String TEMPLATE_FRAGMENT_TEXT = "question/questionTemplateFragement :: TEXT";
	private static final String TEMPLATE_FRAGMENT_PARATEXT = "question/questionTemplateFragement :: PARATEXT";
	private static final String TEMPLATE_FRAGMENT_ADD_OPTION = "question/questionTemplateFragement :: addOption";
	private static final String TEMPLATE_FRAGMENT_DATE = "question/questionTemplateFragement :: DATE";
    private static final String JSON_NUMBER_QUESTION_MAPPER = "jsonNumberQuestionMapper";
	/**
	 * Constructor to Initialize services
	 * @param questionService
	 * @param workspaceSecurityService
	 */
	@Autowired
	public QuestionTypeController(QuestionService questionService,
			WorkspaceSecurityService workspaceSecurityService) {
		this.questionService = questionService;
		this.workspaceSecurityService = workspaceSecurityService;
	}

	/**
	 * To show fragment for input text field.
	 * @return {@code String}
	 */
	@RequestMapping(value = "/TEXT", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTION_MODIFY', 'QUESTION_VIEW'})")
	public String textFragment() {
		return TEMPLATE_FRAGMENT_TEXT;
	}
	
	/**
	 * To show fragment for paragraph text area field.
	 * @return {@code String}
	 */
	@RequestMapping(value = "/PARATEXT", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTION_MODIFY', 'QUESTION_VIEW'})")
	public String paraTextFragment() {
		return TEMPLATE_FRAGMENT_PARATEXT;
	}
	
	/**
	 * To show fragment for multiple choices of radio button.
	 * @return {@code String}
	 */
	@RequestMapping(value = "/MULTCHOICE", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTION_MODIFY', 'QUESTION_VIEW'})")
	public String multipleChoiceFragment() {
		return TEMPLATE_FRAGMENT_MULTIPLECHOICE;
	}
	
	/**
	 * To show fragment for multiple choices by Id while update question.
	 * @param idOfQuestion
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/MULTCHOICE/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTION_MODIFY'})")
	public String editMultipleChoiceFragment(@PathVariable(value = "id") Integer idOfQuestion, Model model) {
		Question question = questionService.findOne(idOfQuestion);
		// Check if the user can access this question.
		workspaceSecurityService.authorizeQuestionAccess(question);
		
		String queTypeText = question.getQueTypeText();
		List<JSONQuestionOptionMapper> options;
		boolean otherOption;
		if(queTypeText != null && question.getQuestionType().equalsIgnoreCase(QuestionType.MULTCHOICE.toString())){
			JSONMultiChoiceQuestionMapper mappedObject = Utility.convertJSONStringToObject(queTypeText, JSONMultiChoiceQuestionMapper.class); 
			options = mappedObject.getOptions();
			otherOption = mappedObject.isOtherOption();
		}else{
			return TEMPLATE_FRAGMENT_MULTIPLECHOICE;
		}
		model.addAttribute("otherOption", otherOption);
		model.addAttribute("decodedArrayOfOption", options);
		return "question/questionTemplateFragement :: MULTICHOICE";
	}

	
	/**
	 * To show fragment when add option in multiple choices.
	 * @return {@code String}
	 */
	@RequestMapping(value = "/addOption", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTION_MODIFY', 'QUESTION_VIEW'})")
	public String addOption() {
		return TEMPLATE_FRAGMENT_ADD_OPTION;
	}
	
	/**
	 * To show fragment for input number type field for NUMBER question type.
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/NUMBER", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTION_MODIFY', 'QUESTION_VIEW'})")
	public String numberFragment(Model model) {
		model.addAttribute(JSON_NUMBER_QUESTION_MAPPER, new JSONNumberQuestionMapper());
		return "question/questionTemplateFragement :: NUMBER";
	}
	
	/**
	 * To show fragment for number by Id while update question.
	 * @param model
	 * @param idOfQuestion
	 * @return {@code String}
	 */
	@RequestMapping(value = "/NUMBER/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTION_MODIFY', 'QUESTION_VIEW'})")
	public String editNumberFragment(Model model, @PathVariable(value = "id") Integer idOfQuestion) {
		Question question = questionService.findOne(idOfQuestion);
		// Check if the user can access this question.
		workspaceSecurityService.authorizeQuestionAccess(question);
		
		String queTypeText = question.getQueTypeText();
		if(queTypeText != null && question.getQuestionType().equalsIgnoreCase(QuestionType.NUMBER.toString())){
			JSONNumberQuestionMapper jsonNumberQuestionMapper = Utility.convertJSONStringToObject(question.getQueTypeText(), JSONNumberQuestionMapper.class);
			model.addAttribute(JSON_NUMBER_QUESTION_MAPPER, jsonNumberQuestionMapper);
		}else{
			model.addAttribute(JSON_NUMBER_QUESTION_MAPPER, new JSONNumberQuestionMapper());
		}
		return "question/questionTemplateFragement :: NUMBER";
	}
	
	/**
	 * To show fragment for input date type field for DATE question type.
	 * @return {@code String}
	 */
	@RequestMapping(value = "/DATE", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTION_MODIFY', 'QUESTION_VIEW'})")
	public String dateFragment() {
		return TEMPLATE_FRAGMENT_DATE;
	}
}
