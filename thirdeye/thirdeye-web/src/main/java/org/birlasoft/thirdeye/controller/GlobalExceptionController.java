package org.birlasoft.thirdeye.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;


@ControllerAdvice
public class GlobalExceptionController{
	private static Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionController.class);
	
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ModelAndView handleError405(HttpServletRequest request, Exception e)   {
          /*  ModelAndView mav = new ModelAndView("/405");*/
		LOGGER.error("Error in servicing request",e);
            ModelAndView mav = new ModelAndView();
            mav.addObject("exception", e);
            mav.setViewName("405");
            return mav;
      }
	
	@ExceptionHandler(NullPointerException.class)
    public ModelAndView handleError500(HttpServletRequest request, Exception e)   {
			LOGGER.error("Error in servicing request",e);
            ModelAndView mav = new ModelAndView("/500");
            mav.addObject("exception", e);  
            return mav;
      }
}
