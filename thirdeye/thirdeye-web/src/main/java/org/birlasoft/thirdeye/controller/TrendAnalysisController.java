package org.birlasoft.thirdeye.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.TrendWrapper;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.TrendAnalysisService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for {@code view Trend Analysis} 
 */
@Controller
@RequestMapping(value="/trend")
public class TrendAnalysisController {
	
	private static Logger logger = LoggerFactory.getLogger(TrendAnalysisController.class);
	
	private TrendAnalysisService trendAnalysisService;
	private QuestionnaireService questionnaireService;
	private ParameterService parameterService;
	private final WorkspaceSecurityService workspaceSecurityService;
	private QuestionnaireQuestionService qqService;
	/**
	 * Constructor to initialize services
	 * @param questionnaireService
	 * @param parameterService
	 * @param scatterGraphService
	 * @param customUserDetailsService
	 * @param workspaceSecurityService
	 * @param responseService
	 * @param qqService
	 */
	@Autowired
	public TrendAnalysisController(QuestionnaireService questionnaireService,
			ParameterService parameterService,
			WorkspaceSecurityService workspaceSecurityService,		
			QuestionnaireQuestionService qqService,
			TrendAnalysisService trendAnalysisService) {
		this.questionnaireService = questionnaireService;
		this.parameterService = parameterService;
		this.workspaceSecurityService = workspaceSecurityService;	
		this.qqService = qqService;
		this.trendAnalysisService = trendAnalysisService;
	}


	/**
	 * View graph by Id.
	 * @param model
	 * @param idOfGraph
	 * @return {@code String}
	 */
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_TREND_ANALYSIS'})")
	public String viewGraphByID(Model model	) {
		List<Questionnaire> questionnairesToDisplay = trendAnalysisService.getQuestionnaireforTrend();
		model.addAttribute("listOfQuestionnaire", questionnairesToDisplay);
		return "trend/viewTrendAnalysis";
	}
	
	
	/**
	 * Get root parameter.
	 * @param model
	 * @param idOfQuestionnaire
	 * @return {@code String}
	 */
	@RequestMapping(value="/fetchRootParameters/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_TREND_ANALYSIS'})")
	public String getRootParameter(Model model, @PathVariable(value = "id") Integer idOfQuestionnaire){
		
		// First find the questionnaire
		Questionnaire questionnaire = questionnaireService.findOne(idOfQuestionnaire);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);
		
		// Get root parameters of QE
		List<ParameterBean> listOfRootParameters = parameterService.getRootParametersOfQuestionnaire(questionnaire);
		// You should validate that we only show a list of quantifiable parameters
		// Put the parameters into the modal
		model.addAttribute("rootParamList", listOfRootParameters);
		
		return "trend/trendAnalysisFragment :: optionList(listOfParameters=${rootParamList})";
	}
	
	

	/**
	 * Plot scatter graph.
	 * @param idsOfParameter
	 * @param idsOfQuestionnaire
	 * @param idOfQualityGate
	 * @return {@code ScatterWrapper}
	 */
	@RequestMapping(value="/plot", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_TREND_ANALYSIS'})")
	@ResponseBody
	public TrendWrapper plotGraph(@RequestParam(value = "questionnaireIds") String idsOfQuestionnaire, @RequestParam(value = "parameterIds") String idsOfParameter,
			                      @RequestParam(value = "startDate1",required=false) Date startdate1,
			                      @RequestParam(value = "endDate1",required=false) Date endDate1,
			                      @RequestParam(value = "startDate2",required=false) Date startdate2,
			                      @RequestParam(value = "endDate2",required=false) Date endDate2,HttpServletRequest request){
	
		TrendWrapper trendWrapper = new TrendWrapper();
		String[] questionnaireIds = idsOfQuestionnaire.split(",");
		String[] parameterIds = idsOfParameter.split(",");
 		for (String qeIdInStr : questionnaireIds) {
			Integer qeId = Integer.parseInt(qeIdInStr);
			if(qeId > 0){
				Questionnaire questionnaire = questionnaireService.findOne(qeId);
				// Check if the user can access this questionnaire.
				workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);				
				// Check QQ size if QE does not have QQs.
				List<QuestionnaireQuestion> qqToCheckSize = qqService.findByQuestionnaire(questionnaire, false);
				if(qqToCheckSize.isEmpty()){
					return trendWrapper;
				}
			}
		}
 		try {
 			//Need to implement Trend Analysis graph
 			trendWrapper = trendAnalysisService.plotTrendGraph(questionnaireIds, parameterIds,startdate1,endDate1,startdate2,endDate2);
		} catch (Exception e) {
			logger.info("Something went wrong in Trend Analysis graph - " + e);
		}
		return trendWrapper;
	
	}
}
