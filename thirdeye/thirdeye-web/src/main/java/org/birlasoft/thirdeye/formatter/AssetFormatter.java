package org.birlasoft.thirdeye.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.birlasoft.thirdeye.entity.Asset;
import org.springframework.format.Formatter;

/**
 * {@code Formatter} , which allows you to define custom parse and print capabilities for just about any type
 */
public class AssetFormatter implements Formatter<Asset> {

	@Override
	public String print(Asset asset, Locale locale) {
		return asset.getId().toString();
	}

	@Override
	public Asset parse(String id, Locale locale) throws ParseException {
		Asset asset = new Asset();
		asset.setId(Integer.parseInt(id));
		return asset;
	}
}
