package org.birlasoft.thirdeye.controller.benchmark;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.BenchmarkQuestionBean;
import org.birlasoft.thirdeye.entity.Benchmark;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.BenchmarkService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author samar.gupta
 */	
@Controller
@RequestMapping(value="/benchmark/question")
public class BenchmarkQuestionController {
	
	private BenchmarkService benchmarkService;
	private CustomUserDetailsService customUserDetailsService;
	
	private static final String TEMPLATE_FRAGMENT_MULTIPLECHOICE_BENCHMARK = "benchmark/benchmarkFragment :: viewBenchmarks";
	private static final String TEMPLATE_FRAGMENT_MULTIPLECHOICE_BENCHMARK_ITEM = "benchmark/benchmarkFragment :: viewBenchmarkItems";
	
	/**
	 * Constructor to Initialize services
	 * @param benchmarkService
	 * @param customUserDetailsService
	 */
	@Autowired
	public BenchmarkQuestionController(BenchmarkService benchmarkService,
			CustomUserDetailsService customUserDetailsService) {
		this.benchmarkService = benchmarkService;
		this.customUserDetailsService = customUserDetailsService;
	}

	/**
	 * Fetch benchmark.
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/fetchBenchmark/{workspaceId}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTION_MODIFY', 'QUESTION_VIEW'})")
	public String fetchBenchmark(Model model, @PathVariable("workspaceId") Integer workspaceId) {
		List<Benchmark> listOfBenchmarks = new ArrayList<>();
		if(workspaceId == -1){
			listOfBenchmarks = benchmarkService.findByWorkspaceIsNull();
		} else {
			Set<Workspace> setOfWorkspace = new HashSet<>();
			setOfWorkspace.add(customUserDetailsService.getActiveWorkSpaceForUser());
			listOfBenchmarks = benchmarkService.findByWorkspaceIn(setOfWorkspace);
		}
		model.addAttribute("listOfBenchmark", listOfBenchmarks);
		model.addAttribute("benchmarkQuestionBean", new BenchmarkQuestionBean());
		return TEMPLATE_FRAGMENT_MULTIPLECHOICE_BENCHMARK;
	}
	
	/**
	 * Fetch benchmark item.
	 * @param model
	 * @param benchmarkId
	 * @return {@code String}
	 */
	@RequestMapping(value = "/fetchBenchmarkItem", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTION_MODIFY', 'QUESTION_VIEW'})")
	public String fetchBenchmarkItem(Model model, @RequestParam(value = "benchmarkId") Integer benchmarkId) {
		Benchmark benchmark = benchmarkService.findOne(benchmarkId);
		model.addAttribute("listOfBenchmarkItem", benchmarkService.findByBenchmark(benchmark));
		model.addAttribute("setOfCheckedBenchmarkItemIds", new HashSet<Integer>());
		return TEMPLATE_FRAGMENT_MULTIPLECHOICE_BENCHMARK_ITEM;
	}
}
