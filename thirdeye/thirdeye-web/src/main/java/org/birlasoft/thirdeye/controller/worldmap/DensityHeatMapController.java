package org.birlasoft.thirdeye.controller.worldmap;

import java.util.List;

import org.birlasoft.thirdeye.beans.DensityHeatMapWrapper;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.service.DensityHeatMapService;
import org.birlasoft.thirdeye.service.ScatterGraphService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value="/densityHeatMap")
public class DensityHeatMapController {
	@Autowired
	private DensityHeatMapService densityHeatMapService;
	@Autowired
	private ScatterGraphService scatterGraphService;
	
	
	@Autowired
	public DensityHeatMapController(DensityHeatMapService densityHeatMapService) {
		this.densityHeatMapService = densityHeatMapService;
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'VIEW_DENSITY_HEAT_MAP'})")
	public String viewDensityHeatMap(Model model) {
		List<BcmLevel> level1ToGetDisplay = scatterGraphService.getBcmLevelsforScatter();
		model.addAttribute("bcmLevelList", level1ToGetDisplay);
		return "densityHeatMap/densityHeatMap" ;
	}

	@RequestMapping(value = "/getDensityHeatMapWrapper", method = {RequestMethod.GET,RequestMethod.POST})
	@ResponseBody
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'VIEW_DENSITY_HEAT_MAP'})")
	public List<DensityHeatMapWrapper> worldWrapper(Model model,@RequestParam(value = "idsOfLevel1",required=false) Integer[] idsOfLevel1,@RequestParam(value = "idsOfLevel2",required=false) Integer[] idsOfLevel2,@RequestParam(value = "idsOfLevel3",required=false) Integer[] idsOfLevel3, @RequestParam(value = "idsOfLevel4",required=false) Integer[] idsOfLevel4) {
		List<DensityHeatMapWrapper> densityHeatMapWrapper = densityHeatMapService.getAllAssetsFromAssetTemplatesForActiveWorkSpace(idsOfLevel1,idsOfLevel2,idsOfLevel3,idsOfLevel4);
		return densityHeatMapWrapper ;
	}
}
