package org.birlasoft.thirdeye.controller.asset;


import java.sql.Timestamp;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.entity.AssetAuditTrail;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserWorkspace;
import org.birlasoft.thirdeye.service.AssetAuditTrailService;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.UserWorkspaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value="/assetAuditTrail")
public class AssetAuditTrailController {
	@Autowired
	private AssetAuditTrailService assetAuditTrailService;
	@Autowired 
	private UserWorkspaceService userWorkspaceService;
	@Autowired 
	private CustomUserDetailsService customUserDetailsService;
	@Autowired 
	private AssetTemplateService assetTemplateService;
	
	@RequestMapping( value = "/report", method = RequestMethod.GET ) 
	public String viewLifeCycleReprt(HttpServletRequest request,Model model) {
		List<AssetAuditTrail> listOfAssetAuditTrail = assetAuditTrailService.findByActiveWorkspaceId(customUserDetailsService.getActiveWorkSpaceForUser().getId());
		List<UserWorkspace> listOfUserWorkspace = userWorkspaceService.findByWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
     	List<User> listOfUserInActiveWorkspace =  listOfUserWorkspace.stream().map(onea->onea.getUser()).collect(Collectors.toList());
     	List<AssetTemplate> listOfAssetTemplateInActiveWorkspace = assetTemplateService.listTemplateForActiveWorkspace();
		model.addAttribute("listOfAssetAuditTrail", listOfAssetAuditTrail);
		model.addAttribute("listOfUserInActiveWorkspace", listOfUserInActiveWorkspace);
		model.addAttribute("listOfAssetTemplateInActiveWorkspace", listOfAssetTemplateInActiveWorkspace);
		return "assetAuditTrail/assetAuditTrail";
	}
	
	@RequestMapping( value = "/data", method = RequestMethod.GET ) 
	public String getAssetAuditTrailData(Model model,@RequestParam(value="toDate",required=false) String toDate,@RequestParam(value="fromDate",required=false) String fromDate,
			@RequestParam(value="userIds[]",required=false) Set<String> userIds,@RequestParam(value="eventFilters[]",required=false) List<String> eventFilters,@RequestParam(value="assetTemplateIds[]",required=false) Set<Integer> assetTemplateIds) {
		Timestamp dateToInTimestamp = null;
		Timestamp dateFromInTimestamp  =null;
		if((!fromDate.isEmpty()) && (!toDate.isEmpty())){
			String dateFrom  = fromDate+" 00:00:00";
			String dateTo = toDate+" 23:59:59";
			dateFromInTimestamp = Timestamp.valueOf(dateFrom);
			dateToInTimestamp = Timestamp.valueOf(dateTo);
		}
		List<AssetAuditTrail> listOfAssetAuditTrail = assetAuditTrailService.getAssetAudiTrailData(dateFromInTimestamp, dateToInTimestamp, userIds,eventFilters,assetTemplateIds,customUserDetailsService.getActiveWorkSpaceForUser().getId()); 
		model.addAttribute("listOfAssetAuditTrail", listOfAssetAuditTrail);
		return "assetAuditTrail/assetAuditTrail::assetAuditTrailData";
	}
	
	
}
