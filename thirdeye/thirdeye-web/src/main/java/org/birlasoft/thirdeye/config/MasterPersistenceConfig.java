package org.birlasoft.thirdeye.config;

import java.beans.PropertyVetoException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * Configure {@code hibernate} with {@code jpa} 
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = EntityManagers.MASTERENTITYMANAGER,
		transactionManagerRef = TransactionManagers.MASTERTRANSACTIONMANAGER,
		basePackages="org.birlasoft.thirdeye.master.repository")

// TODO this class should be renamed to MasterPersistenceConfig (delete the existing file
public class MasterPersistenceConfig {

	@Autowired
	private Environment env;


	@Value("${init-db:false}")
	private String initDatabase;


	@Bean(name = TransactionManagers.MASTERTRANSACTIONMANAGER)
	public PlatformTransactionManager transactionManager() throws PropertyVetoException {
		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(entityManagerFactory());
		//    txManager.setPersistenceUnitName("master");
		return txManager;
	}


	@Bean(name = EntityManagers.MASTERENTITYMANAGER)
	public EntityManagerFactory entityManagerFactory() throws PropertyVetoException{
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		//		vendorAdapter.setGenerateDdl(Boolean.TRUE);
		vendorAdapter.setShowSql(Boolean.valueOf(env.getProperty("hibernate.show_sql")));
		vendorAdapter.setDatabasePlatform("org.hibernate.dialect.MySQL5Dialect");

		factory.setDataSource(dataSource());
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan("org.birlasoft.thirdeye.master.entity");

		Properties jpaProperties = new Properties();
		//		jpaProperties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
		factory.setJpaProperties(jpaProperties);

		factory.afterPropertiesSet();
		//		factory.setLoadTimeWeaver(new InstrumentationLoadTimeWeaver());
		return factory.getObject();
	}


	@Bean
	public HibernateExceptionTranslator hibernateExceptionTranslator()
	{
		return new HibernateExceptionTranslator();
	}

	@Bean(name="dataSource")
	public ComboPooledDataSource dataSource() throws PropertyVetoException{
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		dataSource.setDriverClass(env.getProperty("jdbc.driverClassName"));
		dataSource.setJdbcUrl(env.getProperty("jdbc.url") + "?noAccessToProcedureBodies=true");
		dataSource.setUser(env.getProperty("jdbc.username"));
		dataSource.setPassword(env.getProperty("jdbc.password"));
		dataSource.setPreferredTestQuery("SELECT 1");
		dataSource.setTestConnectionOnCheckout(true);
		return dataSource;
	}
}
