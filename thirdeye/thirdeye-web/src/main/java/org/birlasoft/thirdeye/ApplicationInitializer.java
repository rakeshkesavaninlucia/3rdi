package org.birlasoft.thirdeye;

import javax.servlet.Filter;

import org.birlasoft.thirdeye.config.AppConfig;
import org.birlasoft.thirdeye.config.TenantConfiguration;
import org.birlasoft.thirdeye.config.WebMvcConfig;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Class for application initialization of root, servlet configuration and mappings and filters.
 */
public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer  {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class [] {AppConfig.class, WebMvcConfig.class, TenantConfiguration.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
//		return new Class [] {WebMvcConfig.class,WebSecurityConfig.class};
		return null;
	}

	@Override
	protected String[] getServletMappings() {
		return new String[]{"/"};
	}
	
	@Override protected Filter[] getServletFilters() { 
		return new Filter[]{ new DelegatingFilterProxy("springSecurityFilterChain")}; 
	} 
}
