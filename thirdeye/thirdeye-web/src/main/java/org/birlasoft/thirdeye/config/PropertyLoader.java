package org.birlasoft.thirdeye.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**
 * Load all property files. 
 */
@Configuration
@PropertySources({
	@PropertySource("file:${app.home}/config/application-${spring.profiles.active}.properties")
})
public class PropertyLoader {

}
