package org.birlasoft.thirdeye.controller.trend;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.birlasoft.thirdeye.entity.InputTrend;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.TrendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/trend")
public class TrendController {
	@Autowired
	public CustomUserDetailsService customUserDetailsService;
	@Autowired
	public ParameterService parameterService;
	@Autowired
	public TrendService trendService;
	
	@RequestMapping(value = "/input", method = RequestMethod.GET)
	public String getTrendData(Model model) {
		List<Parameter> getListOfParameters = parameterService.findByWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
		List<Parameter> filterAPLPParameter  = new ArrayList<>();
		getListOfParameters.stream().filter(p -> {
	                    if ("AP".equals(p.getType()) || "LP".equals(p.getType())) {
	                    	filterAPLPParameter.add(p);
	                    	return true;
	                    }
                    return false;
	                }).collect(Collectors.toList());
		model.addAttribute("listParameterAPLP", filterAPLPParameter);
		return "trend/inputTrend";
	}
	@RequestMapping(value = "/row", method = RequestMethod.GET)
	public String getRow(Model model) {
		return "trend/inputTrend :: addDateValue";
	}
	
	@RequestMapping(value = "/save/row", method = RequestMethod.GET)
	public String saveRow(Model model,@RequestParam(value="paramId", required=false) Integer paramId,@RequestParam(value = "date") String date,@RequestParam(value = "value") Integer value) {
		String dateFrom  = date+" 00:00:00";
		Timestamp dateFromInTimestamp = Timestamp.valueOf(dateFrom);
		Parameter oneParameter = parameterService.findOne(paramId);
		InputTrend oneTrend = new InputTrend();
		oneTrend.setCreatedDate(new Date());
		oneTrend.setParameter(oneParameter);
		oneTrend.setWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
		oneTrend.setStartDate(dateFromInTimestamp);
		oneTrend.setUpdatedDate(new Date());
		oneTrend.setValue(value);
		oneTrend.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
		oneTrend.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		trendService.save(oneTrend);
		return "trend/inputTrend :: addDateValue";
	}
}
