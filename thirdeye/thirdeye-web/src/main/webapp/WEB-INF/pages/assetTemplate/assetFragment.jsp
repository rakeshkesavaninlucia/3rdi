<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.templatemanagement.inventoryaddasset.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
<div th:fragment = "assetinformation">
		<form th:action="@{/templates/saveAssetDetails}" method="get" id="assetdetailForms">
			<div class="form-group">
				<table class="table table-bordered table-condensed table-hover">
					<tr th:each="assetData : ${asset.assetDatas}">
						<td class="col-md-4 horizontal"><span th:text="${assetData.name}"></span></td>
						<td class="col-md-8">
                       	<input th:if="${assetData.dataType ne T(org.birlasoft.thirdeye.constant.DataType).DATE.name() and (assetData.dataType ne T(org.birlasoft.thirdeye.constant.Constants).JSON_DATA_TYPE)}" th:name="${assetData.assetTemplateColId}" th:type="${assetData.dataType}" class="assetdetails form-control" readonly="readonly" th:value="${assetData.data}"></input>
						<th:block th:if="${assetData.dataType eq T(org.birlasoft.thirdeye.constant.Constants).JSON_DATA_TYPE}" >
						<select class="form-control select2 basicSelect" th:name="'lifeCycle_'+${listofJson.get(assetData.assetTemplateColId).id}" disabled="disabled">
							<option value="">--Select--</option>
							<option th:each="dataType : ${T(org.birlasoft.thirdeye.constant.ApplicationLifeCycleStages).values()}"  th:value="${dataType}" th:text="${dataType}" th:selected="${listofJson.get(assetData.assetTemplateColId).lifeCycle_s.equalsIgnoreCase(dataType)}">
							</option>
	 					   </select>
	 					   <span th:text="From"></span><input type="text" readonly="readonly" th:name="'lifeCycle_f'+${listofJson.get(assetData.assetTemplateColId).id}" class="form-control datepicker" th:value="${listofJson.get(assetData.assetTemplateColId).lifeCycle_f}"   ></input>
							<span th:text="TO"></span><input   type="text"  readonly="readonly" th:name="'lifeCycle_t'+${listofJson.get(assetData.assetTemplateColId).id}" class="form-control datepicker"  th:value="${listofJson.get(assetData.assetTemplateColId).lifeCycle_t}" ></input>
					        </th:block>
						</td>
					</tr>
				</table>
			</div>
		</form>
	</div>
	
	<div  th:fragment="editAssetDetails">
		<form th:action="@{/templates/form/saveAssetDetails}" method="post" id="assetdetailForm">
		<input type="hidden" name="templateId" th:value="${assetTemplateModelId}" />
		<input type="hidden" name="id" th:value="${assetid}" />
			<div class="form-group">
				<table class="table table-bordered table-condensed table-hover">				
					<tr th:each="templatecol : ${assetTemplateModel.assetTemplateColumns}">
						<td class="col-md-4 horizontal"><span th:text="${templatecol.assetTemplateColName}"></span></td>
						<td class="col-md-8">
						<select class="form-control select2 basicSelect" th:if="${templatecol.sequenceNumber eq 2}" th:name="${templatecol.id}" th:type="${templatecol.dataType}" th:maxlength="${templatecol.length}" th:required="${templatecol.mandatory}" data-type="selecttype">
                       <option value ="">--Select--</option>
                       <option th:each ="oneCountryName :${listOfCountries}" th:text="${oneCountryName.countryName}" th:selected="${mapOfColIdAndData.get(templatecol.id).equalsIgnoreCase(oneCountryName.countryName)}"  ></option>
                        </select> 
						<input th:if="${templatecol.dataType ne T(org.birlasoft.thirdeye.constant.DataType).DATE.name() and templatecol.sequenceNumber ne 2 and (templatecol.dataType ne T(org.birlasoft.thirdeye.constant.Constants).JSON_DATA_TYPE)}" th:name="${templatecol.id}" th:type="${templatecol.dataType}" class="form-control" th:maxlength="${templatecol.length}" th:required="${templatecol.mandatory}" th:value="${mapOfColIdAndData.get(templatecol.id)}"></input>
							<input th:if="${templatecol.dataType eq T(org.birlasoft.thirdeye.constant.DataType).DATE.name()}" th:name="${templatecol.id}" type="date" class="form-control" th:maxlength="${templatecol.length}" th:required="${templatecol.mandatory}" th:value="${mapOfColIdAndData.get(templatecol.id)}"></input>
						<th:block th:if="${templatecol.dataType eq T(org.birlasoft.thirdeye.constant.Constants).JSON_DATA_TYPE}" >
						<select class="form-control select2 basicSelect" th:name="'lifeCycle_'+${mapOfAssettempColidandValue.get(templatecol.id).id}">
							<option value="">--Select--</option>
							<option th:each="dataType : ${T(org.birlasoft.thirdeye.constant.ApplicationLifeCycleStages).values()}"  th:value="${dataType}" th:text="${dataType}" th:selected="${mapOfAssettempColidandValue.get(templatecol.id).lifeCycle_s.equalsIgnoreCase(dataType)}">
							</option>
	 					   </select>
	 					   <span th:text="From"></span><input type="text" th:name="'lifeCycle_f'+${mapOfAssettempColidandValue.get(templatecol.id).id}" class="form-control datepicker" th:value="${mapOfAssettempColidandValue.get(templatecol.id).lifeCycle_f}" ></input>
							<span th:text="TO"></span><input   type="text"  th:name="'lifeCycle_t'+${mapOfAssettempColidandValue.get(templatecol.id).id}" class="form-control datepicker" th:value="${mapOfAssettempColidandValue.get(templatecol.id).lifeCycle_t}"  ></input>
					        </th:block>
						</td>
					</tr>
				</table>
				<span th:if="${error eq 1}" style="color: red;">Name already exists</span>
			</div>
		</form>
	</div>
</body>
</html>