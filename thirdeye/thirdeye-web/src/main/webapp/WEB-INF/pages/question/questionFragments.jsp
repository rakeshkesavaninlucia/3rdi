<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	
	<form role="form" th:action="@{/questionnaire/{questionnaireId}/response/save(questionnaireId=${oneQQ.questionnaireId})}" th:attr="data-qqid=${oneQQ.questionnaireQuestionId}" th:fragment="TEXT(oneQQ,iter)" class="editableForm" method="POST">
         <div class="box-body">
           <div class="form-group">
           	<input type="hidden" name="questionnaireQuestionId" th:value="${oneQQ.questionnaireQuestionId}"/>
             <label th:text="${iter}+ ') For '+ ${oneQQ.asset.shortName}">Which asset is this for</label>
             <label th:attr="for=${oneQQ.questionnaireQuestionId}" th:text="${oneQQ.question.title }">Question Title</label>
             <span th:if="${not #maps.isEmpty(mapOfMandatoryQuestion) and #maps.containsKey(mapOfMandatoryQuestion, oneQQ.questionnaireQuestionId)}"><span style="color: red;">*</span></span>
             <p class="help-block" th:text="${oneQQ.question.helpText}">Help Text the question</p>
             <input type="text" class="form-control editableParameter" th:id="${oneQQ.questionnaireQuestionId}" th:name="'qq_' + ${oneQQ.questionnaireQuestionId}" th:value="${oneQQ.responseText}" placeholder="Enter your text Here"  />
           </div>
         </div><!-- /.box-body -->
    </form>
		
   <form role="form" th:action="@{/questionnaire/{questionnaireId}/response/save(questionnaireId=${oneQQ.questionnaireId})}" th:attr="data-qqid=${oneQQ.questionnaireQuestionId}" th:fragment="PARATEXT(oneQQ,iter)" class="editableForm" method="POST">
       <div class="box-body">
         <div class="form-group">
         	<input type="hidden" name="questionnaireQuestionId" th:value="${oneQQ.questionnaireQuestionId}"/>
         	<label th:text="${iter}+ ') For '+ ${oneQQ.asset.shortName}">Which asset is this for</label>
            <label th:attr="for=${oneQQ.questionnaireQuestionId}" th:text="${oneQQ.question.title }">Question Title</label>
            <span th:if="${not #maps.isEmpty(mapOfMandatoryQuestion) and #maps.containsKey(mapOfMandatoryQuestion, oneQQ.questionnaireQuestionId)}"><span style="color: red;">*</span></span>
           <p class="help-block">It doesn't have to be good. Just be frank..</p>
           <textarea class="form-control editableParameter" rows="3" th:name="'qq_' + ${oneQQ.questionnaireQuestionId}" placeholder="Enter ..." th:text="${oneQQ.responseText}"></textarea>
         </div>
       </div><!-- /.box-body -->
   </form>
		             
 	<form role="form" th:action="@{/questionnaire/{questionnaireId}/response/save(questionnaireId=${oneQQ.questionnaireId})}"  th:attr="data-qqid=${oneQQ.questionnaireQuestionId}" th:fragment="MULTCHOICE(oneQQ,iter)" class="editableForm" method="POST">
		 <div class="box-body">
		   <div class="form-group">
		   	<input type="hidden" name="questionnaireQuestionId" th:value="${oneQQ.questionnaireQuestionId}"/>
		    <label th:text="${iter}+ ') For '+ ${oneQQ.asset.shortName}">Which asset is this for</label>
		   	<label th:attr="for=${oneQQ.questionnaireQuestionId}" th:text="${oneQQ.question.title }">Question Title</label>
		   	<span th:if="${not #maps.isEmpty(mapOfMandatoryQuestion) and #maps.containsKey(mapOfMandatoryQuestion, oneQQ.questionnaireQuestionId)}"><span style="color: red;">*</span></span>
		   	<p class="help-block" th:text="${oneQQ.question.helpText}">Help Text the question</p>
		           <div class="radio" th:each="oneOption : ${oneQQ.question.jsonMultiChoiceQuestionMapper.options}">
		             <label >
		             <input type="radio" th:onclick="'javascript:hide(\'otherText_' + ${oneQQ.questionnaireQuestionId}+ '\');'" th:checked="${#maps.containsKey(oneQQ.mapOfResponseData, oneOption.text)}" class="editableParameter" th:name="'qq_' + ${oneQQ.questionnaireQuestionId}" th:value="${oneOption.text}" th:text="${oneOption.text}"/>
		             </label>
		           </div>
		           <div th:if="${oneQQ.question.jsonMultiChoiceQuestionMapper.otherOption == true}" class="radio">
	           			<label>
	           				<input type="radio" th:checked="${#maps.containsKey(oneQQ.mapOfResponseData, 'Other')}" th:name="'qq_' + ${oneQQ.questionnaireQuestionId}" th:onclick="'javascript:show(\'otherText_' + ${oneQQ.questionnaireQuestionId}+ '\');'" value="Other"/>Other
	           			</label>
		           		<textarea th:if="${#maps.containsKey(oneQQ.mapOfResponseData, 'Other')}" class="form-control editableParameter" rows="3" th:text="${oneQQ.responseText}" th:id="'otherText_' + ${oneQQ.questionnaireQuestionId}" name="otherOptionText" style="display: block;"></textarea>
		           		<textarea th:if="${not #maps.containsKey(oneQQ.mapOfResponseData, 'Other')}" class="form-control editableParameter" rows="3" th:text="${oneQQ.responseText}" th:id="'otherText_' + ${oneQQ.questionnaireQuestionId}" name="otherOptionText" style="display: none;"></textarea>
		           </div>
		       </div>
		  </div>
     </form>	
    <form role="form" th:action="@{/questionnaire/{questionnaireId}/response/save(questionnaireId=${oneQQ.questionnaireId})}" th:attr="data-qqid=${oneQQ.questionnaireQuestionId}" th:fragment="NUMBER(oneQQ,iter)" class="editableForm" method="POST">
         <div class="box-body">
           <div class="form-group">
           	<input type="hidden" name="questionnaireQuestionId" th:value="${oneQQ.questionnaireQuestionId}"/>
             <label th:text="${iter}+ ') For '+ ${oneQQ.asset.shortName}">Which asset is this for</label>
             <label th:attr="for=${oneQQ.questionnaireQuestionId}" th:text="${oneQQ.question.title }">Question Title</label>
             <span th:if="${not #maps.isEmpty(mapOfMandatoryQuestion) and #maps.containsKey(mapOfMandatoryQuestion, oneQQ.questionnaireQuestionId)}"><span style="color: red;">*</span></span>
             <p class="help-block" th:text="${oneQQ.question.helpText}">Help Text the question</p>
             <input type="number" th:min="${oneQQ.question.jsonNumberQuestionMapper.min}" th:max="${oneQQ.question.jsonNumberQuestionMapper.max}" class="form-control editableParameter" th:id="${oneQQ.questionnaireQuestionId}" th:name="'qq_' + ${oneQQ.questionnaireQuestionId}" th:value="${oneQQ.responseText}" placeholder="Enter your number Here"  />
           </div>
         </div><!-- /.box-body -->
    </form>
    
    <form role="form" th:action="@{/questionnaire/{questionnaireId}/response/save(questionnaireId=${oneQQ.questionnaireId})}" th:attr="data-qqid=${oneQQ.questionnaireQuestionId}" th:fragment="DATE(oneQQ,iter)" class="editableForm" method="POST">
         <div class="box-body">
           <div class="form-group">
           	<input type="hidden" name="questionnaireQuestionId" th:value="${oneQQ.questionnaireQuestionId}"/>
             <label th:text="${iter}+ ') For '+ ${oneQQ.asset.shortName}">Which asset is this for</label>
             <label th:attr="for=${oneQQ.questionnaireQuestionId}" th:text="${oneQQ.question.title }">Question Title</label>
             <span th:if="${not #maps.isEmpty(mapOfMandatoryQuestion) and #maps.containsKey(mapOfMandatoryQuestion, oneQQ.questionnaireQuestionId)}"><span style="color: red;">*</span></span>
             <p class="help-block" th:text="${oneQQ.question.helpText}">Help Text the question</p>
             <input type="date" class="form-control editableParameter" th:id="${oneQQ.questionnaireQuestionId}" th:name="'qq_' + ${oneQQ.questionnaireQuestionId}" th:value="${oneQQ.responseText}" />
           </div>
         </div><!-- /.box-body -->
    </form>
</body>
</html>