<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.tco.reports.sunburst})">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<style th:fragment="onPageStyles" th:if="${report.reportType == T(org.birlasoft.thirdeye.constant.ReportType).HEALTH_ANALYSIS.description || report.reportType == T(org.birlasoft.thirdeye.constant.ReportType).WAVE_ANALYSIS.description}">
#chart svg {
	height: 400px;
}
</style>
<style th:fragment="onPageStyles" th:if="${report.reportType == T(org.birlasoft.thirdeye.constant.ReportType).PORTFOLIO_HEALTH.description || report.reportType == T(org.birlasoft.thirdeye.constant.ReportType).TCO_ANALYSIS.description || report.reportType == T(org.birlasoft.thirdeye.constant.ReportType).DENSITY_HEAT_MAP.description }">

.chart-responsive{
	overflow:auto;
}

.axis text {
  font: 10px sans-serif;
}

.axis path,
.axis line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}

.bar {
  fill: steelblue;
  fill-opacity: .9;
}

.x1.axis path {
  display: none;
}

.x1.axis text {
  text-anchor: end !important;
}

.nv-x.nv-axis text {
  text-anchor: end !important;
  transform: translate(-10px, 0) !important;
  transform: rotate(-60deg) !important;
}

.brush .extent {
      stroke: #fff;
      fill-opacity: .125;
      shape-rendering: crispEdges;
    }
</style>
</head>
<body>
	<div th:fragment="pageTitle">
		<div>
		    <a class="pull-left fa fa-chevron-left btn btn-default" href="/3rdeye-web/report/myReports" style="margin-left: 10px;"></a>
			<span th:text="${report.reportName}" th:id="${report.id}"></span>
			
	 </div>
	   </div>
	<div th:fragment="pageSubTitle">
		<span th:text="${report.description}"></span>
	</div>
	<div th:fragment="contentContainer">
	   <div>
	   <div class="row nonClick"  style="padding-bottom: 20px; padding-left: 20px;">
			<div style="padding-top: 15px;" data-module="module-filterTags">
				<div class="col-md-12">
					<div id="tags"></div>
				</div>
			</div>
		</div>
	     <div th:if = "${report.reportType ne T(org.birlasoft.thirdeye.constant.ReportType).DENSITY_HEAT_MAP.description}" class="row">
	        <div class="col-md-12">
					<div class="box box-primary scatterPlot" id = "graph">
						<div class="box-header with-border">
							<h3 class="box-title graphTitle"></h3>
							<div class="box-tools pull-right">
							</div>
						</div>
						<canvas name = "canvas" style="display:none"></canvas>
						<div class="box-body chart-responsive">
							<div id="chart" class='with-3d-shadow with-transitions'>
								<svg>
								 
                    			</svg>
							</div>
						</div>
					</div>
				</div>
			</div>
	        
	   </div>	
	   <div data-module="module-viewSavedReport">
	    <script type="text/x-config" th:inline="javascript">{"reportType":"[[${report.reportType}]]","reportConfig":"[[${report.reportConfig}]]"}</script>
	   </div>
	   <div data-module="module-viewScatterGraph">
	   </div>
	   <div data-module="module-viewWaveAnalysis">
	      <div class="row" style="display:none;" id="waveReport">
				<div class="col-md-12">
					<div class="box box-primary scatterPlot" >
						<div class="box-header with-border">
							<h3 class="box-title graphTitle"></h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-widget="collapse">
									<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						<div class="box-body">
							<div id="waveReportFragment">
								
							</div>
						</div>
					</div>
				</div>
			</div>
	   </div>
	   <div data-module="module-tcoAnalysisGraphs">
	
	   </div>
	   <div data-module="module-viewPhp">
	   </div>
	
	   <div th:if ="${report.reportType eq T(org.birlasoft.thirdeye.constant.ReportType).DENSITY_HEAT_MAP.description}" class="row">
	      <div data-module="module-densityHeatMap">
		<div class="col-md-12">
		
			<div class="box box-primary">
			<div class="box-header with-border"></div>
			<div class="box-body" >
			 <script type="text/x-config" th:inline="javascript">{"pngimagename":"Map"}</script>
			<div id="graphhh" style="overflow-x: scroll;width: 100%;">
			<span style="position: absolute;bottom: 253px;font-family: serif;" th:text="#{pages.reports.legend.heatMap.title}"></span>
			<div class= "legend" style="position:  absolute; bottom: 1px;"></div></div>
			  </div>
			</div>
		</div>
	</div>
	</div>
	  </div>
	<div th:if ="${report.reportType ne T(org.birlasoft.thirdeye.constant.ReportType).DENSITY_HEAT_MAP.description}" th:fragment="scriptsContainer" th:remove="tag"><script th:src="@{/static/js/ext/worldMap/d3-tip.js}"></script>
		<script th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
		<script th:src="@{/static/js/ext/nv.d3/1.8.1-dev/nv.d3.min.js}"></script>
		<script th:src="@{/static/js/ext/d3.tip/0.6.3/d3.tip.js}"></script>
		<script th:src="@{/static/js/ext/thirdEye/saveSvgAsPng.min.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-viewWaveAnalysis.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-tcoAnalysisGraphs.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-viewScatterGraph.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-viewPhp.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/report/module-viewSavedReport.js}"></script>
		
	</div>
	<div th:if ="${report.reportType eq T(org.birlasoft.thirdeye.constant.ReportType).DENSITY_HEAT_MAP.description}" th:fragment ="scriptsContainer" th:remove="tag">
		<script th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
		<script th:src="@{/static/js/ext/nv.d3/1.8.1-dev/nv.d3.min.js}"></script>
		<script th:src="@{/static/js/ext/d3.tip/0.6.3/d3.tip.js}"></script>
		<script th:src="@{/static/js/ext/worldMap/d3-tip.js}"></script>
		<script th:src="@{/static/js/ext/worldMap/d3.min.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-densityHeatMap.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/report/module-viewSavedReport.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-viewWaveAnalysis.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-tcoAnalysisGraphs.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-viewScatterGraph.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-viewPhp.js}"></script>
	</div>
</body>

</html>