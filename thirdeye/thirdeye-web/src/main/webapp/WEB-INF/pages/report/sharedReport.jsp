<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Shared Report Modal</title>
</head>
<body>
	<div th:fragment="reportShared">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<form >
						<div class="modal-header">
							<h5 class="modal-title">Share Report</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
						 <div class="table-responsive" data-module="common-data-table">
							<p>List of saved users available in same workspace.</p>
							
							<table class="table table-bordered table-condensed table-hover userListTable">
								<thead>
									<tr>
									    <th><span th:text="#{user.first.name}"></span></th>
										<th><span th:text="#{user.last.name}"></span></th>
										<th><span th:text="#{user.email}"></span></th>
										<th><span th:text="#{question.action}"></span></th>
									</tr>
								</thead>
								<tbody>
									<tr th:each="listOfUsers : ${userList}" th:id="${listOfUsers.id}">
									    <td><span th:text="${listOfUsers.firstName}">Test</span></td>
										<td><span th:text="${listOfUsers.lastName}">Test</span></td>
										<td><span th:text="${listOfUsers.emailAddress}">Test</span></td>
										<td><input type="checkbox" name="select_User" class = "userCheckedClass" th:value="${listOfUsers.id}"/></td>
									</tr>
								</tbody>
							</table>
                        
						</div>
						</div>
						<div class="modal-footer" >
							  <input type="submit" data-type ="addUsersToShareReport" value="Done" class="btn btn-primary"></input>
							  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
	</div>
</body>
</html>
