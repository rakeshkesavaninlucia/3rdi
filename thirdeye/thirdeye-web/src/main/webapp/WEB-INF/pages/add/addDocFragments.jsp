<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="addDoc" class="docs" th:id="'obj-'+${addWrapper.name}">
		<div class="docTools">
			<span class="fa fa-times" data-type="docs-close"></span>
		</div>
		<h2>
			<span th:text="${addWrapper.name}"></span><em><span th:text="${addWrapper.type}"></span></em>
		</h2>

		<div class="alert alert-warning"><span th:text="#{dependency.documentation.unavailable}"></span></div>

		<!-- <h3>Documentation</h3>

		<p>This view contains miscellaneous other data for the current period.</p> -->

		<h3><span th:text="#{dependency.depends.on}"></span><em th:if="${#lists.size(addWrapper.depends) lt 1 }"><span th:text="#{dependency.none}"></span></em></h3>

		<ul th:if="${#lists.size(addWrapper.depends) ge 1 }" th:each="depend : ${addWrapper.depends }">
			<li><a data-type="select-object" class="select-object" th:attr="data-name=${depend.asset}" th:text="${depend.asset}">Test</a></li>
		</ul>

		<h3><span th:text="#{dependency.depended.on.by}"></span> <em th:if="${#lists.size(addWrapper.dependedOnBy) lt 1 }"><span th:text="#{dependency.none}"></span></em></h3>

		<ul th:if="${#lists.size(addWrapper.dependedOnBy) ge 1 }" th:each="depend : ${addWrapper.dependedOnBy }">
			<li><a data-type="select-object" class="select-object" th:attr="data-name=${depend}" th:text="${depend}">Test</a></li>
		</ul>
	</div>
</body>
</html>