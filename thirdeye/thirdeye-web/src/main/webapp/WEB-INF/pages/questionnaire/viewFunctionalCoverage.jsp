<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='', pageTitle = #{pages.questionnaireresponse.functionalcoverage.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
	<div th:fragment="pageTitle">
	  <a class="pull-left fa fa-chevron-left btn btn-default" href="javascript:window.history.back();" style="margin-left: 10px;"></a>
	  <span th:text="#{view.functional.coverage.title}"></span></div>
	<div th:fragment="pageSubTitle"><span th:text="#{view.functional.coverage.subtitle}"></span></div>
	<div class="container" th:fragment="contentContainer">	
		<div class="box box-primary">
            <div class="box-body" data-module="module-exportFm">
	             <div class="table-responsive" data-module="common-data-table">					
		            <table class="table table-bordered">
						<thead>
							<tr>
								<th><span th:text="#{functional.coverage.name}"></span></th>								
								<th><span th:text="#{tabel.header.action}"></span></th>
							</tr>
						</thead>
						<tbody>
							<tr th:each="oneFcFm : ${fcFmMap}">
								<td><span th:text="${oneFcFm.value}"></span></td>								
		                        <td>
		                        	<a class="fa fa-download exportImport clickable" data-type="exportImportMap" th:attr="data-fmid=${oneFcFm.key},data-qid=${questionnaireId}" th:title="#{icon.title.export/import}"></a>&nbsp;
								    <a class="fa fa-eye viewImportFMData clickable" data-type="viewImportFMData" th:attr="data-fmid=${oneFcFm.key},data-qid=${questionnaireId}" th:title="#{icon.title.fm.eye}"></a>
		                        </td>
							</tr>					
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal fade" data-module="module-functionalMapData" tabindex="-1" role="dialog" style="display: none;">
	            <script type="text/x-config">{"mode":"viewFunctinalMapdata"}</script>
		    </div>
		</div>		
	</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
		<script	th:src="@{/static/js/3rdEye/modules/module-functionalMapData.js}"></script>
     	<script	th:src="@{/static/js/3rdEye/modules/module-exportFm.js}"></script>      
    </div>
</body>
</html>