<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<title>Block 1 fragments</title>
</head>
<body>

	<div th:fragment="BLOCK_4_view(blockForDisplay)">
		<div class="aidblock block_4">
			<table style="width:100%;">
				<tr>
					<th colspan="3" class="blockTitle" th:text="${blockForDisplay.blockTitle} ? ${blockForDisplay.blockTitle} : '-- No Title --'">This is the application name</th>
				</tr>
				<tr>
					<td class="col-md-4 subBlockTitle" th:text="${blockForDisplay.subBlock1.blockTitle} ? ${blockForDisplay.subBlock1.blockTitle} : '-- No Title --'">2</td>
					<td class="col-md-8 subBlockContent" th:text="${blockForDisplay.subBlock1.isConfigured() } ? ${blockForDisplay.subBlock1.displayString}:'-- Nothing to display --'">2</td>
				</tr>
				<tr>
					<td class="col-md-4 subBlockTitle" th:text="${blockForDisplay.subBlock2.blockTitle} ? ${blockForDisplay.subBlock2.blockTitle} : '-- No Title --'">2</td>
					<td class="col-md-8 subBlockContent" th:text="${blockForDisplay.subBlock2.isConfigured() } ? ${blockForDisplay.subBlock2.displayString}:'-- Nothing to display --'">2</td>
				</tr>
			</table>
		</div>
	</div>
	
	<div th:fragment="BLOCK_4_edit(blockForDisplay)" >
		<div class="modal fade" id="blockEditModal" tabindex="-1" role="dialog" aria-labelledby="blockEditModal">
			<div class="modal-dialog" role="document" >
				    <div class="modal-content">
						<form th:action="@{/aid/configure/blockedit/save}" th:object="${blockEditForm}" method="post" id="blockEditForm">
							<input type="hidden" th:field="*{id}"/>
								<div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        <h4 class="modal-title">Update Block Configuration</h4>
						      	</div>
						      	<div class="modal-body">
									<table style="width:100%;" class="table table-bordered">
										<tr>
											<th colspan="2">
												<div class="form-group">
										            <input type="text" class="form-control" th:field="*{blockTitle}" placeholder="Add a title"/>
							            			<span th:if="${#fields.hasErrors('blockTitle')}" th:errors="*{blockTitle}" style="color: red;">Test</span>
										          </div>
											</th>
										</tr>
										<tr>
											<td class="col-md-4">
												<a class="btn btn-block btn-primary btn-sm configureSubBlock" th:attr="data-blockid=${blockEditForm.id},data-subblockid='subBlock1'" >Edit Sub Section 1</a>
											</td>
											<td class="col-md-8">
												<dl>
								                    <dt>Title</dt>
								                    <dd th:text="${blockEditForm.subBlock1.blockTitle} ? ${blockEditForm.subBlock1.blockTitle} : 'No Title given'">A description list is perfect for defining terms.</dd>
								                    <dt>Configuration</dt>
								                    <dd th:text="${blockEditForm.subBlock1.isConfigured() } ? ${blockEditForm.subBlock1.displayString}:'Not configured'">something</dd>
								                 </dl>
											</td>
										</tr>
										<tr>
											<td class="col-md-4">
												<a class="btn btn-block btn-primary btn-sm configureSubBlock" th:attr="data-blockid=${blockEditForm.id},data-subblockid='subBlock2'" >Edit Sub Section 2</a>	
											</td>
											<td class="col-md-8">
												<dl>
								                    <dt>Title</dt>
								                    <dd th:text="${blockEditForm.subBlock2.blockTitle} ? ${blockEditForm.subBlock2.blockTitle} : 'No Title given'">A description list is perfect for defining terms.</dd>
								                    <dt>Configuration</dt>
								                    <dd th:text="${blockEditForm.subBlock2.isConfigured() } ? ${blockEditForm.subBlock2.displayString}:'Not configured'">something</dd>
								                 </dl>
											</td>
										</tr>
										
									</table>
								</div>
								
								  <div class="modal-footer">
							        	<input type="submit" value="Update Configuration" class="btn btn-primary"></input>
							      </div>
					    </form>
				    </div>
			 </div>
		 </div>
	</div>
	
	
	
</body>
</html>