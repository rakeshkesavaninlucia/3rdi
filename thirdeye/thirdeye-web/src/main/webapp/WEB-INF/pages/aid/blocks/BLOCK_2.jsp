<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<title>Block 1 fragments</title>
</head>
<body>

	<div th:fragment="BLOCK_2_view(blockForDisplay)">
		<div class="aidblock block_2">
			<table style="width:100%;">
				<tr>
					<th class="blockTitle" th:text="${blockForDisplay.blockTitle} ? ${blockForDisplay.blockTitle} : '-- No Title --'">This is the block title.</th>
				</tr>
				<tr>
					<td class="col-md-12 subBlockContent" th:text="${blockForDisplay.subBlock1.isConfigured() } ? ${blockForDisplay.subBlock1.displayString}:'-- Nothing to display --'">2</td>
				</tr>
				
			</table>
		</div>
	</div>
	
	<div th:fragment="BLOCK_2_edit(blockForDisplay)" >
		<div class="modal fade" id="blockEditModal" tabindex="-1" role="dialog" aria-labelledby="blockEditModal">
			<div class="modal-dialog" role="document" >
				    <div class="modal-content">
						<form th:action="@{/aid/configure/blockedit/save}" th:object="${blockEditForm}" method="post" id="blockEditForm">
							<input type="hidden" th:field="*{id}"/>
								<div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        <h4 class="modal-title">Update Block Configuration</h4>
						      	</div>
						      	<div class="modal-body">
									<table style="width:100%;" class="table table-bordered">
										<tr>
											<th>
												<div class="form-group">
										            <input type="text" class="form-control" th:field="*{blockTitle}" placeholder="Add a title"/>
							            			<span th:if="${#fields.hasErrors('blockTitle')}" th:errors="*{blockTitle}" style="color: red;">Test</span>
										          </div>
											</th>
										</tr>
										<tr>
											<td class="col-md-12">
												<a class="btn btn-block btn-primary btn-sm configureSubBlock" th:attr="data-blockid=${blockEditForm.id},data-subblockid='subBlock1'" >Edit Sub Section 1</a>
											</td>
										</tr>
										<tr>
											<td class="col-md-12">
												<dl>
								                    <dt>Title</dt>
								                    <dd th:text="${blockEditForm.subBlock1.blockTitle} ? ${blockEditForm.subBlock1.blockTitle} : 'No Title given'">A description list is perfect for defining terms.</dd>
								                    <dt>Configuration</dt>
								                    <dd th:text="${blockEditForm.subBlock1.isConfigured() } ? ${blockEditForm.subBlock1.displayString}:'Not configured'">something</dd>
								                 </dl>
											</td>
										</tr>
										
									</table>
								</div>
								
								  <div class="modal-footer">
							        	<input type="submit" value="Update Configuration" class="btn btn-primary"></input>
							      </div>
					    </form>
				    </div>
			 </div>
		 </div>
	</div>
	
	
	
</body>
</html>