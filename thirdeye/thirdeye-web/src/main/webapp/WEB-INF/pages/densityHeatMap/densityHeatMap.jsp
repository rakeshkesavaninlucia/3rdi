<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = 'DensityHeatMap')">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
<div th:fragment="pageTitle">
		<div>
			<span th:text="#{pages.reports.densityHeatMap.title}"></span>
		</div>
	</div>
	<div th:fragment="pageSubTitle">
	<span></span>
	</div>
	<div class="container" th:fragment="contentContainer">
		<div class="box box-primary ">
			<div class="box-header with-border">
				<h3 class="box-title" th:text="BcmLevelFilter"></h3>
				<div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			
			
			<div class="box-body" data-module="module-bcmLevelFilter">
			<table>
			<tr>
			<td>
				<div class="col-md-3 form-group">
					<label style=" font-size: 14px;">Select BCMLevel 1</label> <select
						class="form-control ajaxSelector select2"
						th:attr="data-dependency=level1,data-url='graph/scatter/fetchbcmlevel/'"
						id="idsOfLevel1" name="idsOfLevel1" multiple="multiple">

						<option th:each="oneLevel : ${bcmLevelList}"
							th:value="${oneLevel.id}" th:text="${oneLevel.bcmLevelName}"></option>
					</select>

				</div>

				<div class="col-md-3 form-group">
					<label style=" font-size: 14px;">Select BCMLevel 2</label> <select
						class="form-control ajaxPopulated ajaxSelector  select2"
						th:attr="data-dependson=level1,data-dependency=level2,data-url='graph/scatter/fetchbcmlevel/'"
						id="idsOfLevel2" name="idsOfLevel2" multiple="multiple">
						<option value=""></option>
					</select>


				</div>
				<div class="col-md-3 form-group">
					<label style=" font-size: 14px;">Select BCMLevel 3</label> <select
						class="form-control ajaxPopulated ajaxSelector  select2"
						th:attr="data-dependson=level2,data-dependency=level3,data-url='graph/scatter/fetchbcmlevel/'"
						id="idsOfLevel3" name="idsOfLevel3" multiple="multiple">
						<option value=""></option>
					</select>


				</div>
				<div class="col-md-3 form-group">
					<label style=" font-size: 14px;">Select BCMLevel 4</label> <select
						class="form-control ajaxPopulated  select2"
						th:attr="data-dependson=level3" id="idsOfLevel4"
						name="idsOfLevel4" multiple="multiple">
						<option value=""></option>
					</select>
				</div>

				<select th:fragment="optionListBcm(listOfLevels)" th:remove="tag">
					<option th:each="oneLevel : ${listOfLevels}"
						th:value="${oneLevel.id}" th:text="${oneLevel.bcmLevelName}"></option>
				</select>
				</td>
				</tr>
				<tr>
				<td>
				<div class="box-footer">
					<button type="submit" class="btn btn-primary pull-right"
						data-type="plotGraph">Plot Graph</button>
					<div id="saveAs" class="pull-right"
						style="display: none; margin-right: 5px;">
						<a sec:authorize="@securityService.hasPermission({'SAVE_EDIT_DELETE_REPORT'})"
							class="btn btn-primary" data-type="saveReport"
							th:text="#{report.modal.title}"></a>
					</div>
					<script type="text/x-config" th:inline="javascript">{ "id": "","reportType":"[[${T(org.birlasoft.thirdeye.constant.ReportType).DENSITY_HEAT_MAP.description}]]"}</script>
				</div>
				</td></tr></table>
			</div>
			
			
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border"></div>
					<div class="box-body3" data-module="module-densityHeatMap">
						<script type="text/x-config" th:inline="javascript">{"pngimagename":"Density Heat Map"}</script>
						<div id="graphhh" style="overflow-x: scroll; width: 100%;">
							<span
								style="position: absolute; bottom: 253px; font-family: serif;"
								th:text="#{pages.reports.legend.heatMap.title}"></span>
							<div class="legend" style="position: absolute; bottom: 1px;"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div data-module="module-report">
			<script type="text/x-config" th:inline="javascript">{"url":"report/createReport", "modal": "saveReportModal"}</script>
		</div>
	</div>
	<div th:fragment="scriptsContainer">
		<script th:src="@{/static/js/3rdEye/modules/module-densityHeatMap.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-bcmLevelFilter.js}"></script>
		<script th:src="@{/static/js/ext/d3.tip/0.6.3/d3.tip.js}"></script>
		<script th:src="@{/static/js/ext/worldMap/d3.min.js}"></script>
		<script th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
		<script th:src="@{/static/js/ext/worldMap/d3-tip.js}"></script>
		<script th:src="@{/static/js/ext/worldMap/d3.min.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/report/module-report.js}"></script>
	</div>
</body>
</html>