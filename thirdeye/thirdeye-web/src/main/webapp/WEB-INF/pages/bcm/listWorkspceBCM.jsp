<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.functionalmaps.viewworkspacebcm.nav })">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{pages.functionalmaps.viewworkspacebcm.title}"></span>
	<div  id="new" data-module ="module-showHelp">
			<button class="btn btn-primary btn-sm glyphicon glyphicon-question-sign"  data-type ="help" style="float:right;margin-top: -19px;margin-right: 50px;"></button>
	</div>
</div>
<div th:fragment="pageSubTitle"><span th:text="#{pages.functionalmaps.viewworkspacebcm.subtitle}"></span></div>
	<div class="container" th:fragment="contentContainer">
	  <div class="row">
        	<div class="col-sm-12">
	        	<div class="box box-primary">
	                <div class="box-body">
	     <div class="table-responsive" data-module="common-data-table">
					<div class="overlay">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
			<table class="table table-bordered table-condensed table-hover" id="listOfWorkspaceBCM">
				<thead>
					<tr>
						<th><span th:text="#{bcm.name}"></span></th>
						<th><span th:text="#{bcm.template.name}"></span></th>
						<th><span th:text="#{bcm.update.date}"></span></th>
						<th><span th:text="#{bcm.action}"></span></th>
					 	<th><span th:text="#{bcm.active}"></span></th>
					</tr>
				</thead>
				<tbody>
					<tr th:each="oneBcm : ${listOfBcms}" th:id="${oneBcm.id}">
						<td><span th:text="${oneBcm.bcmName}">Test</span></td>
						<td><span th:text="${oneBcm.bcm.bcmName}">Test</span></td>
						<td><span th:text="${#dates.format(oneBcm.updatedDate,'dd-MMM-yyyy')}">Test</span></td>
						<td><a th:href="@{/bcmlevel/{id}/levels(id=${oneBcm.id})}" class="fa fa-pencil-square-o" th:title="#{icon.title.edit}" sec:authorize="@securityService.hasPermission({'BCM_MODIFY'})"></a></td>
						<td><input type="radio" name="bcmStatus" class="bcmStatusClass" data-type="bcmStatusDataType" th:value="${oneBcm.id}" th:checked="${oneBcm.status}"  sec:authorize="@securityService.hasPermission({'BCM_MODIFY'})"/></td>
					</tr>					
				</tbody>
			</table>
		</div>
		</div>
		<div class="box-footer clearfix" data-module="module-modalCreation">
	                  <div>
				          <a class="createWorkspaceBCM btn btn-primary" th:text="#{bcm.workspace.manageCapabilityMap}" data-type="createModal" sec:authorize="@securityService.hasPermission({'BCM_MODIFY'})"></a>
				          <script type="text/x-config" th:inline="javascript">{"url":"wsbcm/create", "modal": "createWorkspaceBCMModal"}</script>
				      </div>
		</div>
		</div>
		</div>
		</div>
	</div>

	<div th:fragment="scriptsContainer"  th:remove="tag">
		<script	th:src="@{/static/js/3rdEye/modules/module-modalCreation.js}"></script>
	</div>
</body>
</html>