<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<ul class="treeview-menu" th:fragment="oneLevelWithWrapper(listOfLevels)">
		<li th:fragment="oneLevelAdded(listOfLevels)" th:each="oneLevel : ${listOfLevels}"> 
			<a th:fragment="oneAnchor(oneLevel)">
				 <span th:text="${oneLevel.costStructureName}" data-container="body" data-placement="top">Option in tree</span></a> 
				<!-- Process children -->
			<div th:replace="tco/tcolevelfragment :: oneLevelWithWrapper(listOfLevels=${oneLevel.subCostStructureList})"></div>
		</li>
	</ul>
</body>
</html>