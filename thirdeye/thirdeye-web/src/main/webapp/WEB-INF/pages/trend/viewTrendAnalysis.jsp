<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.reports.trend.nav})">
<head>
<meta charset="utf-8" />
<title>Trend Analysis</title>
<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
  Remove this if you use the .htaccess -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<style th:fragment="onPageStyles">
#chart svg {
	height: 400px;
}
 /* set the CSS */
path { 
	stroke: steelblue;
	stroke-width: 3;
	fill: none;
}
.axis path,
.axis line {
	fill: none;
	stroke: black;
	stroke-width: 1;
	shape-rendering: crispEdges;
}
</style>
</head>
<body>
	<div th:fragment="pageTitle">
		<div>
			<span th:text="#{pages.reports.trend.title}"></span>
		</div>
		
		<div  id="new" data-module ="module-showHelp">
			<button class="btn btn-primary btn-sm glyphicon glyphicon-question-sign"  data-type ="help" style="float:right;margin-top: -25px;margin-right: 15px;"></button>
		</div>
	</div>
	<div th:fragment="pageSubTitle">
		<span th:text="#{pages.reports.trend.subtitle}"></span>
	</div>
	<div th:fragment="contentContainer">
		<div data-module="module-viewTrendAnalysis">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body">
							<div th:replace="trend/trendAnalysisFragment :: createFullFragment"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary trendAnalysis" >
						<div class="box-header with-border">
							<h3 class="box-title graphTitle"></h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-type="download" title = "Download">
									<i class="glyphicon glyphicon-download clickable"></i>
								</button>
								<button class="btn btn-box-tool" data-widget="collapse">
									<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						<div class="box-body chart-responsive">
							<div id="chart" class='with-3d-shadow with-transitions'>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div data-module="module-report">
		  <script type="text/x-config" th:inline="javascript">{"url":"report/createReport", "modal": "saveReportModal"}</script>
		</div>		
	</div>

	<div th:fragment="scriptsContainer" th:remove="tag">
		<script th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
		<script th:src="@{/static/js/ext/nv.d3/1.8.1-dev/nv.d3.min.js}"></script>
		<script th:src="@{/static/js/ext/d3.tip/0.6.3/d3.tip.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-viewTrendAnalysis.js}"></script>
		<script th:src="@{/static/js/ext/thirdEye/saveSvgAsPng.min.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/report/module-report.js}"></script>
	</div>

</body>
</html>