<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="viewQuestionnaireList(nameOfAxis)">
		<div class="form-group">
			<label>Select the questionnaire</label>
			<select class="form-control ajaxSelector select2" th:attr="data-dependency=${nameOfAxis},data-url='trend/fetchRootParameters/'" name="questionnaireIds">
	          <option value="-1">--Select--</option>
	          <option th:each="oneQuestionnaire : ${listOfQuestionnaire}" th:value="${oneQuestionnaire.id}" th:text="${oneQuestionnaire.name}" />
	        </select>
        </div>
	</div>
	
	<div th:fragment="viewParamList(nameOfAxis,id)">
		<div class="form-group">
			<label>Select the parameter</label>
						
			<select  th:id="${id}" class="form-control ajaxPopulated select2 paramDrop" th:attr="data-dependson=${nameOfAxis}" name="parameterIds">
	             <option value="-1">--Select--</option>
	             <option th:each="oneParameter : ${listOfParameters}" th:value="${oneParameter.id}" th:text="${oneParameter.displayName}" />
	        </select>
        </div>
	</div>
	
		<select th:fragment="optionList(listOfParameters)" th:remove="tag">
	         <option value="-1">--Select--</option>
	         <option th:each="oneParameter : ${listOfParameters}" th:value="${oneParameter.id}" th:text="${oneParameter.displayName}" />
	    </select>

	<div th:fragment="combineQuestionnaireAndParamAndDate(oneAxis,id,i)">
		<div class="col-md-4">
			<div th:replace="trend/trendAnalysisFragment :: viewQuestionnaireList(nameOfAxis=${oneAxis})"></div>
			<div th:replace="trend/trendAnalysisFragment :: viewParamList(nameOfAxis=${oneAxis},id=${id})"></div>
			<div th:replace="trend/trendAnalysisFragment :: viewDate(i=${i})"></div>
		</div>
	</div>
	
	<div th:fragment ="viewDate(i)">
	   <label th:text="From"></label><input type="text" th:name="'startDate'+${i}" class="form-control datepicker"  th:required="true" ></input>
	   <label th:text="TO"></label><input type="text" th:name="'endDate'+${i}" class="form-control datepicker"  th:required="true" ></input>
	</div>					
	
	<div th:fragment="createFullFragment">
			<form role="form" th:action="@{/trend/plot}" method="post" id="trendAnalysisControl">
				<div class="row">
					<div th:replace="trend/trendAnalysisFragment :: combineQuestionnaireAndParamAndDate(oneAxis='x-axis',id='paramdropdownone',i='1')"></div>
					<div th:replace="trend/trendAnalysisFragment :: combineQuestionnaireAndParamAndDate(oneAxis='y-axis',id='paramdropdowntwo',i='2')"></div>
				</div>
				<div class="box-footer">
					<div class = "pull-right">
						<button type="submit" class="btn btn-primary">Plot Graph</button>
					</div>
	            </div>
			</form>
	</div>
</body>
</html>