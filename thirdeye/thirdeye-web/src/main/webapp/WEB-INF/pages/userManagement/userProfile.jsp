<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.usermanagement.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
<div th:fragment="pageTitle">
		<div>
		<a class="pull-left fa fa-chevron-left btn btn-default" href="/3rdeye-web/home" style="margin-left: 10px;"></a>
			<span th:text="#{user.profilepage}"></span>
		</div>
	</div>	

	<div class="container" th:fragment="contentContainer">
		<div class="col-md-14">
			<div class="box box-primary">
				<div class="box-body" id="box-body1" >
					<div th:fragment="createUserForm">
						<div data-module="module-userProfileView">
							<i class="fa fa-camera fa-2 camera clickable photo" title="Upload Photo"></i>
							<span class = "labelForPhoto"> Maximum size of Photo is 300kb</span>
							 <img id="user-img" th:if="${@customUserDetailsServiceImpl.getCurrentUser().getPhoto() eq null}" style="float: right" height="140px" width="140px" th:src="@{/static/img/user2-160x160.jpg}" class="img-circle" />
							<img id="user-img" style="float: right" height="140px" width="140px" class="img-circle"	th:if="${@customUserDetailsServiceImpl.getCurrentUser().getPhoto() ne null}" th:src="@{/user/{id}/showimage(id=${@customUserDetailsServiceImpl.getCurrentUser().getId()})}" />
							<form th:action="@{/user/saveUserProfile?_csrf={_csrf.token}(_csrf.token=${_csrf.token})}" class ="userForm" method="post" th:object="${UserForm}" enctype="multipart/form-data">
								<input type="hidden" th:field="*{id}" />
								<input type = "hidden" th:field = "*{photo}"/>
								<div class="form-group">
									<span>First Name :<span style="color: red;">*</span></span> 
									<input style="width: 380px;" type="text" th:field="*{firstName}" class="form-control textfield" placeholder="First Name" />
									 <span th:if="${# fields.hasErrors('firstName')}" th:errors="*{firstName}" style="color: red;">Test</span><br />
									<span>Last Name :<span style="color: red;">*</span></span>
									 <input	style="width: 380px;" type="text" th:field="*{lastName}" class="form-control textfield" placeholder="Last Name" />
									  <span	th:if="${#fields.hasErrors('lastName')}" th:errors="*{lastName}" style="color: red;">Test</span> <br />
									<div style="float: right;">
										<span th:if="${#fields.hasErrors('photo')}"
											th:errors="*{photo}" style="color: red;">Test</span>
									</div>
									<input type="file" class="fake" style="float: right;" name="fileUpload" value="fileUpload" accept="fileUpload/jpeg,fileUpload/png,fileUpload/jpg" />
									 <span>Email:<span style="color: red;">*</span>
									</span> <input type="email" th:field="*{emailAddress}" readonly = "readonly" class="form-control" placeholder="Email" />
									 <span th:if="${#fields.hasErrors('emailAddress')}" th:errors="*{emailAddress}" style="color: red;">Test</span><br />
									<span> Hint Question1 :<span style="color: red;">*</span></span>
									<select th:if ="${listQuestionAnswer ne null}" class="form-control select2" id="question1dropdown"	name="listOfQuestionAnswer"  data-type="selecttype">
										<option selected="selected" value="-1">--Select--</option>
										<option  
											th:each="question : ${T(org.birlasoft.thirdeye.constant.HintQuestionType).values()}"
										     th:text="${question.value}"  th:value="${question.value}"  th:selected="${question.value == listQuestionAnswer[0].question}" >
										</option>
									</select>
									<select th:if ="${listQuestionAnswer eq null}" class="form-control select2" id="question1dropdown" name="listOfQuestionAnswer"  data-type="selecttype">
										<option selected="selected" value="-1">--Select--</option>
										<option  
											th:each="question : ${T(org.birlasoft.thirdeye.constant.HintQuestionType).values()}"
										     th:text="${question.value}"  th:value="${question.value}" >
										</option>
									</select> <br />
									
									<div th:if = "${listQuestionAnswer ne null}" id="answerText1field">
										<br /> <span>Hint Answer</span><span style="color: red;">*</span>
										 <input	type="text" name="listOfQuestionAnswer" class="form-control"  th:value = "${listQuestionAnswer[0].answer}" placeholder="Enter Answer" />
									</div>
									<div th:if = "${listQuestionAnswer eq null}" id="answerText1field">
										<br /> <span>Hint Answer</span><span style="color: red;">*</span>
										 <input	type="text" name="listOfQuestionAnswer" class="form-control" placeholder="Enter Answer" />
									</div>
									<br /> <span> Hint Question2 :<span style="color: red;">*</span></span>
									<select th:if ="${listQuestionAnswer ne null}" class="form-control select2" id="question2dropdown"	name="listOfQuestionAnswer" data-type="selecttype">
										<option value="-1">--Select--</option>
										<option
											th:each="question2 : ${T(org.birlasoft.thirdeye.constant.HintQuestionType).values()}"
												 th:text="${question2.value}"  th:selected="${question2.value == listQuestionAnswer[1].question}" >
										</option>
									</select>
									<select th:if ="${listQuestionAnswer eq null}" class="form-control select2" id="question2dropdown"	name="listOfQuestionAnswer" data-type="selecttype">
										<option value="-1">--Select--</option>
										<option
											th:each="question2 : ${T(org.birlasoft.thirdeye.constant.HintQuestionType).values()}"
												 th:text="${question2.value}"  th:value="${question2.value}">
										</option>
									</select> <br />
									<div th:if = "${listQuestionAnswer ne null}" id="answerText2field">
										<br /> <span>Hint Answer</span><span style="color: red;">*</span>
										 <input	type="text" name="listOfQuestionAnswer" class="form-control" id="answerTextfield" th:value = "${listQuestionAnswer[1].answer}"  placeholder="Enter Answer" />
									</div>
									<div th:if = "${listQuestionAnswer eq null}" id="answerText2field">
										<br /> <span>Hint Answer</span><span style="color: red;">*</span>
										 <input	type="text" name="listOfQuestionAnswer" class="form-control" id="answerTextfield" placeholder="Enter Answer" />
									</div>
									<br /> <span>Organization Name</span><span style="color: red;">*</span>
									<input type="text" th:field="*{organizationName}" class="form-control" placeholder="Organization Name" /> 
									<span th:if="${#fields.hasErrors('organizationName')}" th:errors="*{organizationName}" style="color: red;">Test</span>
									<br /> <span>Country:<span style="color: red;">*</span></span>
									  <select class="form-control select2" data-role="country-selector" th:field="*{country}" data-type="selecttype" id="dropdown" th:value = "${UserForm.country}">
									</select> 
									  <span th:if="${#fields.hasErrors('country')}"
										th:errors="*{country}" style="color: red;">Test</span>
								</div>
								<span th:if="${#fields.hasErrors('questionAnswer')}"
									th:errors="*{questionAnswer}" style="color: red;">Test</span>
										<div th:if = "${successmessage eq false}">
								 <script type="text/x-config" th:inline="javascript">{"error":"false"}</script>
				                    </div>
								<div class="box-footer">
									<div>
										<input class="btn btn-primary" type="submit" value="Save"/>
										<div style="float: right;"><a  th:href="@{/user/changePassword}" class="btn btn-primary" >Change Password</a></div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div th:fragment="scriptsContainer">
		<script
			th:src="@{/static/js/3rdEye/modules/module-userProfileView.js}"></script>
	</div>
</body>
</html>