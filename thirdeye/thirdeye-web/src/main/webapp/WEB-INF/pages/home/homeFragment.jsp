<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="homeAssetClasses">		
		<div th:if="${not #lists.isEmpty(listAssetClassSearchBean)}" th:each="oneAssetClassSearchBean : ${listAssetClassSearchBean}" class="container">
			<div class="row" style="padding-bottom: 30px;">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					<div class="assetTypeTile">
						<div class="info-box">
							<span class="info-box-icon" style="background-color: black"><i class="fa fa-desktop"></i></span>
							<div class="info-box-content">
								<span class="info-box-text" th:text="${oneAssetClassSearchBean.name}"></span>
								<span class="info-box-number" th:text="${oneAssetClassSearchBean.count}"></span>
							</div><!-- /.info-box-content -->
						</div><!-- /.info-box -->
					</div>
				</div>
	
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<div th:each="oneParameter : ${oneAssetClassSearchBean.parameters}">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 homeParameter" style="padding-bottom: 30px;">
							<div th:attr="data-loadurl=${'homeParameterTiles/' + oneAssetClassSearchBean.name + '/' + oneParameter.id}">								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="box box-primary" th:if="${#lists.isEmpty(listAssetClassSearchBean)}">
			<div class="box-header with-border">
				<h3 class="box-title">No Data available</h3>
			</div>
		</div>
	</div>
</body>
</html>