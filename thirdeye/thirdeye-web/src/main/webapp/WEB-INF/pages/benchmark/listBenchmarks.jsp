<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle ='')">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
    <div th:fragment="pageTitle"><span th:text="#{page.available.benchmark}"></span>
    
    <div  id="new" data-module ="module-showHelp">
			<button class="btn btn-primary btn-sm glyphicon glyphicon-question-sign"  data-type ="help" style="float:right;margin-top: -19px;margin-right: 50px;"></button>
	</div>
    </div>
	
	<div class="container" th:fragment="contentContainer">
		       <div class="row">
        	<div class="col-sm-12">
	        	<div class="box box-primary">
	                <div class="box-body" data-module="module-deleteBenchmark" >					
							<div class="table-responsive" data-module="common-data-table">
									<div class="overlay">
										<i class="fa fa-refresh fa-spin"></i>
									</div>
									<table class="table table-bordered table-condensed table-hover" id="benchmarkView">
											<thead>
												<tr>
													<th><span th:text="#{benchmark.name}"></span></th>																			
													<th><span th:text="#{benchmark.action}"></span></th>
												</tr>
											</thead>
											<tbody >
											<tr th:each="oneBenchmark : ${listOfBenchmark}">
												    <input type="hidden" name="benchmarkId" th:value="${oneBenchmark.id}" />
													<td><span th:text="${oneBenchmark.name}">Test</span></td>												
													<td><a th:href="@{/benchmark/{id}/edit(id=${oneBenchmark.id})}" class="fa fa-pencil-square-o" th:title="#{icon.title.edit}" sec:authorize="@securityService.hasPermission({'BENCHMARK_MODIFY'})"></a>
								        			<a class="deleteRow clickable  fa fa-trash-o" th:title="#{icon.title.delete}" sec:authorize="@securityService.hasPermission({'BENCHMARK_MODIFY'})"></a></td>
											    </tr>
											</tbody>
									</table>
							</div>
			          </div>
			          <div class="box-footer clearfix">
	                  <div>
				          <a class="btn btn-primary" th:href="@{/benchmark/create}" th:text="#{benchmark.create}" sec:authorize="@securityService.hasPermission({'BENCHMARK_MODIFY'})">Create BenchMark</a>
				        </div>
	                </div>
			          </div>
			          </div>
			          </div>	
</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
       <script	th:src="@{/static/js/3rdEye/modules/module-deleteBenchmark.js}"></script>
   </div>
</body>
	</html>