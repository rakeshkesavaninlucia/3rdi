/**
 * @author: shaishav.dixit
 */
Box.Application.addModule('module-exportInventory', function(context) {

	var $,moduleEl;
	var commonUtils;
	var pageUrl = "/exportImport/fetchModal";
	var id;
	var dataObj= {};
	
	function bindInventoryExportImportModalRequirements($newModal){
		$('#importInventoryForm', $newModal).ajaxForm(function(htmlResponse) {
			
			if (htmlResponse.redirect){
				window.location.replace(commonUtils.getAppBaseURL(htmlResponse.redirect));
			} else{ 
				var $replacement = commonUtils.swapModalContent("exportImportInventoryModal", htmlResponse);
				$replacement.modal({backdrop:false});
				bindInventoryExportImportModalRequirements($replacement);
			}
		}); 
	}
	
	function createInventoryExportImportModal(id){
		
    	var url = commonUtils.getAppBaseURL("templates/"+id+pageUrl);
    	
		commonUtils.getHTMLContent(url).then(function(content){
			
			var $newModal = commonUtils.appendModalToPage("exportImportInventoryModal", content, true);
			
			bindInventoryExportImportModalRequirements($newModal);
			
			$newModal.modal({backdrop:false,show:true});
		});
	}
	
	function handleDelete(dataObj){
		var deferred = $.Deferred();
		var postRequest = $.post( commonUtils.getAppBaseURL("templates/delete"), dataObj);
		postRequest.done(function(){
			window.location.href= commonUtils.getAppBaseURL("templates/list");
			deferred.resolve();
		});
		return deferred.promise();
	}
	
    return {

        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');
            moduleEl = context.getElement();
        },
    
        onclick: function(event, element, elementType) {
        	if (elementType === 'exportImportAsset') {
        		id = $(element).data("templateid");
        		createInventoryExportImportModal(id);
        	}else if (elementType==='deleteTemplate'&&confirm('This item will be permanently deleted and cannot be recovered. Are you sure?')===true){
        		dataObj.templateId = $(element).data("templateid");
        		handleDelete(dataObj);
        	}
        }	
    };
});