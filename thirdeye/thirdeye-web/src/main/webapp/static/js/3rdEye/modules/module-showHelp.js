/**
 * @author: Manmeet Singh
 * Behavior for show help
 */
Box.Application.addModule('module-showHelp', function(context) {
	'use strict';
	var $;
	//private method.
	function showHelp(){
		
		var pagePrefix = "/3rdeye-web/static/help/";		
		var pageName = window.location.pathname;
		var parts = pageName.split('/');
		
		if(parts.length > 4)
			var vpageName = parts[3];
		else
			var vpageName= parts[2];
		

		if(parts.includes("dashboard"))
			var vpageName="dashboard" ;
		//this returns just the page name and its extension
	    pageName = pageName.substring(pageName.lastIndexOf('/') + 1);
	
		//this identifies just the page extension of the current page
		var pageExtension = pageName.substring(pageName.lastIndexOf('.'));
		
		if(parts.length==6)
			pageExtension=parts[4];
		
		var pageindex="#"+ pageExtension;		
		pageName = pagePrefix + vpageName +".html"+pageindex;		
		var myWindow = window.open(pageName, "tinyWindow", 'scrollbars=yes,menubar=no,height=450,width=900,resizable=yes,toolbar=no,location=no,status=no');
		myWindow.focus()
	
	}
	
	

	return {
		init: function (){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');

		},

		onclick:function(event, element,elementType){
			if(elementType ==="help"){
				showHelp();
			}
		}


	}
});