/**
 * This service is used to apply notification using Notify.js
 * @author summerintern.sarthak
 */
Box.Application.addService('service-notify', function(application) {
	
	'use strict';
	var $;
	
	//function to set the className in properties
	function getProperties(className){
		var prop = {globalPosition:"top center",
			elementPosition : "bottom right",
			autoHideDelay: 1300 ,
			className : className,
			showAnimation: 'fadeIn', 
			hideAnimation: 'fadeOut'};
		return prop;
	}
	
	
	//To get the global jquery reference
	function extractJquery(){
		if (!$){
			$ = application.getGlobal('jQuery');
		}
	}
		
	
	return {
		setNotification : function(message, position, className){
			extractJquery();
			
			//Setting the className in the properties
			var properties = getProperties(className);
			
			//Applying Notification at given position with incoming message
			if(position){
				$(position).notify(message, properties);
			}else{
				$.notify(message, properties);
			}
		}
	};
	
});