/**
 * @author: shaishav.dixit
 */
Box.Application.addModule('module-exportFm', function(context) {

	var $;
	var commonUtils;
	var pageUrl = "/exportImport/fetchModal";
	var id;
	var dataObj = {};
	var qid;
	
	function bindFmExportImportModalRequirements($newModal){
		// Bind the ajax form 
		$('#importFmForm', $newModal).ajaxForm(function(htmlResponse) {
			
			if (htmlResponse.redirect){
				window.location.replace(commonUtils.getAppBaseURL(htmlResponse.redirect));
			} else{ 
				var $replacement = commonUtils.swapModalContent("exportImportFmModal", htmlResponse);
				$replacement.modal({backdrop:false});
				bindFmExportImportModalRequirements($replacement);
			}
		}); 
	}
	
	function createFmExportImportModal(id){
		
    	var url = commonUtils.getAppBaseURL("fm/"+id+pageUrl+"/"+qid);
    	
		commonUtils.getHTMLContent(url).then(function(content){
			
			var $newModal = commonUtils.appendModalToPage("exportImportFmModal", content, true);
			
			bindFmExportImportModalRequirements($newModal);
			
			$newModal.modal({backdrop:false,show:true});
		});
	}

    return {

        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');
            
        },
        destroy: function() {
			 dataObj = {};
        },   
    
        onclick: function(event, element, elementType) {
        	if (elementType === 'exportImportMap') {
        		id = $(element).data("fmid");
        		qid = $(element).data("qid");  
        		createFmExportImportModal(id);
        	}else{
	        	if (elementType === 'viewImportFMData') {
	        		dataObj.fmId = $(element).data("fmid");
	        		dataObj.qeId = $(element).data("qid");
	        		context.broadcast('fm::showFunctionalMapData', dataObj);	
	        	}
        	}
        }	
    };
});