/**
 * @author: ananta.sethi
 */
Box.Application.addModule('module-bcmLevelFilter', function(context) {
	'use strict';
	// private methods here
	var $, commonUtils, $moduleDiv, title, message,	moduleConfig,filterMapData;
	var spinner = null;

	function getReportConfiguration(){ 
		var configData= {};
		configData.idsOfLevel1= $('#idsOfLevel1',$moduleDiv).val() != null ? $('#idsOfLevel1',$moduleDiv).val()+"" : null;
		configData.idsOfLevel2= $('#idsOfLevel2',$moduleDiv).val() != null ? $('#idsOfLevel2',$moduleDiv).val()+"" : null;
		configData.idsOfLevel3= $('#idsOfLevel3',$moduleDiv).val() != null ? $('#idsOfLevel3',$moduleDiv).val()+"" : null;
		configData.idsOfLevel4= $('#idsOfLevel4',$moduleDiv).val() != null ? $('#idsOfLevel4',$moduleDiv).val()+"" : null;
		return JSON.stringify(configData);
	}
	
	return {
		behaviors : [ 'behavior-fetchParameterGraph', 'behavior-select2' ],

		init : function() {
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			$moduleDiv = context.getElement();
			moduleConfig = context.getConfig();
			
		},
	
		onclick: function(event, element, elementType) {
			if(elementType === "saveReport"){
				var dataObj = {};
				dataObj.id = moduleConfig.id;
				dataObj.reportType = moduleConfig.reportType.replace(/[']/g, "");
				dataObj.reportConfig = getReportConfiguration(); 
				context.broadcast('repo::savereport', dataObj);
			}
			
			else if (elementType ==="plotGraph"){
				$("#saveAs").show();
				var dataObj = {};
				dataObj.idsOfLevel1= $('#idsOfLevel1',$moduleDiv).val() != null ? $('#idsOfLevel1',$moduleDiv).val()+"" : null;
				dataObj.idsOfLevel2= $('#idsOfLevel2',$moduleDiv).val() != null ? $('#idsOfLevel2',$moduleDiv).val()+"" : null;
				dataObj.idsOfLevel3= $('#idsOfLevel3',$moduleDiv).val() != null ? $('#idsOfLevel3',$moduleDiv).val()+"" : null;
				dataObj.idsOfLevel4= $('#idsOfLevel4',$moduleDiv).val() != null ? $('#idsOfLevel4',$moduleDiv).val()+"" : null;
				context.broadcast('repo::plotgraph', dataObj);
			}
			
		}
	}
});