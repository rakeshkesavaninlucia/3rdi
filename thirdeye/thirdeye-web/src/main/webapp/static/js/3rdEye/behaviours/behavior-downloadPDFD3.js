/**
 * @author: tanushree.mittal
 *  Behavior for Download report as PDF
 *  This behavior allows us to download the reports  as PDF 
 */

Box.Application.addBehavior('behavior-downloadPDFD3', function(context) {
	'use strict';
	var $,d3;
	var $html2canvas,commonUtils;
	var _moduleConfig;
	var module;
	var $imagename;

	function downloadPDFButton()
	{
		$('.box-header').append('<div class="downloadPDF"><i title = "Download PDF" class="fa fa-file-pdf-o clickable"/></div>');
		//on click of download button
		$('.downloadPDF').click( function() {
			$html2canvas($('.box-body'), {
				onrendered: function(canvas) {
					var dataObj={};
					var html = d3.select("svg")
					.attr("version", 1.1)
					.attr("xmlns", "http://www.w3.org/2000/svg")
					.node().parentNode.innerHTML;
					var svgTag = $.find('svg');
					var canvas = module.querySelector('[name="canvas"]');
					if(null==canvas){
						canvas = document.createElement("canvas");
					}
					context = canvas.getContext("2d");
					//Canvas Height & Width
					canvas.height = svgTag[0].clientHeight;
					canvas.width = svgTag[0].clientWidth;
					var image = new Image;
					image.src = 'data:image/svg+xml;base64,'+ btoa(html);
					image.onload = function() {
						context.drawImage(image, 0, 0);
						if(_moduleConfig.pngimagename === "null" || _moduleConfig.pngimagename == undefined)
							$imagename = 'DownloadPDF';
						else
							$imagename =_moduleConfig.pngimagename.replace(/'/g,"");
						dataObj.fileName = $imagename;
						var canvasData = canvas.toDataURL("image/png");
						var pdf = new jsPDF("l", "mm", [canvas.width, canvas.height]);
						pdf.addImage(canvasData, 'png', 0, 0,canvas.width,canvas.height);
						pdf.save($imagename+".pdf");
					};
				},
				background :'#FFFFFF',
			})
		});
	}

	return {
		init: function (){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			$html2canvas = context.getGlobal('html2canvas');
			d3 = context.getGlobal('d3');
			_moduleConfig = context.getConfig();
			module = context.getElement();
			downloadPDFButton();
		}
	}
})