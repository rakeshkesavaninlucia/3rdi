/**
 * @author: ananta.sethi
 *  Behavior for Fetch Parameter
 */
Box.Application.addBehavior('behavior-fetchParameterGraph',function(context) {

	var $,commonUtils;
	
	function fetchParameterValues(urlToHit) {
		var deferred = $.Deferred();

		//var urlToHit = commonUtils.getAppBaseURL($(this).data("url")+ qeId);
		commonUtils.getHTMLContent(urlToHit).then(function(htmlResponse) {
			// Check for the modal indicator and then
			// replace the content.
			deferred.resolve(htmlResponse);
			// deferred.reject(htmlResponse);
		});
		return deferred.promise();
	}
		
	return {
								
       	init : function() {
       		// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			var _this = this;
			
			$(".ajaxSelector").change(function(){
				var $selector = $(this);
				var urlToHit = commonUtils.getAppBaseURL($(this).data("url")+ $selector.val());
				// find value
				
				if($selector.val()!=null && $selector.val()!=-1){
				fetchParameterValues(urlToHit).then(function(htmlResponse){
					// Find the selector which has to be populated.
					var depName = $selector.data("dependency");
					var depSelector = ".ajaxPopulated[data-dependson="+ depName+ "]";
					var $matchingSelectors = $(depSelector);
					$matchingSelectors.empty();
					$matchingSelectors.append(htmlResponse)
				})
				}
			});
			
       	}
	}
});
