/**
 * 
 */
Box.Application.addModule('module-3rdiApp', function(context) {
	'use strict';
	
	var $,commonUtils;
	
	function getCreateModal(url, modalId) {
		
		commonUtils.getHTMLContent(commonUtils.getAppBaseURL(url)).then(function(content){
			
			var $newModal = commonUtils.appendModalToPage(modalId, content, true);
			
			bindNewDashboardModalRequirements($newModal, modalId);
			
			$newModal.modal({backdrop:false,show:true});
		});
	}
	
	function bindNewDashboardModalRequirements ($newModal, modalId){
		// Bind the ajax form 
		var formId = $newModal.find('form').attr('id');
		$("#"+formId, $newModal).ajaxForm(function(htmlResponse) {
			
			if (htmlResponse.redirect){
				window.location.replace(commonUtils.getAppBaseURL(htmlResponse.redirect));
			} else{ 
				var $replacement = commonUtils.swapModalContent(modalId, htmlResponse);
				$replacement.modal({backdrop:false});
				bindNewDashboardModalRequirements($replacement);
			}
		}); 
	}
	
	return{
		init: function(){
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');

			var targetURL = $(location).attr('pathname');
			var $links = $("li>a", $(".sidebar-menu"));

			// Set the side bar to the correct option
			$.each($links, function( index, oneAnchor ) {
				if ( targetURL === $(oneAnchor).attr("href")){
					var $parents = $(oneAnchor).parents("ul.treeview-menu");
					$.each($parents, function( index, parentul ) {
						var $anchorToClick = $(parentul).prev(); 
						if ($anchorToClick.is("a")){
							$anchorToClick.trigger( "click" );
						}
					})
				}
			});

			// Bind the clicks to 
			/*_this._bindWorkspaceLinks();
			_this._bindDashboardCreation();
			_this._bindBCMTemplateCreation();
			_this._bindWorkspaceBCMCreation();
			_this._bindRelationshipTypeCreation();*/
		
		},
		
		onclick: function(event, element, elementType){
			if(elementType === 'createDashboard') {
				getCreateModal("dashboard/create", "createNewDashBoardModal");
			} else if(elementType === 'createWorkspaceBCM') {
				getCreateModal("wsbcm/create", "createWorkspaceBCMModal");
			}
			
		}
	}
});