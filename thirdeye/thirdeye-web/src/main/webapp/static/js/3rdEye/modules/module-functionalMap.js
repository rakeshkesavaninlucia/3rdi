/**
 * @author: shaishav.dixit
 */
Box.Application.addModule('module-functionalMap', function(context) {

var $,commonUtils,bcmId;
	
	function fetchBcmValues(urlToHit) {
		var deferred = $.Deferred();
		commonUtils.getHTMLContent(urlToHit).then(function(htmlResponse) {
			// Check for the modal indicator and then
			// replace the content.
			deferred.resolve(htmlResponse);
			// deferred.reject(htmlResponse);
		});
		return deferred.promise();
	}
	
	function getbcmlevelsforfm($selector){
		//var $selector = $(this);
		var urlToHit = commonUtils.getAppBaseURL($selector.data("url")+ $selector.val());
		// find value
		
		if($selector.val()!=null && $selector.val()!=-1){
		fetchBcmValues(urlToHit).then(function(htmlResponse){
			// Find the selector which has to be populated.
			var depName = $selector.data("dependency");
			var depSelector = ".ajaxPopulated[data-dependson="+ depName+ "]";
			var $matchingSelectors = $(depSelector);
			$matchingSelectors.empty();
			$matchingSelectors.append(htmlResponse)
		})
		}
	}	
	return {
								
       	init : function() {
       		// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			  $(".dropdown_select").select2({
				  placeholder: "Select",
				  allowClear: true
			});
			commonUtils = context.getService('commonUtils');
			var _this = this;        
			$(".ajaxSelector").change(function(){
				getbcmlevelsforfm($(this));
		
			
			});
			
       	}
	
	}
});