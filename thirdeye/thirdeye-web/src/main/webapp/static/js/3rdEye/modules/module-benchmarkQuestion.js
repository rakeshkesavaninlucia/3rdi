/**
 * @author: dhruv.sood
 * function find fragment,addOption,remove option -used while creating a question 
 */
Box.Application.addModule('module-benchmarkQuestion', function(context) {
	'use strict';
	var $,commonUtils;

	function fetchBenchmark(data){
		if(data.isChecked){
			if(data.workspaceId === '')
				data.workspaceId = -1;
			getFragment("benchmark/question/fetchBenchmark/"+data.workspaceId);
		}else {
			getFragment("questionType/MULTCHOICE");
		}
	}
	
	function fetchBenchmarkItem(data){
		var postRequest = $.post(commonUtils.getAppBaseURL("benchmark/question/fetchBenchmarkItem"), data);
		postRequest.done(function(incomingData){
			$('.benchmarkItemsContainer').empty();
			$( incomingData ).insertAfter( $( ".benchmarksContainer" ) );
		})
	}
	
	function getFragment(url){
		var $urlToHit = commonUtils.getAppBaseURL(url);
		$.get( $urlToHit, function( newRowContent ) {
			$( "#getFragment" ).empty();
			$("#getFragment").append(newRowContent);
		});
	}
	
	return {
		behaviors : [ 'behavior-select2' ],
		
		messages: [ 'bq::fetchBenchmark', 'bq::fetchBenchmarkItem' ],
	  	   
        onmessage : function(name, data) {
            if (name === 'bq::fetchBenchmark') {
            	fetchBenchmark(data);
            } else if(name === 'bq::fetchBenchmarkItem') {
            	fetchBenchmarkItem(data);
            } 
        },
		
		init : function(){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');
			
		}
	};
});