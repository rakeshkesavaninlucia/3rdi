/**
* @author: dhruv.sood
* 
* This module takes the frId and bcm levelId as input and generates the mapping between bcm levels and the assets on the basis of 
* functional redundancy data using Handlebars Js. 
*/
Box.Application.addModule('module-viewFunctionalRedundancy', function(context) {
	
		       // private methods here
		       'use strict';
		       var $,$moduleDiv, frURL,jsonRequest,jsonService,commonUtils;
		       var dataObj = {};
		       
		       function getAssetClasses(urlToHit){

			   		var url = commonUtils.getAppBaseURL(urlToHit);
	
			   		var filterURL = commonUtils.readCookie("filterURL");
	
			   		if (filterURL !== undefined && filterURL !== null) {
			   			var urlData = filterURL.split('?').pop();
			   			url = url + "?" + urlData;
			   		}
			   		var postRequest = $.get(url);
			   		postRequest.done(function() {
			   			emptyTableRows();
			   			dataObj.filterMap=$('.assetMappingMainDiv').data("filtermap");
			   			drawFrAssetMap();
			   			context.broadcast('lazyLoad', urlData);
			   		});
		   		}
		       
		       function emptyTableRows() {
		    	   $('.assetMappingMainDiv table').each(function(){ 
		   			   $('#'+this.id).empty();  
		   			});
		       }
		       
		       /*This function fetches the parameters 
		       * for getting the functional redundancy data for mapping with the assets.*/
		       function getFrParameters(){
		          dataObj.fcid=$('.assetMappingMainDiv').data("fcid");
		          dataObj.levelid=$('.assetMappingMainDiv').data("levelid");
		          dataObj.qeid=$('.assetMappingMainDiv').data("qeid");
		          dataObj.mode=$('.assetMappingMainDiv').data("mode");
		          dataObj.filterMap=$('.assetMappingMainDiv').data("filtermap");
		       }
		       
		       function showLoading(toShow){
			   		if (toShow){
			   			$(".assetMappingMainDiv .overlay").fadeIn("slow");
			   		} else {
			   			$(".assetMappingMainDiv .overlay").fadeOut("slow");
			   		}
		   		}
		       
		       function registerHelperForLink(data) {
		    	   Handlebars.registerHelper('link', function(name, id) {
                 	  id = Handlebars.escapeExpression(id);
                 	  name = Handlebars.escapeExpression(name);
                 	  var url = "functionalRed/view/mode/single/" + data.fcid + '/qe/' + data.qeid + '/level/' + id;
                 	  return new Handlebars.SafeString(
                 			  "<a href='" + commonUtils.getAppBaseURL(url) + "'>" + name + "</a>"
                 	  );
                 });
		       }
		       
		       /*This function gets the functional redundancy data 
		        * on the basis of parameters received from request*/ 
		       function drawFrAssetMap(){
		           frURL = commonUtils.getAppBaseURL("functionalRed/view");
		           jsonRequest = jsonService.getJsonResponse(frURL,dataObj); 
		           jsonRequest.done(function(incomingData){ 
		        	   
		           //Check if incomingData is not empty 
		           if(incomingData.bcmDetails!="" && incomingData.assetDetails!="") {

			           //Getting the source for Handlebars
			           var sourceBcmLevelTemplate   = $("#bcmLevelTemplate").html();
			           var sourceAssetMappingTemplate   = $("#assetMappingTemplate").html();
			           var sourceAssetTitlesMappingTemplate   = $("#assetTitlesMappingTemplate").html();
			           
			           //Creating the Handlebars template from source data
			           var template1 = Handlebars.compile(sourceBcmLevelTemplate);
			           var template2 = Handlebars.compile(sourceAssetMappingTemplate);
			           var template3 = Handlebars.compile(sourceAssetTitlesMappingTemplate);
			           
			           //Getting the data in place using the incoming data
			           var bcmLevelDetailsHtml    = template1(incomingData);
			           var assetMappingTableHtml    = template2(incomingData);
			           var assetMappingTableHtml2    = template3(incomingData);
			           
			          
			           //Populating the data on the page
			           $("#bcmLevelDetails", $moduleDiv).append(bcmLevelDetailsHtml);
			           $("#assetMappingTable", $moduleDiv).append(assetMappingTableHtml);
			           $("#assetMappingTable", $moduleDiv).append(assetMappingTableHtml2);
			           
			           //Triggering the action on mouse over event
			           $('[data-toggle="popover"]').popover();
			           $('[data-toggle="tooltip"]').tooltip(); 
			           if($('#nodataavailable').length)
			           	  $('#nodataavailable').remove();
	        	  } else {
	        		  	emptyTableRows();
	        		  	if(!$('#nodataavailable').length)
	        		  		$(".assetMappingMainDiv").append("<b id='nodataavailable' style='font-size:16px;'>No Data Available.</b>");
	        		  }
		           });
		       }

		       function generateHrefLink(data){
		    	   
		   		var url = commonUtils.getAppBaseURL("functionalRed/downloadExcel");
		   		var link = url + '?fcid='+dataObj.fcid+
		   		                 '&levelid='+dataObj.levelid+
		   		                 '&qeid='+ dataObj.qeid+
		   		                 '&mode='+ dataObj.mode;
		   		$("#link").attr("href", link);
		       }
		       
		       function removeCookie(){
		   		commonUtils.removeCookies();
		   	}
		       return {
		    	   	  messages: ['filterSelected'],
		    	   	  
		              init : function(){
		                     $ = context.getGlobal('jQuery');
		                     $moduleDiv = $(context.getElement());
		                     jsonService = context.getService('jsonService');
		                     commonUtils = context.getService('commonUtils');
		                     
		                     //Get the frId and the levelId                     
		                     getFrParameters();  
		                     
		                     showLoading(true);
		                     //Create the JSON request, get the JSON data and draw the mapping between FR and asset
		                     generateHrefLink();
		                     drawFrAssetMap(dataObj);
		                     showLoading(false);
		                     
		                     registerHelperForLink(dataObj);
		                     removeCookie();
		              },
		              
		              onclick: function(event, element, elementType) {
		              	if (elementType === 'single') {
		              		var urlToHitSingle = 'functionalRed/view/mode/single/'+ dataObj.fcid+'/qe/'+dataObj.qeid +'/level/'+dataObj.levelid;
		    				window.location.href = commonUtils.getAppBaseURL(urlToHitSingle);
		              	} else if (elementType === 'full') {
		              		var urlToHitFull = 'functionalRed/view/mode/full/'+ dataObj.fcid+'/qe/'+dataObj.qeid +'/level/'+dataObj.levelid;
		              		window.location.href = commonUtils.getAppBaseURL(urlToHitFull);
		              	}else if(elementType === 'assetTitle'){
		              		
		              		var data = $(element).attr('id');
		              		location.href = commonUtils.getAppBaseURL("templates/asset/view/"+data);
		              	}
		              },
		          	
		      		onmessage : function(name){
		      			if (name === 'filterSelected') {
		      				var urlToHit = 'functionalRed/view/mode/' + dataObj.mode + '/'+ dataObj.fcid+'/qe/'+dataObj.qeid +'/level/'+dataObj.levelid;
		      				getAssetClasses(urlToHit);
		      			}
		      		}
		       };
		});
