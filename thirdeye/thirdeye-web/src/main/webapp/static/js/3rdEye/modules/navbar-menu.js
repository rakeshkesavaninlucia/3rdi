/**
 * 
 */

Box.Application.addModule('navbar-menu', function(context) {

    // private methods here
	var $;
	var commonUtils;
	var mySkins = [
	    'skin-blue',
	    'skin-black',
	    'skin-red',
	    'skin-yellow',
	    'skin-purple',
	    'skin-green',
	    'skin-blue-light',
	    'skin-black-light',
	    'skin-red-light',
	    'skin-yellow-light',
	    'skin-purple-light',
	    'skin-green-light'
	  ]
 function changeLayoutColor(){
		 var tmp = get('skin')
		    if (tmp && $.inArray(tmp, mySkins))
		      changeSkin(tmp)

		    // Add the change skin listener
		    $('[data-skin]').on('click', function (e) {
		      if ($(this).hasClass('knob'))
		        return
		      e.preventDefault()
		      changeSkin($(this).data('skin'))
		    })

	
	}
	
	
	  function changeSkin(cls) {
		    $.each(mySkins, function (i) {
		      $('body').removeClass(mySkins[i])
		    })

		    $('body').addClass(cls)
		    store('skin', cls)
		    return false
		  }

	
	 function get(name) {
	    if (typeof (Storage) !== 'undefined') {
	      return localStorage.getItem(name)
	    } else {
	      window.alert('Please use a modern browser to properly view this template!')
	    }
	  }
	 function store(name, val) {
	    if (typeof (Storage) !== 'undefined') {
	      localStorage.setItem(name, val)
	    } else {
	      window.alert('Please use a modern browser to properly view this template!')
	    }
	  }
    return {

        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');
            changeLayoutColor();

	
          
        },
    
        onclick: function(event, element, elementType) {
        	if (elementType === 'workspaceSwitch') {
        		this.switchActiveWorkspace($(element).data("workspaceid"), $(element).data("workspacename"));
        	}
        	if (elementType === 'switchSkin') {
        		 changeLayoutColor();
        		
        	}
        },
        
        switchActiveWorkspace: function(workspaceId, workspaceName) {
        	if (confirm("Your active workspace will be shifted to " + workspaceName + "\n You will lose any unsaved data.")){
				var dataObj = {};
				dataObj.workspaceId = workspaceId;
				
				// This will be the service call
				var postRequest = $.post( commonUtils.getAppBaseURL("workspace/switchactive"), dataObj);
				postRequest.done(function(incomingData){
					window.location.reload(true);
					 changeLayoutColor();
				})
			}
        },
        switchSkin: function() {
        	
        }


    };
});