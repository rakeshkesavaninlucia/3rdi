/**
 * @author sunil1.gupta
 */
Box.Application.addModule('module-sharedReportSelectUser',function(context) {
	'use strict';
	
	var $,commonUtils,moduleEl,notifyService;
	var modalUrl = "report/shareReportModal";
	var dataObj={};
	function showSharedReportModule(data){	
		 var modalId = moduleEl.id;
		 commonUtils.populateModalWithURL(modalId, commonUtils.getAppBaseURL(modalUrl),data)
		.then(function(){
			Box.Application.startAll(moduleEl);
			// Show the modal
			var $modalDiv = $(moduleEl).modal({
				keyboard: true
			})
		});	
	}
	
	function hideSharedReportModal(){
		$(moduleEl).modal('hide');
	}
	
	
	function requestForUser(dataObj){
		var table = $('.userListTable',moduleEl).DataTable();
		var dataUser = table.$('.userCheckedClass:checked').map(function() { return this.value; }).get();
		dataObj.userids = dataUser;	
		context.broadcast('sr::selectedUser', dataObj);
	}
	
return {
		
	  messages: ['sr::showUserSelector','sr::hideSharedReportModal'],
	
		init : function(){ 
			$ = context.getGlobal('jQuery'); 
			commonUtils = context.getService('commonUtils'); 
			moduleEl = context.getElement(); 
			notifyService = context.getService('service-notify'); 
			moduleEl = context.getElement();
		},

		  onmessage: function(name,data) {
	        	 
				if (name === 'sr::showUserSelector'){
					dataObj.reportid = data.reportid;
					showSharedReportModule(data);
				
				}
				if(name==='sr::hideSharedReportModal'){
					hideSharedReportModal();
				}
			},
		
		 onclick: function(event, element, elementType){
			
			if (elementType === 'addUsersToShareReport') {
                requestForUser(dataObj);
			}
					
		}
	
	};
	
});