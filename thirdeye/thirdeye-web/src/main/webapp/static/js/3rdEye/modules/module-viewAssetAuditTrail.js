/**
 * @author: shagun.sharma
 *  module for view asset history
 *  (functions-edit,delete,save)
 */
Box.Application.addModule('module-viewAssetAuditTrail',function(context) {
	'use strict';
	var $,commonUtils,moduleEl,id,dateVariable,dateVariabl,wwsid,val;
	var dataObj = {};
	var checkBoxes=[];
	var spinner = null;
//	private methods here
	
	function showLoading(container, show){
		var spinService = context.getService('service-spinner');
		//Calling the service
		spinner = spinService.getSpinner(container,show,spinner);   	
	 }
	function showDatePicker(){
		$( ".datepicker" ).datepicker({
			autoclose: true,
			format: "yyyy-mm-dd",
		});
	}
	function getAssetAuditTrailData(dataObj)
	{
		
		dataObj.userIds =$('.userChecked:checked').map(function() { return this.value; }).get();
		dataObj.assetTemplateIds = $('.assetTemplateChecked:checked').map(function() { return this.value; }).get();
		dataObj.eventFilters = $('.eventChecked:checked').map(function() { return this.value; }).get();
		var url = commonUtils.getAppBaseURL("assetAuditTrail/data");
		var getRequest = $.get(url,dataObj);
		getRequest.done(function(incomingData){
			var data = 	incomingData;
			$(".assetauditTrail",moduleEl).empty();
			showLoading($('#assetTypeView'), false);
			$(".assetauditTrail",moduleEl).append(incomingData);	 
		})

	}
	
	
	return {	

		// Initialize the page
		init : function() {

			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			moduleEl = context.getElement();
			commonUtils = context.getService('commonUtils');	
			showDatePicker();
		},
		onclick:function(event,element,elementType){
			if(elementType ==='generateReport'){
				dataObj.toDate = $(".toDate").val();
				dataObj.fromDate = $(".fromDate").val();
				if(dataObj.fromDate!="" && 	dataObj.toDate==="" || dataObj.toDate!="" && dataObj.fromDate===""){
					alert("Please Select Date Range");
				}
				else{
				showLoading($('#assetTypeView'), true);
				getAssetAuditTrailData(dataObj);
				}
			}
		}
	}
});