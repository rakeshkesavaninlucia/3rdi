package org.birlasoft.thirdeye.search.api.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Bean class for Aid block indexing
 * @author samar.gupta
 *
 */
public class IndexAidBlockBean {
	
	private Integer id;
	private String title;
	private Integer sequenceNumber;
	private String aidBlockType;
	private List<IndexAidSubBlockBean> subBlocks = new ArrayList<>();
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	
	public List<IndexAidSubBlockBean> getSubBlocks() {
		return subBlocks;
	}
	public void setSubBlocks(List<IndexAidSubBlockBean> subBlocks) {
		this.subBlocks = subBlocks;
	}
	public String getAidBlockType() {
		return aidBlockType;
	}
	public void setAidBlockType(String aidBlockType) {
		this.aidBlockType = aidBlockType;
	}
}
