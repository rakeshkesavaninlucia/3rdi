package org.birlasoft.thirdeye.search.api.beans;

import java.math.BigDecimal;
import java.util.List;

public class AssetParameterWrapper {
	
	private String parameter;
	private Integer parameterId;
	private List<AssetParameterBean> values;
	
	public String getParameter() {
		return parameter;
	}
	
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	
	public Integer getParameterId() {
		return parameterId;
	}

	public void setParameterId(Integer parameterId) {
		this.parameterId = parameterId;
	}

	public List<AssetParameterBean> getValues() {
		return values;
	}
	
	public void setValues(List<AssetParameterBean> values) {
		this.values = values;
	}
	
	/**
	 * Fetch parameter value for an asset from the list
	 * @param ab
	 * @return
	 */
	public BigDecimal fetchParamValueForAsset(Integer asetId){
		if(!values.isEmpty()){
			for (AssetParameterBean assetParameterBean : values) {
				if(assetParameterBean.getAssetId().equals(asetId)){
					return assetParameterBean.getParameterValue();
				}
			}
		}
		return new BigDecimal(0);
	}

}
