package org.birlasoft.thirdeye.search.api.beans;


public class AssetBeanBase  extends AbstractProtectedEntityBean{
	
	protected String uid;
	protected String name;
	protected String assetClass;
	protected String assetTemplateName;
	protected Integer assetTemplateId;
	
	public AssetBeanBase(String tenantURL, int workspaceId) {
		super(tenantURL, workspaceId);
	}
	
	public AssetBeanBase() {}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAssetClass() {
		return assetClass;
	}

	public void setAssetClass(String assetClass) {
		this.assetClass = assetClass;
	}

	public String getAssetTemplateName() {
		return assetTemplateName;
	}

	public void setAssetTemplateName(String assetTemplateName) {
		this.assetTemplateName = assetTemplateName;
	}

	public Integer getAssetTemplateId() {
		return assetTemplateId;
	}

	public void setAssetTemplateId(Integer assetTemplateId) {
		this.assetTemplateId = assetTemplateId;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
	
}
