package org.birlasoft.thirdeye.search.support;

import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class SearchArgumentResolver implements HandlerMethodArgumentResolver {

	
	
	@Override
	public Object resolveArgument(MethodParameter arg0,
			ModelAndViewContainer arg1, NativeWebRequest arg2,
			WebDataBinderFactory arg3) throws Exception {
		return new SearchConfig("", "");
	}

	@Override
	public boolean supportsParameter(MethodParameter methodParam) {
		return methodParam.getParameterType().equals(SearchConfig.class);
	}

}
