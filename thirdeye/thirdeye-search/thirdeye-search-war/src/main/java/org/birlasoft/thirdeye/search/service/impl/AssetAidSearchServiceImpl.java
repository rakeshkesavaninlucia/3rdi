package org.birlasoft.thirdeye.search.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import org.apache.lucene.search.join.ScoreMode;
import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.api.constant.IndexAidTags;
import org.birlasoft.thirdeye.search.api.constant.IndexAssetTags;
import org.birlasoft.thirdeye.search.service.AssetAidSearchService;
import org.birlasoft.thirdeye.search.service.FilterSearchService;
import org.birlasoft.thirdeye.util.Utility;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This service implementation class for asset aid from elastic search.
 * @author samar.gupta
 *
 */
@Service
public class AssetAidSearchServiceImpl implements AssetAidSearchService {
	
	@Autowired
	private Client client;
	
	@Autowired
	private FilterSearchService filterSearchService;
	
	@Override
	public IndexAssetBean getAssetAid(SearchConfig searchConfig) {
		IndexAssetBean indexAssetBean = new IndexAssetBean();
		int size = 10;
		for(String tenant: searchConfig.getTenantURLs()){
			SearchResponse response = getAsset(searchConfig, tenant, size);
			if(response.getHits().getTotalHits() > size){
				size = (int) response.getHits().getTotalHits();
				response = getAsset(searchConfig, tenant, size);
			}

			indexAssetBean = extractAidSearchWrapper(response);
		}
		return indexAssetBean;
	}

	private SearchResponse getAsset(SearchConfig searchConfig, String tenant, int size) {
		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(QueryBuilders.boolQuery()
						.must(QueryBuilders.termQuery("id", searchConfig.getAssetId()))
								.must(QueryBuilders.nestedQuery(IndexAssetTags.AIDS.getTagKey(), QueryBuilders.termQuery(IndexAssetTags.AIDS.getTagKey()+"."+IndexAidTags.ID.getTagKey(), searchConfig.getAidId()), ScoreMode.Avg)))
								.setFrom(0).setSize(size)
								.execute()
								.actionGet();
	}
	
	private IndexAssetBean extractAidSearchWrapper(SearchResponse response) {
		IndexAssetBean indexAssetBean = new IndexAssetBean();

		if (response.getHits() != null && response.getHits().getTotalHits() == 1) {
			indexAssetBean = Utility.convertJSONStringToObject(response.getHits().getAt(0).getSourceAsString(), IndexAssetBean.class);
		}
		return indexAssetBean;
	}

	
	@Override
	public Set<Integer> getAidAssetIds(SearchConfig searchConfig) {
		Set<Integer> setOfAssetId = new HashSet<>();
		int size = 10;
		for (String tenant : searchConfig.getTenantURLs()) {
			SearchResponse searchResponse = getAids(searchConfig, tenant, size);
			if (searchResponse.getHits().getTotalHits() > size) {
				size = (int) searchResponse.getHits().getTotalHits();
				searchResponse = getAids(searchConfig, tenant, size);
			}

			setOfAssetId = extractAidWrapper(searchResponse);
		}

		return setOfAssetId;
	}

	private Set<Integer> extractAidWrapper(SearchResponse searchResponse) {

		Set<Integer> setOfAssetIds = new HashSet<>();
		if (searchResponse.getHits() != null) {

			for (SearchHit oneHit : searchResponse.getHits()) {
				IndexAssetBean indexAssetBean = Utility.convertJSONStringToObject(oneHit.getSourceAsString(),
						IndexAssetBean.class);
				setOfAssetIds.add(indexAssetBean.getId());
			}

		}
		return setOfAssetIds;
	}

	private SearchResponse getAids(SearchConfig searchConfig, String tenant, int size) {

		QueryBuilder filterQueryBuilder = null;
		if (!searchConfig.getFilterMap().isEmpty()) {
			filterQueryBuilder = filterSearchService.extractBoolFilterQuery(searchConfig.getFilterMap());
		}

		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(QueryBuilders.nestedQuery("aids", QueryBuilders.termQuery("aids.id", searchConfig.getAidId()), ScoreMode.Avg))
				.setPostFilter(filterQueryBuilder)
				.setFrom(0).setSize(size)
				.execute().actionGet();

	}

	@Override
	public Set<Integer> getAssetIdsByAssetTemplate(SearchConfig searchConfig) {
		Set<Integer> setOfAssetId = new HashSet<>();
		int size = 10;
		for (String tenant : searchConfig.getTenantURLs()) {
			SearchResponse searchResponse = getAssetIdsByTemplate(searchConfig, tenant, size);
			if (searchResponse.getHits().getTotalHits() > size) {
				size = (int) searchResponse.getHits().getTotalHits();
				searchResponse = getAssetIdsByTemplate(searchConfig, tenant, size);
			}

			setOfAssetId = extractAidWrapper(searchResponse);
		}

		return setOfAssetId;
	}
	
	private SearchResponse getAssetIdsByTemplate(SearchConfig searchConfig, String tenant, int size) {

		QueryBuilder filterQueryBuilder = null;
		if (!searchConfig.getFilterMap().isEmpty()) {
			filterQueryBuilder = filterSearchService.extractBoolFilterQuery(searchConfig.getFilterMap());
		}

		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(QueryBuilders.termQuery("assetTemplateId", searchConfig.getListOfIds().get(0)))
				.setPostFilter(filterQueryBuilder)
				.setFrom(0).setSize(size)
				.execute().actionGet();

	}
	
	
	@Override
	public 	List<IndexAssetBean> getAssetsForDensityHeatMap(SearchConfig searchConfig) {
		List<IndexAssetBean> indexAssetBean = new ArrayList<>();
		SearchResponse response = null;
		int size = 10;
		for(String tenant: searchConfig.getTenantURLs()){
			if(searchConfig.getListOfIds()!=null) {
				response = getAssetForBCM(searchConfig, tenant, size);
			}else {
				response = getAssetTemplateCol(searchConfig, tenant, size);
			}
			if(response.getHits().getTotalHits() > size){
				size = (int) response.getHits().getTotalHits();
				if(searchConfig.getListOfIds()!=null) {
					response = getAssetForBCM(searchConfig, tenant, size);
				}else {
				response = getAssetTemplateCol(searchConfig, tenant, size);
				}
			}

			indexAssetBean = extractIndexAssetBean(response);
		}
		return indexAssetBean;
	}

	private SearchResponse getAssetTemplateCol(SearchConfig searchConfig, String tenant, int size) {
		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(QueryBuilders.boolQuery()
				.must(QueryBuilders.termsQuery("workspaceId", searchConfig.getListOfWorkspace())))
				.execute()
				.actionGet();
	}
	
	private SearchResponse getAssetForBCM(SearchConfig searchConfig, String tenant, int size) {
		QueryBuilder postFilterQuery = QueryBuilders.boolQuery()
				.must(QueryBuilders.termsQuery("workspaceId", searchConfig.getListOfWorkspace()));
		
		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(QueryBuilders.termsQuery("id", searchConfig.getListOfIds()))
				.setPostFilter(postFilterQuery)
				.setFrom(0).setSize(size)
				.execute()
				.actionGet();
	}
	
	private List<IndexAssetBean> extractIndexAssetBean(SearchResponse response) {
		List<IndexAssetBean> assetParameterWrapper = new ArrayList<>();
		for (SearchHit oneHit : response.getHits()) {
			IndexAssetBean indexAssetBean = Utility.convertJSONStringToObject(oneHit.getSourceAsString(), IndexAssetBean.class);
			if(indexAssetBean!=null) {
			assetParameterWrapper.add(indexAssetBean);
			}
	}
		return assetParameterWrapper;
	}

}
