package org.birlasoft.thirdeye.search.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.search.join.ScoreMode;
import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.IndexQuestionBean;
import org.birlasoft.thirdeye.search.api.beans.IndexQuestionValueTypeBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.api.beans.SearchCostStructureBean;
import org.birlasoft.thirdeye.search.api.beans.SearchQuestionBean;
import org.birlasoft.thirdeye.search.api.beans.TcoAssetBean;
import org.birlasoft.thirdeye.search.api.constant.IndexAssetTags;
import org.birlasoft.thirdeye.search.service.TcoReportSearchService;
import org.birlasoft.thirdeye.util.Utility;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.nested.InternalNested;
import org.elasticsearch.search.aggregations.bucket.terms.LongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.elasticsearch.search.aggregations.metrics.sum.InternalSum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TcoReportSearchServiceImpl implements TcoReportSearchService {

	private static final String TOTAL_COST = "total_cost";
	@Autowired
	private Client client;

	@Override
	public List<SearchCostStructureBean> getCostStructureAggregateValue(SearchConfig searchConfig) {
		List<SearchCostStructureBean> listOfTcoSearchBean = new ArrayList<>();
		for(String tenant: searchConfig.getTenantURLs()){
			SearchResponse response = getResponseForCostStructureAggregate(tenant);

			extractSearchCostStructureBeanForAggregate(searchConfig, listOfTcoSearchBean, response);
		}
		return listOfTcoSearchBean;
	}

	/**
	 * Prepare {@link SearchCostStructureBean} from elasticsearch aggregate response
	 * @param searchConfig
	 * @param listOfTcoSearchBean
	 * @param response
	 */
	private void extractSearchCostStructureBeanForAggregate(SearchConfig searchConfig, List<SearchCostStructureBean> listOfTcoSearchBean,
			SearchResponse response) {
		InternalNested internalNested = response.getAggregations().get(IndexAssetTags.COSTSTRUCTURES.getTagKey());
		LongTerms longTerms = internalNested.getAggregations().get("popular_cost");
		for (Bucket oneBucket : longTerms.getBuckets()) {
			SearchCostStructureBean tcoSearchBean = new SearchCostStructureBean();
			if(searchConfig.getListOfIds().contains(Integer.valueOf(oneBucket.getKeyAsString()))){
				InternalNested internalNested1 = oneBucket.getAggregations().get(TOTAL_COST);
				InternalSum internalSum = internalNested1.getAggregations().get("total");
				tcoSearchBean.setId(Integer.valueOf(oneBucket.getKeyAsString()));
				tcoSearchBean.setAggregate(BigDecimal.valueOf((Double) internalSum.getValue()).setScale(2, BigDecimal.ROUND_HALF_EVEN));
				listOfTcoSearchBean.add(tcoSearchBean);
			}
		}
	}

	/**
	 * Get average of all the assets for a parameter
	 * @param searchConfig
	 * @param tenant
	 * @return
	 */
	private SearchResponse getResponseForCostStructureAggregate(String tenant) {
		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.addAggregation(AggregationBuilders.nested(IndexAssetTags.COSTSTRUCTURES.getTagKey(), IndexAssetTags.COSTSTRUCTURES.getTagKey())/*.path(IndexAssetTags.COSTSTRUCTURES.getTagKey())*/
						.subAggregation(AggregationBuilders.terms("popular_cost").field("costStuctures.id").size(50)
								.subAggregation(AggregationBuilders.nested(TOTAL_COST, "costStuctures.currentValue")/*.path("costStuctures.currentValue")*/
										.subAggregation(AggregationBuilders.sum("total").field("costStuctures.currentValue.value")))))
										.execute()
										.actionGet();
	}

	@Override
	public List<SearchQuestionBean> getCostElementAggregateValue(SearchConfig searchConfig) {
		List<SearchQuestionBean> listOfQuestionBean = new ArrayList<>();
		for(String tenant: searchConfig.getTenantURLs()){
			SearchResponse response = getResponseForQuestionAggregate(tenant);

			extractSearchQuestionBeans(searchConfig, listOfQuestionBean, response);
		}
		return listOfQuestionBean;
	}

	private void extractSearchQuestionBeans(SearchConfig searchConfig, List<SearchQuestionBean> listOfQuestionBean,
			SearchResponse response) {
		InternalNested internalNested = response.getAggregations().get(IndexAssetTags.QUESTIONS.getTagKey());
		LongTerms longTerms = internalNested.getAggregations().get("cost_element");
		for (Bucket oneBucket : longTerms.getBuckets()) {
			if(searchConfig.getListOfIds().contains(Integer.valueOf(oneBucket.getKeyAsString()))){
				SearchQuestionBean searchQuestionBean = new SearchQuestionBean();
				InternalNested internalNested1 = oneBucket.getAggregations().get(TOTAL_COST);
				LongTerms longTerms1 = internalNested1.getAggregations().get("ques_param");
				for (Bucket bucket : longTerms1.getBuckets()) {
					if(searchConfig.getQpId().equals(Integer.valueOf(bucket.getKeyAsString()))){
						InternalSum internalSum = bucket.getAggregations().get("ques_sum");
						searchQuestionBean.setQuestionnaireParameterId(Integer.valueOf(bucket.getKeyAsString()));
						searchQuestionBean.setValue(BigDecimal.valueOf((Double) internalSum.getValue()).setScale(2, BigDecimal.ROUND_HALF_EVEN));
						searchQuestionBean.setId(Integer.valueOf(oneBucket.getKeyAsString()));
					}
				}

				listOfQuestionBean.add(searchQuestionBean);
			}
		}		
	}

	private SearchResponse getResponseForQuestionAggregate(String tenant) {
		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.addAggregation(AggregationBuilders.nested(IndexAssetTags.QUESTIONS.getTagKey(), IndexAssetTags.QUESTIONS.getTagKey())/*.path(IndexAssetTags.QUESTIONS.getTagKey())*/
						.subAggregation(AggregationBuilders.terms("cost_element").field("questions.id").size(1000)
								.subAggregation(AggregationBuilders.nested(TOTAL_COST, "questions.values")/*.path("questions.values")*/
										.subAggregation(AggregationBuilders.terms("ques_param").field("questions.values.questionnaireParameterId")
												.subAggregation(AggregationBuilders.sum("ques_sum").field("questions.values.value"))))))
												.execute()
												.actionGet();
	}

	@Override
	/**
	 * Get IndexAssetBean 
	 * @param searchConfig
	 */
	public List<IndexAssetBean> getListOfAssetbean(SearchConfig searchConfig){
		List<IndexAssetBean> listOfAssetBean = new ArrayList<>();
		int size = 40;
		for(String tenant: searchConfig.getTenantURLs()){
			SearchResponse searchResponse = getResponseForAssetInCOA(searchConfig, tenant, size);
			if(searchResponse.getHits().getTotalHits() > size){
				size = (int) searchResponse.getHits().getTotalHits();
				searchResponse = getResponseForAssetInCOA(searchConfig, tenant, size);
			}
			listOfAssetBean = extractSearchAssetBean(searchResponse);
		}
		return listOfAssetBean;
	}

	
	/**
	 * @param searchResponse
	 */
	private List<IndexAssetBean> extractSearchAssetBean(SearchResponse searchResponse) {
		List<IndexAssetBean> assetBeanList = new ArrayList<>();
		if (searchResponse.getHits() != null) {
			for (SearchHit oneHit : searchResponse.getHits()) {
				IndexAssetBean indexAssetBean = Utility.convertJSONStringToObject(oneHit.getSourceAsString(), IndexAssetBean.class);
				assetBeanList.add(indexAssetBean);
			}
		}
		return assetBeanList;	
	}
	

	/**
	 * @param searchConfig
	 * @param tenant
	 * @param size
	 */
	private SearchResponse getResponseForAssetInCOA(SearchConfig searchConfig,  String tenant, int size) {
		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(QueryBuilders.nestedQuery("costStuctures",QueryBuilders
						.nestedQuery("costStuctures.currentValue", QueryBuilders
								.boolQuery().must(QueryBuilders
										.termQuery("costStuctures.currentValue.questionnaireId", searchConfig.getQuestionnaireId()))
										.mustNot(QueryBuilders.termQuery("costStuctures.currentValue.value", 0)), ScoreMode.Avg), ScoreMode.Avg))
										.setFrom(0).setSize(size)
										.execute().actionGet();
	}
	
	@Override
	public List<TcoAssetBean> getTotalCostOfAsset(SearchConfig searchConfig) {
		int size = 10;
		List<TcoAssetBean> listOfTcoAssetBean = new ArrayList<>();
		for(String tenant: searchConfig.getTenantURLs()){
			SearchResponse response = getResponseForAssetsInChart(tenant, searchConfig.getQuestionnaireId(), size);
			if(response.getHits().getTotalHits() > size){
				size = (int) response.getHits().getTotalHits();
				response = getResponseForAssetsInChart(tenant, searchConfig.getQuestionnaireId(), size);
			}

			listOfTcoAssetBean = extractListOfTcoForAssets(searchConfig, response);
		}
		return listOfTcoAssetBean;
	}

	private List<TcoAssetBean> extractListOfTcoForAssets(SearchConfig searchConfig, SearchResponse response) {
		List<TcoAssetBean> tcoAssetBeans = new ArrayList<>();

		for (SearchHit oneHit : response.getHits()) {
			TcoAssetBean tcoAssetBean = new TcoAssetBean();
			BigDecimal totalCost = new BigDecimal(0);
			IndexAssetBean indexAssetBean = Utility.convertJSONStringToObject(oneHit.getSourceAsString(), IndexAssetBean.class);
			for (IndexQuestionBean oneQuestionBean : indexAssetBean.getQuestions()) {
				for (IndexQuestionValueTypeBean oneQuesValueBean : oneQuestionBean.getValues()) {
					if(oneQuesValueBean.getQuestionnaireId().equals(searchConfig.getQuestionnaireId())) {
						totalCost = totalCost.add(oneQuesValueBean.getValue());
					}
				}
			}
			tcoAssetBean.setTotalCost(totalCost);
			tcoAssetBean.setAssetName(indexAssetBean.getName());
			tcoAssetBean.setAssetId(indexAssetBean.getId());
			tcoAssetBeans.add(tcoAssetBean);	
		}
		return tcoAssetBeans;
	}

	private SearchResponse getResponseForAssetsInChart(String tenant, Integer questionnaireId, Integer size) {
		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(QueryBuilders.nestedQuery("questions", QueryBuilders.nestedQuery("questions.values", QueryBuilders.matchQuery("questions.values.questionnaireId", questionnaireId), ScoreMode.Avg), ScoreMode.Avg))
				.setSize(size)
				.execute()
				.actionGet();
	}

}
