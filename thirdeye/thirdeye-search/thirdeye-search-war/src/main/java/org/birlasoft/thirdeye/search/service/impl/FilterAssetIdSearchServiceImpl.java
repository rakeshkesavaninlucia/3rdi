package org.birlasoft.thirdeye.search.service.impl;

import org.birlasoft.thirdeye.search.service.FilterSearchService;
import java.util.List;
import java.util.ArrayList;
import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.util.Utility;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.birlasoft.thirdeye.search.service.FilterAssetIdSearchService;

@Service
public class FilterAssetIdSearchServiceImpl implements FilterAssetIdSearchService{

	@Autowired
	private Client client;

	@Autowired
	private FilterSearchService filterSearchService;

	@Override
	public List<Integer> getAssetIds(SearchConfig searchConfig) {
		List<Integer> listofassetids = new ArrayList<>();
		int size = 10;
		for(String tenant: searchConfig.getTenantURLs()){
			SearchResponse response = getAssetid(searchConfig, tenant, size);
			if(response.getHits().getTotalHits() > size){
				size = (int) response.getHits().getTotalHits();
				response = getAssetid(searchConfig, tenant, size);
			}
			listofassetids = extractAssetIdWrapper(response);
		}
		return listofassetids;
	}

	private SearchResponse getAssetid(SearchConfig searchConfig, String tenant, int size) {

		QueryBuilder filterQueryBuilder = null;
		if (!searchConfig.getFilterMap().isEmpty()) {
			filterQueryBuilder = filterSearchService.extractBoolFilterQuery(searchConfig.getFilterMap());
		}
		return client.prepareSearch(tenant)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setQuery(filterQueryBuilder)
				.setFrom(0).setSize(size)
				.execute()
				.actionGet();
	}

	private List<Integer> extractAssetIdWrapper(SearchResponse searchResponse) {

		List<Integer> listOfassetid = new ArrayList<>();
		if (searchResponse.getHits() != null) {
			for (SearchHit oneHit : searchResponse.getHits()) {
				IndexAssetBean indexAssetBean = Utility.convertJSONStringToObject(oneHit.getSourceAsString(),IndexAssetBean.class);
				listOfassetid.add(indexAssetBean.getId());
			}
		}
		return listOfassetid;
	}
}
