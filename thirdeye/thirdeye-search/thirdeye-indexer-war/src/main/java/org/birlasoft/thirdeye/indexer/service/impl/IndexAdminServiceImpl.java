package org.birlasoft.thirdeye.indexer.service.impl;

import org.birlasoft.thirdeye.indexer.service.IndexAdministratorService;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IndexAdminServiceImpl implements IndexAdministratorService {
	
	private static Logger LOGGER = LoggerFactory.getLogger(IndexAdminServiceImpl.class);
	
	@Autowired
	private Client client;
	
	@Override
	public boolean createIndexAndMapping(String tenant, String type, XContentBuilder mappingAndTypeObject) {
		
		if (mappingAndTypeObject == null){
			return false;
		}
		
		PutMappingResponse responseFromES = client.admin().indices().preparePutMapping(tenant)   
        		.setType(type)                               
    			.setSource(mappingAndTypeObject)
    			.get();
		
		LOGGER.info("SEARCH;INDEX MGR; Create Mapping for tenant '" + tenant + "' for Type "+ type + "is " + responseFromES.isAcknowledged());
		
		return responseFromES.isAcknowledged();
	}
	
	@Override
	public boolean createTenant(String tenant) {
		
		IndicesAdminClient indicesAdminClient = client.admin().indices();
		CreateIndexResponse indexResponse = indicesAdminClient.prepareCreate(tenant).setSettings(Settings.builder()             
                .put("index.mapping.nested_fields.limit",922337)).get();
		
		boolean responseStatus =  indexResponse.isAcknowledged();
		
		LOGGER.info("SEARCH;INDEX MGR; Create Index for tenant '" + tenant + "' is " + responseStatus);
		
		
		return responseStatus;
		
	}
	
	@Override
	public boolean deleteTenant(String tenant) {
    	
        DeleteIndexResponse response = client.admin().indices().prepareDelete(tenant).get();
        if (response.isAcknowledged()) {
            return true;
        }
		return false;
	}
	
	@Override
	public boolean tenantExists(String tenantURL) {
		boolean exists = client.admin().indices().prepareExists(tenantURL).execute().actionGet().isExists();
		
		return exists;
	}
}
